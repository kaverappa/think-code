import unittest
import os
import pathlib
import itertools
from collections import deque
import heapq
from collections import defaultdict
import math
import operator
import string
import bisect
import copy
from fractions import Fraction
from sortedcontainers import SortedList

def getTestFileList( tag, basepath='tests' ):
	return set( [ pathlib.Path( filename ).stem for filename in os.listdir( '{}/{}'.format( basepath, tag ) ) ] )

def getTestFileSuffixList( tag, basepath='tests' ):
	return set( [ pathlib.Path( filename ).suffix for filename in os.listdir( '{}/{}'.format( basepath, tag ) ) ] )

def readString( file ):
	return file.readline().strip()

def readStrings( file ):
	return file.readline().strip().split()

def readRawString( file ):
	newline = '\n'
	return file.readline().strip( newline )

def readAllStrings( file ):
	return [ line.strip() for line in file.readlines() ]

def readAllIntegers( file ):
	return list( map( int, readAllStrings( file ) ) )

def readTokens( file ):
	return readString( file ).split()

def readInteger( file ):
	return int( readString( file ) )

def readIntegers( file ):
	return map( int, file.readline().strip().split() )

'''
Google Kick Start Round A 2021 - "Rabbit House"
'''
class RabbitHouse:
	def __init__( self, areaLayout ):
		self.rows, self.cols = len( areaLayout ), len( areaLayout[ 0 ] )
		self.areaLayout = areaLayout
		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

	def minimumBoxes( self ):
		boxes = 0

		q = deque()
		for u in range( self.rows ):
			for v in range( self.cols ):
				q.append( (u, v) )

		while len( q ) > 0:
			u, v = q.popleft()
			for du, dv in self.adjacentCellDelta:
				x, y = u + du, v + dv
				if not 0 <= x < self.rows or not 0 <= y < self.cols:
					continue
				if self.areaLayout[ u ][ v ] > 1 + self.areaLayout[ x ][ y ]:
					boxes += self.areaLayout[ u ][ v ] - self.areaLayout[ x ][ y ] - 1
					self.areaLayout[ x ][ y ] = self.areaLayout[ u ][ v ] - 1
					q.append( (x, y) )
		return boxes

class RabbitHouseTest( unittest.TestCase ):
	def test_RabbitHouse( self ):
		self._verify( 'ts1_input', 'ts1_output' )
		self._verify( 'ts2_input', 'ts2_output' )

	def _verify( self, inputfileName, outputfileName ):
		with open( 'tests-kickstart/rabbithouse/{}.txt'.format( inputfileName ) ) as inputFile, \
		     open( 'tests-kickstart/rabbithouse/{}.txt'.format( outputfileName ) ) as solutionFile:
			
			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				rows, cols = readIntegers( inputFile )
				areaLayout = [ list( readIntegers( inputFile ) ) for _ in range( rows ) ]

				solutionHeader = readString( solutionFile )

				print( 'Testcase {}#{} rows = {} cols = {} [{}]'.format( inputfileName, t + 1, rows, cols, solutionHeader ) )
				minimumBoxes = RabbitHouse( areaLayout ).minimumBoxes()
				self.assertEqual( 'Case #{}: {}'.format( t + 1, minimumBoxes ), solutionHeader )

	def test_RabbitHouse_Sample( self ):
		areaLayout = [
		[ 3, 4, 3 ]
		]
		self.assertEqual( RabbitHouse( areaLayout ).minimumBoxes(), 0 )

		areaLayout = [
		[ 3, 0, 0 ]
		]
		self.assertEqual( RabbitHouse( areaLayout ).minimumBoxes(), 3 )

		areaLayout = [
		[ 0, 0, 0 ],
		[ 0, 2, 0 ],
		[ 0, 0, 0 ]
		]
		self.assertEqual( RabbitHouse( areaLayout ).minimumBoxes(), 4 )

'''
Google Kick Start Round F 2021 - "Trash Bins"
'''
class Bins:
	@staticmethod
	def totalDistance( inputString ):
		binLocationList = list()
		for index, character in enumerate( inputString ):
			if character == '1':
				binLocationList.append( index )
		binLocationList.append( float( 'inf' ) )

		r = 0
		totalDistance_ = 0
		for index in range( len( inputString ) ):
			if index == binLocationList[ r ]:
				r += 1
			left = abs( index - binLocationList[ r - 1 ] )
			right = abs( index - binLocationList[ r ] )
			totalDistance_ += min( left, right )
		return totalDistance_

class BinsTest( unittest.TestCase ):
	def test_Bins( self ):
		self._verify( 'ts1_input', 'ts1_output' )
		self._verify( 'ts2_input', 'ts2_output' )

	def _verify( self, inputfileName, outputfileName ):
		with open( 'tests-kickstart/bins/{}.txt'.format( inputfileName ) ) as inputFile, \
		     open( 'tests-kickstart/bins/{}.txt'.format( outputfileName ) ) as solutionFile:
			
			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				N = readInteger( inputFile )
				inputString = readString( inputFile )

				solutionHeader = readString( solutionFile )
				totalDistance = Bins.totalDistance( inputString )

				print( 'Testcase {}#{} N = {} [{}]'.format( inputfileName, t + 1, N, totalDistance ) )
				self.assertEqual( 'Case #{}: {}'.format( t + 1, totalDistance ), solutionHeader )

	def test_Bins_Sample( self ):
		inputString = '111'
		self.assertEqual( Bins.totalDistance( inputString ), 0 )

		inputString = '100100'
		self.assertEqual( Bins.totalDistance( inputString ), 5 )

'''
Google Kick Start Round F 2021 - "Festival"
'''
class Festival:
	@staticmethod
	def maximumHappiness( K, attractionList ):
		attractions = list()
		for happinessScore, startTime, endTime in attractionList:
			attractions.append( (startTime, 'BEGIN', happinessScore) )
			attractions.append( (endTime, 'END', happinessScore) )
		attractions.sort()

		maximumHappiness_ = - float( 'inf' )
		sortedScoreList = SortedList()
		runningSum = 0
		
		for timeStamp, eventType, happinessScore in attractions:
			j = sortedScoreList.bisect_left( happinessScore )
			if eventType == 'BEGIN':
				if len( sortedScoreList ) - j < K:
					runningSum += happinessScore
					if len( sortedScoreList ) >= K:
						runningSum -= sortedScoreList[ -K ]
				sortedScoreList.add( happinessScore )
			elif eventType == 'END':
				if len( sortedScoreList ) - j <= K:
					runningSum -= happinessScore
					if len( sortedScoreList ) > K:
						runningSum += sortedScoreList[ -K - 1 ]
				sortedScoreList.remove( happinessScore )
			maximumHappiness_ = max( maximumHappiness_, runningSum )
		
		return maximumHappiness_

class FestivalTest( unittest.TestCase ):
	def test_Festival( self ):
		self._verify( 'ts1_input', 'ts1_output' )
		self._verify( 'ts2_input', 'ts2_output' )

	def _verify( self, inputfileName, outputfileName ):
		with open( 'tests-kickstart/festival/{}.txt'.format( inputfileName ) ) as inputFile, \
		     open( 'tests-kickstart/festival/{}.txt'.format( outputfileName ) ) as solutionFile:
			
			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				_, attractions, K = readIntegers( inputFile )
				attractionList = [ tuple( readIntegers( inputFile ) ) for _ in range( attractions ) ]
				solutionHeader = readString( solutionFile )

				print( 'Testcase {}#{} Attractions = {} K = {} [{}]'.format( inputfileName, t + 1, attractions, K, solutionHeader ) )
				maximumHappiness = Festival.maximumHappiness( K, attractionList )
				self.assertEqual( 'Case #{}: {}'.format( t + 1, maximumHappiness ), solutionHeader )

	def test_Festival_Sample( self ):
		K = 2
		attractionList = [ (800, 2, 8), (1500, 6, 9), (200, 4, 7), (400, 3, 5) ]
		self.assertEqual( Festival.maximumHappiness( K, attractionList ), 2300 )

		K = 3
		attractionList = [ (400, 1, 3), (500, 5, 5), (300, 2, 3) ]
		self.assertEqual( Festival.maximumHappiness( K, attractionList ), 700 )

'''
Google Kick Start Round E 2021 - "Palindromic Crossword"
'''
class PalindromicCrossword:
	def __init__( self, areaLayout ):
		self.rows, self.cols = len( areaLayout ), len( areaLayout[ 0 ] )
		self.areaLayout = [ list( areaLayoutRow ) for areaLayoutRow in areaLayout ]
		self.emptyCell, self.blockedCell = '.#'

	def fill( self ):
		crosswordTable = [ [ (None, None) for _ in range( self.cols ) ] for _ in range( self.rows ) ]
		def _add( r1, c1, r2, c2 ):
			cell1, cell2 = crosswordTable[ r1 ][ c1 ]
			if cell1 == None:
				cell1 = r2, c2
			else:
				cell2 = r2, c2
			crosswordTable[ r1 ][ c1 ] = cell1, cell2

		def _addToTable( r1, c1, r2, c2 ):
			_add( r1, c1, r2, c2 )
			_add( r2, c2, r1, c1 )
		
		evaluationQ = deque()

		for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
			if self.areaLayout[ u ][ v ] == self.blockedCell:
				continue
			if v == 0 or self.areaLayout[ u ][ v - 1 ] == self.blockedCell:
				c1, c2 = v, v
				while c2 < self.cols and self.areaLayout[ u ][ c2 ] != self.blockedCell:
					c2 = c2 + 1
				c2 = c2 - 1
				while c1 < c2:
					_addToTable( u, c1, u, c2 )
					if self.areaLayout[ u ][ c1 ].isalpha() and self.areaLayout[ u ][ c2 ] == self.emptyCell:
						evaluationQ.append( (u, c1) )
					elif self.areaLayout[ u ][ c1 ] == self.emptyCell and self.areaLayout[ u ][ c2 ].isalpha():
						evaluationQ.append( (u, c2) )
					c1, c2 = c1 + 1, c2 - 1
			
			if u == 0 or self.areaLayout[ u - 1 ][ v ] == self.blockedCell:
				r1, r2 = u, u
				while r2 < self.rows and self.areaLayout[ r2 ][ v ] != self.blockedCell:
					r2 = r2 + 1
				r2 = r2 - 1
				while r1 < r2:
					_addToTable( r1, v, r2, v )
					if self.areaLayout[ r1 ][ v ].isalpha() and self.areaLayout[ r2 ][ v ] == self.emptyCell:
						evaluationQ.append( (r1, v) )
					elif self.areaLayout[ r1 ][ v ] == self.emptyCell and self.areaLayout[ r2 ][ v ].isalpha():
						evaluationQ.append( (r2, v) )
					r1, r2 = r1 + 1, r2 - 1

		count = 0
		while len( evaluationQ ) > 0:
			u, v = evaluationQ.popleft()
			for cell in crosswordTable[ u ][ v ]:
				if cell == None:
					continue
				x, y = cell
				if self.areaLayout[ x ][ y ] == self.emptyCell:
					count += 1
					self.areaLayout[ x ][ y ] = self.areaLayout[ u ][ v ]
					evaluationQ.append( cell )
		return count, [ ''.join( areaLayoutRow ) for areaLayoutRow in self.areaLayout ]

class PalindromicCrosswordTest( unittest.TestCase ):
	def test_PalindromicCrossword( self ):
		self._verify( 'ts1_input', 'ts1_output' )
		self._verify( 'ts2_input', 'ts2_output' )

	def _verify( self, inputfileName, outputfileName ):
		with open( 'tests-kickstart/crossword/{}.txt'.format( inputfileName ) ) as inputFile, \
		     open( 'tests-kickstart/crossword/{}.txt'.format( outputfileName ) ) as solutionFile:
			
			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				rows, cols = readIntegers( inputFile )
				areaLayout = [ readString( inputFile ) for _ in range( rows ) ]

				solutionHeader = readString( solutionFile )
				filledCrosswordSolution = [ readString( solutionFile ) for _ in range( rows ) ]

				print( 'Testcase {}#{} rows = {} cols = {} [{}]'.format( inputfileName, t + 1, rows, cols, solutionHeader ) )
				count, filledCrossword = PalindromicCrossword( areaLayout ).fill()

				self.assertEqual( 'Case #{}: {}'.format( t + 1, count ), solutionHeader )
				self.assertEqual( filledCrossword, filledCrosswordSolution )

	def test_PalindromicCrossword_Sample( self ):
		areaLayout = [
		'A.',
		'.#'
		]
		filledCrossword = [
		'AA',
		'A#'
		]
		self.assertEqual( PalindromicCrossword( areaLayout ).fill(), (2, filledCrossword) )

		areaLayout = [
		'A...#.',
		'B##...',
		'B.###.',
		'A...#.'
		]
		filledCrossword = [
		'A..A#.',
		'B##A.A',
		'BB###A',
		'ABBA#.'
		]
		self.assertEqual( PalindromicCrossword( areaLayout ).fill(), (8, filledCrossword) )

'''
Google Kick Start Round A 2021 - "K-Goodness String"
'''
class KGoodnessString:
	@staticmethod
	def minimumOperations( string, K ):
		N = len( string )
		i, j = 0, N - 1
		score = 0
		while i < j:
			if string[ i ] != string[ j ]:
				score += 1
			i = i + 1
			j = j - 1
		return abs( score - K )

class KGoodnessStringTest( unittest.TestCase ):
	def test_KGoodnessString( self ):
		self._verify( 'ts1_input', 'ts1_output' )
		self._verify( 'ts2_input', 'ts2_output' )

	def _verify( self, inputfileName, outputfileName ):
		with open( 'tests-kickstart/k-goodness/{}.txt'.format( inputfileName ) ) as inputFile, \
		     open( 'tests-kickstart/k-goodness/{}.txt'.format( outputfileName ) ) as solutionFile:
			
			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				N, K = readIntegers( inputFile )
				string = readString( inputFile )

				solutionHeader = readString( solutionFile )
				minimumOperations = KGoodnessString.minimumOperations( string, K )

				print( 'Testcase {}#{} K = {} [{}]'.format( inputfileName, t + 1, K, solutionHeader ) )
				self.assertEqual( 'Case #{}: {}'.format( t + 1, minimumOperations ), solutionHeader )

	def test_KGoodnessString_Sample( self ):
		self.assertEqual( KGoodnessString.minimumOperations( 'ABCAA', 1 ), 0 )
		self.assertEqual( KGoodnessString.minimumOperations( 'ABAA', 2 ), 1 )

'''
Google Kick Start Round A 2021 - "L Shaped Plots"
'''
class LShapedPlot:
	def __init__( self, areaLayout ):
		self.rows, self.cols = len( areaLayout ), len( areaLayout[ 0 ] )
		self.areaLayout = areaLayout

	def count( self ):
		southBound = [ [ 0 for _ in range( self.cols ) ] for _ in range( self.rows ) ]
		for u in range( self.rows ):
			for v in range( self.cols ):
				if self.areaLayout[ u ][ v ] == 0:
					southBound[ u ][ v ] = 0
				else:
					southBound[ u ][ v ] = 1 + ( 0 if u == 0 else southBound[ u - 1 ][ v ] )
		
		northBound = [ [ 0 for _ in range( self.cols ) ] for _ in range( self.rows ) ]
		for u in range( self.rows - 1, -1, -1 ):
			for v in range( self.cols ):
				if self.areaLayout[ u ][ v ] == 0:
					northBound[ u ][ v ] = 0
				else:
					northBound[ u ][ v ] = 1 + ( 0 if u + 1 == self.rows else northBound[ u + 1 ][ v ] )

		eastBound = [ [ 0 for _ in range( self.cols ) ] for _ in range( self.rows ) ]
		for v in range( self.cols ):
			for u in range( self.rows ):
				if self.areaLayout[ u ][ v ] == 0:
					eastBound[ u ][ v ] = 0
				else:
					eastBound[ u ][ v ] = 1 + ( 0 if v == 0 else eastBound[ u ][ v - 1 ] )

		westBound = [ [ 0 for _ in range( self.cols ) ] for _ in range( self.rows ) ]
		for v in range( self.cols - 1, -1, -1 ):
			for u in range( self.rows ):
				if self.areaLayout[ u ][ v ] == 0:
					westBound[ u ][ v ] = 0
				else:
					westBound[ u ][ v ] = 1 + ( 0 if v + 1 == self.cols else westBound[ u ][ v + 1 ] )

		lShapeCount = 0

		def _calculateLShape( A, B ):
			count = 0
			U, V = min( A, B ), max( A, B )
			count += max( 0, min( U, V >> 1 ) - 1 )
			count += max( 0, ( U >> 1 ) - 1 )
			return count

		for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
			calculationList = list()

			calculationList.append( (southBound[ u ][ v ], westBound[ u ][ v ]) )
			calculationList.append( (northBound[ u ][ v ], westBound[ u ][ v ]) )
			calculationList.append( (southBound[ u ][ v ], eastBound[ u ][ v ]) )
			calculationList.append( (northBound[ u ][ v ], eastBound[ u ][ v ]) )

			for A, B in calculationList:
				lShapeCount += _calculateLShape( A, B )

		return lShapeCount

class LShapedPlotTest( unittest.TestCase ):
	def test_LShapedPlot( self ):
		self._verify( 'ts1_input', 'ts1_output' )
		self._verify( 'ts2_input', 'ts2_output' )

	def _verify( self, inputfileName, outputfileName ):
		with open( 'tests-kickstart/l-shapedplot/{}.txt'.format( inputfileName ) ) as inputFile, \
		     open( 'tests-kickstart/l-shapedplot/{}.txt'.format( outputfileName ) ) as solutionFile:
			
			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				rows, cols = readIntegers( inputFile )
				areaLayout = [ list( readIntegers( inputFile ) ) for _ in range( rows ) ]

				solutionHeader = readString( solutionFile )
				count = LShapedPlot( areaLayout ).count()

				print( 'Testcase {}#{} rows = {} cols = {} [{}] count={}'.format( inputfileName, t + 1, rows, cols, solutionHeader, count ) )

	def test_LShapedPlot_Sample( self ):
		areaLayout = [
		[ 1, 0, 0 ],
		[ 1, 0, 1 ],
		[ 1, 0, 0 ],
		[ 1, 1, 0 ]
		]
		self.assertEqual( LShapedPlot( areaLayout ).count(), 1 )

		areaLayout = [
		[ 1, 0, 0, 0 ],
		[ 1, 0, 0, 1 ],
		[ 1, 1, 1, 1 ],
		[ 1, 0, 1, 0 ],
		[ 1, 0, 1, 0 ],
		[ 1, 1, 1, 0 ]
		]
		self.assertEqual( LShapedPlot( areaLayout ).count(), 9 )

class SolutionManager:
	def __init__( self ):
		self.totalSolutions = 0
		self.contestDict = dict()
		self.testClassSet = set()

	def registerSolution( self, contestName, problemName, testClass, tagList, sharedClass=False ):
		if contestName not in self.contestDict:
			self.contestDict[ contestName ] = dict()
		assert problemName not in self.contestDict[ contestName ]
		assert testClass not in self.testClassSet or sharedClass == True
		self.contestDict[ contestName ][ problemName ] = testClass
		self.testClassSet.add( testClass )
		self.totalSolutions += 1

	def printSummary( self ):
		for contestName, problemDict in self.contestDict.items():
			print( '{} : {}'.format( contestName, len( problemDict ) ) )
		print( 'Total : {}'.format( self.totalSolutions ) )

'''
Facebook Hacker Cup Qualification 2021 - "Problem A1: Consistency - Chapter 1"
'''
class Consistency:
	@staticmethod
	def _characterIndex( character ):
		return ord( character ) - ord( 'A' )

	@staticmethod
	def timeToChange( inputString ):
		characters = string.ascii_uppercase
		frequencyList = [ 0 for _ in range( len( characters ) ) ]
		for character in inputString:
			frequencyList[ Consistency._characterIndex( character ) ] += 1

		vowels = set( 'AEIOU' )
		vowelFrequencyList = list()
		consonantFrequencyList = list()
		for index, character in enumerate( characters ):
			if character in vowels:
				vowelFrequencyList.append( frequencyList[ index ] )
			else:
				consonantFrequencyList.append( frequencyList[ index ] )

		maximumFrequency = max( vowelFrequencyList )
		A = 2 * ( sum( vowelFrequencyList ) - maximumFrequency ) + sum( consonantFrequencyList )
		maximumFrequency = max( consonantFrequencyList )
		B = 2 * ( sum( consonantFrequencyList ) - maximumFrequency ) + sum( vowelFrequencyList )
		return min( A, B )

	@staticmethod
	def timeToChangeV2( inputString, replacementList ):
		characters = string.ascii_uppercase
		distanceList = [ [ float( 'inf' ) for _ in range( len( characters ) ) ] for _ in range( len( characters ) ) ]
		for character1, character2 in replacementList:
			u, v = Consistency._characterIndex( character1 ), Consistency._characterIndex( character2 )
			distanceList[ u ][ v ] = 1

		for k in range( len( characters ) ):
			for u in range( len( characters ) ):
				for v in range( len( characters ) ):
					if u == v:
						distanceList[ u ][ v ] = 0
						continue
					distanceList[ u ][ v ] = min( distanceList[ u ][ v ], distanceList[ u ][ k ] + distanceList[ k ][ v ] )

		timeToChange = float( 'inf' )

		for v in range( len( characters ) ):
			timeTaken = 0
			for character in inputString:
				u = Consistency._characterIndex( character )
				timeTaken += distanceList[ u ][ v ]
			timeToChange = min( timeToChange, timeTaken )
		return -1 if timeToChange == float( 'inf' ) else timeToChange

class ConsistencyTest( unittest.TestCase ):
	def test_Consistency_SampleV2( self ):
		testcases = [
		('ABC', [ 'BA', 'CA' ], 2),
		('ABC', [ 'AB', 'AC' ], -1),
		( 'F', [], 0),
		('BANANA', [ 'AB', 'AN', 'BA', 'NA' ], 3),
		('FBHC', [ 'FB', 'BF', 'HC', 'CH' ], -1),
		('FOXEN', [ 'NI', 'OE', 'NX', 'EW', 'OI', 'FE', 'FN', 'XW' ], 8),
		('CONSISTENCY', [ 'AB', 'BC', 'CD', 'DE', 'EF', 'FG', 'GH', 'HI', 'IJ', 'JK', 'KL', 'LM', 'MN',
		                  'NO', 'OP', 'PQ', 'QR', 'RS', 'ST', 'TU', 'UV', 'VW', 'WX', 'XY', 'YZ', 'ZA' ], 100)
		]
		for inputString, replacementList, timeToChange in testcases:
			self.assertEqual( Consistency.timeToChangeV2( inputString, replacementList ), timeToChange )

	def test_ConsistencyV2( self ):
		with open( 'tests-fb/consistency/consistency_ch2.in' ) as inputFile, \
		     open( 'tests-fb/consistency/consistency_ch2.out' ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				inputString = readString( inputFile )
				replacements = readInteger( inputFile )
				replacementList = [ readString( inputFile ) for _ in range( replacements ) ]
				solutionHeader = readString( solutionFile )

				timeToChange = Consistency.timeToChangeV2( inputString, replacementList )
				print( 'Testcase #{} [{}] --> [{}]'.format( t + 1, inputString, timeToChange ) )
				self.assertEqual( 'Case #{}: {}'.format( t + 1, timeToChange ), solutionHeader )

	def test_Consistency_Sample( self ):
		testcases = [ ('ABC', 2), ('F', 0), ('BANANA', 3), ('FBHC', 4), ('FOXEN', 5), ('CONSISTENCY', 12) ]
		for inputString, timeToChange in testcases:
			self.assertEqual( Consistency.timeToChange( inputString ), timeToChange )

	def test_Consistency( self ):
		with open( 'tests-fb/consistency/consistency_ch1.in' ) as inputFile, \
		     open( 'tests-fb/consistency/consistency_ch1.out' ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				inputString = readString( inputFile )
				solutionHeader = readString( solutionFile )

				timeToChange = Consistency.timeToChange( inputString )
				print( 'Testcase #{} [{}] --> [{}]'.format( t + 1, inputString, timeToChange ) )
				self.assertEqual( 'Case #{}: {}'.format( t + 1, timeToChange ), solutionHeader )

'''
Facebook Hacker Cup Qualification 2021 - "Problem B: Xs and Os"
'''
class XAndO:
	def __init__( self, areaLayout ):
		self.size = len( areaLayout )
		self.areaLayout = areaLayout

	def analyze( self ):
		countOneLocations = set()
		possibilityList = list()

		for row in range( self.size ):
			count = 0
			possible = True
			countOneLocation = None
			for col in range( self.size ):
				if self.areaLayout[ row ][ col ] == 'O':
					possible = False
					break
				elif self.areaLayout[ row ][ col ] == '.':
					countOneLocation = row, col
					count += 1
			if not possible:
				continue
			if count == 1:
				countOneLocations.add( countOneLocation )
			possibilityList.append( count )

		for col in range( self.size ):
			count = 0
			possible = True
			countOneLocation = None
			for row in range( self.size ):
				if self.areaLayout[ row ][ col ] == 'O':
					possible = False
					break
				elif self.areaLayout[ row ][ col ] == '.':
					countOneLocation = row, col
					count += 1
			if not possible:
				continue
			if count == 1:
				countOneLocations.add( countOneLocation )
			possibilityList.append( count )

		if len( possibilityList ) == 0:
			return 'Impossible'
		minimumEntries = min( possibilityList )
		if minimumEntries == 1:
			return '{} {}'.format( minimumEntries, len( countOneLocations ) )
		else:
			return '{} {}'.format( minimumEntries, possibilityList.count( minimumEntries ) )

class XAndOTest( unittest.TestCase ):
	def test_XAndO_Sample( self ):
		areaLayout = [
		'XO',
		'..'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '1 1' )

		areaLayout = [
		'X.',
		'.O'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '1 2' )

		areaLayout = [
		'...',
		'...',
		'...'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '3 6' )

		areaLayout = [
		'.OX',
		'X..',
		'..O'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '2 2' )

		areaLayout = [
		'OXO',
		'X.X',
		'OXO'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '1 1' )

		areaLayout = [
		'.XO',
		'O.X',
		'XO.'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), 'Impossible' )

		areaLayout = [
		'X...',
		'.O.O',
		'.XX.',
		'O.XO'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '2 2' )

		areaLayout = [
		'OX.OO',
		'X.XXX',
		'OXOOO',
		'OXOOO',
		'XXXX.'
		]
		self.assertEqual( XAndO( areaLayout ).analyze(), '1 2' )

	def test_XAndO( self ):
		with open( 'tests-fb/xando/xs_and_os.in' ) as inputFile, \
		     open( 'tests-fb/xando/xs_and_os.out' ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for t in range( testcaseCount ):
				size = readInteger( inputFile )
				areaLayout = [ readString( inputFile ) for _ in range( size ) ]
				solutionHeader = readString( solutionFile )

				print( 'Testcase #{} size = {} --> [{}]'.format( t + 1, size, solutionHeader ) )
				self.assertEqual( 'Case #{}: {}'.format( t + 1, XAndO( areaLayout ).analyze() ), solutionHeader )

def registerContestSolutions( solutionManager ):
	solutionManager.registerSolution( 'Google Kick Start Round A 2021', 'K-Goodness String', KGoodnessStringTest, [] )
	solutionManager.registerSolution( 'Google Kick Start Round A 2021', 'L Shaped Plots', LShapedPlotTest, [] )
	solutionManager.registerSolution( 'Google Kick Start Round A 2021', 'Rabbit House', RabbitHouseTest, [] )
	solutionManager.registerSolution( 'Google Kick Start Round E 2021', 'Palindromic Crossword', PalindromicCrosswordTest, [] )
	solutionManager.registerSolution( 'Google Kick Start Round F 2021', 'Trash Bins', BinsTest, [] )
	solutionManager.registerSolution( 'Google Kick Start Round F 2021', 'Festival', FestivalTest, [] )

	solutionManager.registerSolution( 'Facebook Hacker Cup Qualification 2021', 'Consistency - Chapter 1', ConsistencyTest, [] )
	solutionManager.registerSolution( 'Facebook Hacker Cup Qualification 2021', 'Consistency - Chapter 2', ConsistencyTest, [], sharedClass=True )
	solutionManager.registerSolution( 'Facebook Hacker Cup Qualification 2021', 'Xs and Os', XAndOTest, [] )

if __name__ == '__main__':
	solutionManager = SolutionManager()
	registerContestSolutions( solutionManager )
	solutionManager.printSummary()

	unittest.main()