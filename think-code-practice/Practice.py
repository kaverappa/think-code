from audioop import reverse
from datetime import time
from logging import StrFormatStyle
from re import U
from sys import _current_frames, path
from typing import List
from typing import Optional
from collections import defaultdict, deque
import unittest
import itertools
import heapq
import string
from collections import Counter
import random

'''
Shortest Path in a Grid with Obstacles Elimination

You are given an m x n integer matrix grid where each cell is either 0 (empty) or 1 (obstacle). You can move up, down, left, or right from and to an empty cell in one step.

Return the minimum number of steps to walk from the upper left corner (0, 0) to the lower right corner (m - 1, n - 1) given that you can eliminate at most k obstacles. If it is not possible to find such walk return -1.

Input: 
grid = 
[[0,0,0],
 [1,1,0],
 [0,0,0],
 [0,1,1],
 [0,0,0]], 
k = 1
Output: 6
Explanation: 
The shortest path without eliminating any obstacle is 10. 
The shortest path with one obstacle elimination at position (3,2) is 6. Such path is (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (3,2) -> (4,2).

Input: 
grid = 
[[0,1,1],
 [1,1,1],
 [1,0,0]], 
k = 1
Output: -1
Explanation: 
We need to eliminate at least two obstacles to find such a walk.
'''

class Obstacle:
    def shortestPath(self, grid: List[List[int]], k: int) -> int:
        adjacentCellsDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]
        rows, cols = len( grid ), len( grid[ 0 ] )
        obstacle = 1

        q = deque()
        q.append( (0, 0, 0) )

        visited = [ [ [ False for _ in range( k + 1 ) ] for _ in range( cols ) ] for _ in range( rows ) ]
        visited[ 0 ][ 0 ][ 0 ] = True

        distance = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                x, y, obstaclesEliminated = q.popleft()
                if x == rows - 1 and y == cols - 1:
                    return distance
                for du, dv in adjacentCellsDelta:
                    u, v = x + du, y + dv
                    if not 0 <= u < rows or not 0 <= v < cols:
                        continue
                    newObstaclesEliminated = obstaclesEliminated if grid[ u ][ v ] != obstacle else obstaclesEliminated + 1
                    if newObstaclesEliminated > k:
                        continue
                    if not visited[ u ][ v ][ newObstaclesEliminated ]:
                        visited[ u ][ v ][ newObstaclesEliminated ] = True
                        q.append( (u, v, newObstaclesEliminated) )
            distance += 1
        return -1

class ObstacleTest( unittest.TestCase ):
    def test_Obstacle_Sample( self ):
        grid = [
            [0,0,0],
            [1,1,0],
            [0,0,0],
            [0,1,1],
            [0,0,0]
        ]
        k = 1
        self.assertEqual( Obstacle().shortestPath( grid, k ), 6 )

        grid = [
            [0,1,1],
            [1,1,1],
            [1,0,0]
        ]
        k = 1
        self.assertEqual( Obstacle().shortestPath( grid, k ), -1 )

'''
Number of Provinces

There are n cities. Some of them are connected, while some are not. If city a is connected directly with city b, and city b is connected directly with city c, then city a is connected indirectly with city c.

A province is a group of directly or indirectly connected cities and no other cities outside of the group.

You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the ith city and the jth city are directly connected, and isConnected[i][j] = 0 otherwise.

Return the total number of provinces.
'''

class Province:
    def findCircleNum(self, isConnected: List[List[int]]) -> int:
        numberOfCities = len( isConnected )
        visited = [ False for _ in range( numberOfCities ) ]

        def _dfs( city ):
            stack = list()
            stack.append( city )

            while len( stack ) > 0:
                city = stack.pop()
                if not visited[ city ]:
                    visited[ city ] = True
                    for v in range( numberOfCities ):
                        if isConnected[ city ][ v ] == 1 and not visited[ v ]:
                            stack.append( v )

        provinceCount = 0
        for city in range( numberOfCities ):
            if not visited[ city ]:
                _dfs( city )
                provinceCount += 1
        return provinceCount

class ProvinceTest( unittest.TestCase ):
    def test_Province_Sample( self ):
        isConnected = [[1,1,0],[1,1,0],[0,0,1]]
        self.assertEqual( Province().findCircleNum( isConnected ), 2 )

        isConnected = [[1,0,0],[0,1,0],[0,0,1]]
        self.assertEqual( Province().findCircleNum( isConnected ), 3 )

'''
Graph Valid Tree

You have a graph of n nodes labeled from 0 to n - 1. You are given an integer n and a list of edges where edges[i] = [ai, bi] indicates that there is an undirected edge between nodes ai and bi in the graph.

Return true if the edges of the given graph make up a valid tree, and false otherwise.
'''

class GraphValidTree:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:
        visited = [ False for _ in range( n ) ]
        
        graph = [ list() for _ in range( n ) ]
        for (u, v) in edges:
            graph[ u ].append( v )
            graph[ v ].append( u )

        def _dfs( vertex, previousVertex ):
            stack = list()
            stack.append( (vertex, previousVertex) )

            while len( stack ) > 0:
                vertex, previousVertex = stack.pop()
                visited[ vertex ] = True
                for adjacentVertex in graph[ vertex ]:
                    if adjacentVertex == previousVertex:
                        continue
                    if visited[ adjacentVertex ]:
                        return True
                    stack.append( (adjacentVertex, vertex) )
            return False

        cyclePresent = _dfs( vertex=0, previousVertex=None )
        return not cyclePresent and all( visited )

class GraphValidTreeTest( unittest.TestCase ):
    def test_GraphValidTree_Sample( self ):
        edges = [[0,1],[0,2],[0,3],[1,4]]
        n = 5
        self.assertEqual( GraphValidTree().validTree( n, edges ), True )

        edges = edges = [[0,1],[1,2],[2,3],[1,3],[1,4]]
        n = 5
        self.assertEqual( GraphValidTree().validTree( n, edges ), False )

class UnionFind:
    def __init__( self ):
        self.representativeDict = dict()
        self._disjointSetCount = 0

    def _representative( self, vertex ):
        currentVertex = vertex
        traversalList = list()
        while True:
            if currentVertex == self.representativeDict[ currentVertex ]:
                for vertex in traversalList:
                    self.representativeDict[ vertex ] = currentVertex
                return currentVertex
            traversalList.append( currentVertex )
            currentVertex = self.representativeDict[ currentVertex ]

    def disjointSetCount( self ):
        return self._disjointSetCount

    def vertexCount( self ):
        return len( self.representativeDict )

    def add( self, vertex ):
        if vertex not in self.representativeDict:
            self.representativeDict[ vertex ] = vertex
            self._disjointSetCount += 1
            return True
        return False

    def inSameComponent( self, vertexA, vertexB ):
        return self._representative( vertexA ) == self._representative( vertexB )

    def merge( self, vertexA, vertexB ):
        representativeA = self._representative( vertexA )
        representativeB = self._representative( vertexB )
        if representativeA != representativeB:
            self.representativeDict[ representativeA ] = representativeB
            self._disjointSetCount -= 1
            return True
        return False

'''
The Earliest Moment When Everyone Become Friends

There are n people in a social group labeled from 0 to n - 1. You are given an array logs where logs[i] = [timestampi, xi, yi] indicates that xi and yi will be friends at the time timestampi.

Friendship is symmetric. That means if a is friends with b, then b is friends with a. Also, person a is acquainted with a person b if a is friends with b, or a is a friend of someone acquainted with b.

Return the earliest time for which every person became acquainted with every other person. If there is no such earliest time, return -1.
'''

class EarliestMoment:
    def earliestAcq(self, logs: List[List[int]], n: int) -> int:
        uf = UnionFind()
        for vertex in range( n ):
            uf.add( vertex )

        logs.sort()
        for (timeStamp, u, v) in logs:
            uf.merge( u, v )
            if uf.disjointSetCount() == 1:
                return timeStamp
        return -1

class EarliestMomentTest( unittest.TestCase ):
    def test_EarliestMoment_Sample( self ):
        logs = [[20190101,0,1],[20190104,3,4],[20190107,2,3],[20190211,1,5],[20190224,2,4],
        [20190301,0,3],[20190312,1,2],[20190322,4,5]]
        n = 6
        self.assertEqual( EarliestMoment().earliestAcq( logs, n ), 20190301 )

        logs = [[0,2,0],[1,0,1],[3,0,3],[4,1,2],[7,3,1]]
        n = 4
        self.assertEqual( EarliestMoment().earliestAcq( logs, n ), 3 )

'''
Shortest Path in Binary Matrix

Given an n x n binary matrix grid, return the length of the shortest clear path in the matrix. If there is no clear path, return -1.

A clear path in a binary matrix is a path from the top-left cell (i.e., (0, 0)) to the bottom-right cell (i.e., (n - 1, n - 1)) such that:

All the visited cells of the path are 0.
All the adjacent cells of the path are 8-directionally connected (i.e., they are different and they share an edge or a corner).
The length of a clear path is the number of visited cells of this path.
'''

class BinaryMatrix:
    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
        size = len( grid )
        adjacentCellsDelta = list()
        for du, dv in itertools.product( [ -1, 0, 1 ], [ -1, 0, 1 ] ):
            if du == dv == 0:
                continue
            adjacentCellsDelta.append( (du, dv) )

        q = deque()
        if grid[ 0 ][ 0 ] == 0:
            q.append( (0, 0, 0) )

        while len( q ) > 0:
            u, v, visitedCellsCount = q.popleft()
            if grid[ u ][ v ] == 1:
                continue
            
            grid[ u ][ v ] = 1
            visitedCellsCount += 1
            if u == v == size - 1:
                return visitedCellsCount
            
            for du, dv in adjacentCellsDelta:
                x, y = u + du, v + dv
                if 0 <= x < size and 0 <= y < size and grid[ x ][ y ] == 0:
                    q.append( (x, y, visitedCellsCount) )
        return -1

class BinaryMatrixTest( unittest.TestCase ):
    def test_BinaryMatrix_Sample( self ):
        grid = [[0,1],[1,0]]
        self.assertEqual( BinaryMatrix().shortestPathBinaryMatrix( grid ), 2 )

        grid = [[0,0,0],[1,1,0],[1,1,0]]
        self.assertEqual( BinaryMatrix().shortestPathBinaryMatrix( grid ), 4 )

        grid = [[1,0,0],[1,1,0],[1,1,0]]
        self.assertEqual( BinaryMatrix().shortestPathBinaryMatrix( grid ), -1 )

'''
Rotting Oranges

You are given an m x n grid where each cell can have one of three values:

0 representing an empty cell,
1 representing a fresh orange, or
2 representing a rotten orange.
Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.

Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1.
'''

class Orange:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        FRESH_ORANGE, ROTTEN_ORANGE = 1, 2
        rows, cols = len( grid ), len( grid[ 0 ] )
        adjacentCellsDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

        freshOranges = 0
        q = deque()
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] == ROTTEN_ORANGE:
                q.append( (u, v) )
            elif grid[ u ][ v ] == FRESH_ORANGE:
                freshOranges += 1

        if freshOranges == 0:
            return 0

        time = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                u, v = q.popleft()
                for du, dv in adjacentCellsDelta:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and grid[ x ][ y ] == FRESH_ORANGE:
                        freshOranges -= 1
                        grid[ x ][ y ] = ROTTEN_ORANGE
                        q.append( (x, y) )
            time += 1
            if freshOranges == 0:
                return time
        return -1

class OrangeTest( unittest.TestCase ):
    def test_Orange_Sample( self ):
        grid = [[2,1,1],[1,1,0],[0,1,1]]
        self.assertEqual( Orange().orangesRotting( grid ), 4 )

        grid = [[2,1,1],[0,1,1],[1,0,1]]
        self.assertEqual( Orange().orangesRotting( grid ), -1 )

        grid = [[0,2]]
        self.assertEqual( Orange().orangesRotting( grid ), 0 )

'''
Network Delay Time

You are given a network of n nodes, labeled from 1 to n. You are also given times, a list of travel times as directed edges times[i] = (ui, vi, wi), where ui is the source node, vi is the target node, and wi is the time it takes for a signal to travel from source to target.

We will send a signal from a given node k. Return the time it takes for all the n nodes to receive the signal. If it is impossible for all the n nodes to receive the signal, return -1.
'''

class NetworkDelay:
    def networkDelayTime(self, times: List[List[int]], n: int, k: int) -> int:
        graph = [ list() for _ in range( n + 1 ) ]
        for (u, v, timeTaken) in times:
            graph[ u ].append( (v, timeTaken) )

        timeTakenList = [ float( 'inf' ) for _ in range( n + 1 ) ]
        # vertex 0 is unused. Mark the distance to vertex 0 as 0.
        timeTakenList[ 0 ] = 0

        timeTakenList[ k ] = 0
        q = list()
        q.append( (0, k) )

        while len( q ) > 0:
            timeTaken, vertex = heapq.heappop( q )
            if timeTaken > timeTakenList[ vertex ]:
                continue
            for adjacentVertex, additionalTimeTaken in graph[ vertex ]:
                totalTimeTaken = timeTaken + additionalTimeTaken
                if timeTakenList[ adjacentVertex ] > totalTimeTaken:
                    timeTakenList[ adjacentVertex ] = totalTimeTaken
                    heapq.heappush( q, (totalTimeTaken, adjacentVertex) )

        totalTimeTaken = max( timeTakenList )
        return -1 if totalTimeTaken == float( 'inf' ) else totalTimeTaken

class NetworkDelayTime( unittest.TestCase ):
    def test_NetworkDelay_Sample( self ):
        times = [[2,1,1],[2,3,1],[3,4,1]]
        n, k = 4, 2
        self.assertEqual( NetworkDelay().networkDelayTime( times, n, k ), 2 )

        times = [[1,2,1]]
        n, k = 2, 1 
        self.assertEqual( NetworkDelay().networkDelayTime( times, n, k ), 1 )

        times = [[1,2,1]]
        n, k = 2, 2
        self.assertEqual( NetworkDelay().networkDelayTime( times, n, k ), -1 )

'''
Cheapest Flights Within K Stops

There are n cities connected by some number of flights. You are given an array flights where flights[i] = [fromi, toi, pricei] indicates that there is a flight from city fromi to city toi with cost pricei.

You are also given three integers src, dst, and k, return the cheapest price from src to dst with at most k stops. If there is no such route, return -1.
'''

class Flight:
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:
        costTable = [ float( 'inf' ) for _ in range( n ) ]
        costTable[ src ] = 0
        
        for _ in range( k + 1 ):
            newCostTable = costTable[ : ]
            for (fromCity, toCity, price) in flights:
                possibleCost = costTable[ fromCity ] + price
                newCostTable[ toCity ] = min( newCostTable[ toCity ], possibleCost )
            costTable = newCostTable
        return -1 if costTable[ dst ] == float( 'inf' ) else costTable[ dst ]

class FlightTest( unittest.TestCase ):
    def test_Flight_Sample( self ):
        flights = [[0,1,100],[1,2,100],[0,2,500]]
        n, src, dst, k = 3, 0, 2, 1
        self.assertEqual( Flight().findCheapestPrice( n, flights, src, dst, k ), 200 )

        flights = [[0,1,100],[1,2,100],[0,2,500]]
        n, src, dst, k = 3, 0, 2, 0
        self.assertEqual( Flight().findCheapestPrice( n, flights, src, dst, k ), 500 )

'''
Path With Minimum Effort

You are a hiker preparing for an upcoming hike. You are given heights, a 2D array of size rows x columns, where heights[row][col] represents the height of cell (row, col). You are situated in the top-left cell, (0, 0), and you hope to travel to the bottom-right cell, (rows-1, columns-1) (i.e., 0-indexed). You can move up, down, left, or right, and you wish to find a route that requires the minimum effort.

A route's effort is the maximum absolute difference in heights between two consecutive cells of the route.

Return the minimum effort required to travel from the top-left cell to the bottom-right cell.
'''

class Effort:
    def minimumEffortPath(self, heights: List[List[int]]) -> int:
        adjacentCellsDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]
        rows, cols = len( heights ), len( heights[ 0 ] )

        effortTable = [ [ float( 'inf' ) for _ in range( cols ) ] for _ in range( rows ) ]
        effortTable[ 0 ][ 0 ] = 0

        q = list()
        q.append( (0, 0, 0) )

        while len( q ) > 0:
            effort, u, v = heapq.heappop( q )
            if u == rows - 1 and v == cols - 1:
                return effort

            if effort > effortTable[ u ][ v ]:
                continue
            for du, dv in adjacentCellsDelta:
                x, y = u + du, v + dv
                if not 0 <= x < rows or not 0 <= y < cols:
                    continue
                totalEffort = max( effort, abs( heights[ x ][ y ] - heights[ u ][ v ] ) )
                if totalEffort < effortTable[ x ][ y ]:
                    effortTable[ x ][ y ] = totalEffort
                    heapq.heappush( q, (totalEffort, x, y) )

class EffortTest( unittest.TestCase ):
    def test_Effort_Sample( self ):
        heights = [[1,2,2],[3,8,2],[5,3,5]]
        self.assertEqual( Effort().minimumEffortPath( heights ), 2 )

        heights = [[1,2,3],[3,8,4],[5,3,5]]
        self.assertEqual( Effort().minimumEffortPath( heights ), 1 )

        heights = [[1,2,1,1,1],[1,2,1,2,1],[1,2,1,2,1],[1,2,1,2,1],[1,1,1,2,1]]
        self.assertEqual( Effort().minimumEffortPath( heights ), 0 )

'''
Course Schedule II

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return the ordering of courses you should take to finish all courses. If there are many valid answers, return any of them. If it is impossible to finish all courses, return an empty array.
'''

class TopologicalSort:
    @staticmethod
    def toposort( graph ):
        vertexCount = len( graph )

        if isinstance( graph, dict ):
            vertexList = list( graph.keys() )
            visited = dict()
            for vertex in vertexList:
                visited[ vertex ] = None
        elif isinstance( graph, list ):
            vertexList = list( range( vertexCount ) )
            visited = [ None for _ in range( vertexCount ) ]

        def _dfs( vertex, toposortList ):
            stack = list()
            stack.append( vertex )

            while len( stack ) > 0:
                vertex = stack.pop()
                if visited[ vertex ] is None:
                    visited[ vertex ] = 'VISITING'
                    stack.append( vertex )
                    for adjacentVertex in graph[ vertex ]:
                        if visited[ adjacentVertex ] is None:
                            stack.append( adjacentVertex )
                        elif visited[ adjacentVertex ] == 'VISITING':
                            return True
                elif visited[ vertex ] == True:
                    continue
                elif visited[ vertex ] == 'VISITING':
                    visited[ vertex ] = True
                    toposortList.append( vertex )
            return False

        toposortList = list()
        for vertex in vertexList:
            if visited[ vertex ] is None:
                cyclePresent = _dfs( vertex, toposortList )
                if cyclePresent:
                    toposortList.clear()
                    return toposortList
        return toposortList

class CourseScheduleII:
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        graph = [ list() for _ in range( numCourses ) ]
        for (course, prerequisiteCourse) in prerequisites:
            graph[ course ].append( prerequisiteCourse )
        return TopologicalSort.toposort( graph )

class CourseScheduleIITest( unittest.TestCase ):
    def test_CourseSchedule_Sample( self ):
        numCourses = 2
        prerequisites = [[1,0]]
        self.assertEqual( CourseScheduleII().findOrder( numCourses, prerequisites ), [ 0, 1 ] )

        numCourses = 4
        prerequisites = [[1,0],[2,0],[3,1],[3,2]]
        schedule = CourseScheduleII().findOrder( numCourses, prerequisites )
        self.assertTrue( schedule == [ 0, 1, 2, 3 ] or schedule == [ 0, 2, 1, 3 ] )

        numCourses = 1
        prerequisites = []
        self.assertEqual( CourseScheduleII().findOrder( numCourses, prerequisites ), [ 0 ] )

'''
Alien Dictionary

There is a new alien language that uses the English alphabet. However, the order among the letters is unknown to you.

You are given a list of strings words from the alien language's dictionary, where the strings in words are sorted lexicographically by the rules of this new language.

Return a string of the unique letters in the new alien language sorted in lexicographically increasing order by the new language's rules. If there is no solution, return "". If there are multiple solutions, return any of them.

A string s is lexicographically smaller than a string t if at the first letter where they differ, the letter in s comes before the letter in t in the alien language. If the first min(s.length, t.length) letters are the same, then s is smaller if and only if s.length < t.length.
'''

class AlienDictionary:
    def alienOrder(self, words: List[str]) -> str:
        graph = dict()

        for word in words:
            for character in word:
                if character not in graph:
                    graph[ character ] = list()

        for index in range( 1, len( words ) ):
            relationFound = False
            for i in range( min( len( words[ index ] ), len( words[ index - 1 ] ) ) ):
                before, after = words[ index - 1 ][ i ], words[ index ][ i ]
                if before == after:
                    continue
                graph[ after ].append( before )
                relationFound = True
                break
            if not relationFound and len( words[ index ] ) < len( words[ index - 1 ] ):
                return ''
        toposortList = TopologicalSort.toposort( graph )
        return ''.join( toposortList )

class AlienDictionaryTest( unittest.TestCase ):
    def test_AlienDictionary_Sample( self ):
        words = ["wrt","wrf","er","ett","rftt"]
        self.assertEqual( AlienDictionary().alienOrder( words ), "wertf" )

        words = ["z","x"]
        self.assertEqual( AlienDictionary().alienOrder( words ), "zx" )

        words = ["z","x","z"]
        self.assertEqual( AlienDictionary().alienOrder( words ), "" )

'''
Parallel Courses

You are given an integer n, which indicates that there are n courses labeled from 1 to n. You are also given an array relations where relations[i] = [prevCoursei, nextCoursei], representing a prerequisite relationship between course prevCoursei and course nextCoursei: course prevCoursei has to be taken before course nextCoursei.

In one semester, you can take any number of courses as long as you have taken all the prerequisites in the previous semester for the courses you are taking.

Return the minimum number of semesters needed to take all courses. If there is no way to take all the courses, return -1.
'''
class ParallelCourses:
    def minimumSemesters(self, n: int, relations: List[List[int]]) -> int:
        incomingEdgesCount = [ 0 for _ in range( n + 1 ) ]
        outgoingEdges = [ list() for _ in range( n + 1 ) ]
        
        for previouseCourse, nextCourse in relations:
            incomingEdgesCount[ nextCourse ] += 1
            outgoingEdges[ previouseCourse ].append( nextCourse )

        coursesTaken = 0
        q = deque()
        for course in range( 1, n + 1 ):
            if incomingEdgesCount[ course ] == 0:
                q.append( course )

        semesters = 0
        while len( q ) > 0:
            N = len( q )
            semesters += 1
            for _ in range( N ):
                course = q.popleft()
                coursesTaken += 1

                for possibleNextCourse in outgoingEdges[ course ]:
                    incomingEdgesCount[ possibleNextCourse ] -= 1
                    if incomingEdgesCount[ possibleNextCourse ] == 0:
                        q.append( possibleNextCourse )
        return semesters if coursesTaken == n else -1

class ParallelCoursesTest( unittest.TestCase ):
    def test_ParallelCourses_Sample( self ):
        n = 3
        relations = [[1,3],[2,3]]
        self.assertEqual( ParallelCourses().minimumSemesters( n, relations ), 2 )

        n = 3
        relations = [[1,2],[2,3],[3,1]]
        self.assertEqual( ParallelCourses().minimumSemesters( n, relations ), -1 )

'''
Minimum Height Trees

A tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.

Given a tree of n nodes labelled from 0 to n - 1, and an array of n - 1 edges where edges[i] = [ai, bi] indicates that there is an undirected edge between the two nodes ai and bi in the tree, you can choose any node of the tree as the root. When you select a node x as the root, the result tree has height h. Among all possible rooted trees, those with minimum height (i.e. min(h))  are called minimum height trees (MHTs).

Return a list of all MHTs' root labels. You can return the answer in any order.

The height of a rooted tree is the number of edges on the longest downward path between the root and a leaf.
'''

class MinimumHeightTree:
    def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> List[int]:
        graph = [ set() for _ in range( n ) ]
        for (u, v) in edges:
            graph[ u ].add( v )
            graph[ v ].add( u )

        q = deque()
        for vertex in range( n ):
            if len( graph[ vertex ] ) <= 1:
                q.append( vertex )

        remainingVertices = n
        while len( q ) > 0:
            if remainingVertices == 1 or remainingVertices == 2:
                return list( q )
            N = len( q )
            for _ in range( N ):
                remainingVertices -= 1
                vertex = q.popleft()
                toVertex = graph[ vertex ].pop()
                graph[ toVertex ].remove( vertex )
                if len( graph[ toVertex ] ) == 1:
                    q.append( toVertex )

class MinimumHeightTreeTest( unittest.TestCase ):
    def test_MinimumHeightTree_Sample( self ):
        n = 4
        edges = [[1,0],[1,2],[1,3]]
        self.assertEqual( MinimumHeightTree().findMinHeightTrees( n, edges ), [ 1 ] )

        n = 6
        edges = [[3,0],[3,1],[3,2],[3,4],[5,4]]
        self.assertEqual( MinimumHeightTree().findMinHeightTrees( n, edges ), [ 3, 4 ] )

        n = 1
        edges = []
        self.assertEqual( MinimumHeightTree().findMinHeightTrees( n, edges ), [ 0 ] )

        n = 2
        edges = [[0,1]]
        self.assertEqual( MinimumHeightTree().findMinHeightTrees( n, edges ), [ 0, 1 ] )

'''
Smallest String With Swaps

You are given a string s, and an array of pairs of indices in the string pairs where pairs[i] = [a, b] indicates 2 indices(0-indexed) of the string.

You can swap the characters at any pair of indices in the given pairs any number of times.

Return the lexicographically smallest string that s can be changed to after using the swaps.
'''

class StringSwaps:
    def smallestStringWithSwaps(self, s: str, pairs: List[List[int]]) -> str:
        characterList = list( s )

        graph = [ list() for _ in range( len( s ) ) ]
        for (i, j) in pairs:
            graph[ i ].append( j )
            graph[ j ].append( i )

        def _bfs( u, visited, connectedIndexList ):
            visited[ u ] = True
            q = deque()
            q.append( u )
            while len( q ) > 0:
                u = q.popleft()
                connectedIndexList.append( u )
                for v in graph[ u ]:
                    if not visited[ v ]:
                        visited[ v ] = True
                        q.append( v )
            return connectedIndexList

        visited = [ False for _ in range( len( s ) ) ]
        for i in range( len( s ) ):
            if not visited[ i ]:
                connectedIndexList = list()
                _bfs( i, visited, connectedIndexList )
                if len( connectedIndexList ) > 1:
                    swapCharacterList = [ s[ index ] for index in connectedIndexList ]
                    swapCharacterList.sort()
                    connectedIndexList.sort()
                    for index, character in zip( connectedIndexList, swapCharacterList ):
                        characterList[ index ] = character
        return ''.join( characterList )

class StringSwapsTest( unittest.TestCase ):
    def test_StringSwaps_Sample( self ):
        s = "dcab"
        pairs = [[0,3],[1,2]]
        self.assertEqual( StringSwaps().smallestStringWithSwaps( s, pairs ), 'bacd' )

        s = "dcab"
        pairs = [[0,3],[1,2],[0,2]]
        self.assertEqual( StringSwaps().smallestStringWithSwaps( s, pairs ), 'abcd' )

        s = "cba"
        pairs = [[0,1],[1,2]]
        self.assertEqual( StringSwaps().smallestStringWithSwaps( s, pairs ), 'abc' )

'''
Min Cost to Connect All Points

You are given an array points representing integer coordinates of some points on a 2D-plane, where points[i] = [xi, yi].

The cost of connecting two points [xi, yi] and [xj, yj] is the manhattan distance between them: |xi - xj| + |yi - yj|, where |val| denotes the absolute value of val.

Return the minimum cost to make all points connected. All points are connected if there is exactly one simple path between any two points.
'''

class ConnectPoints:
    def minCostConnectPoints(self, points: List[List[int]]) -> int:
        N = len( points )
        edgeList = list()
        for u in range( N ):
            for v in range( u + 1, N ):
                x1, y1 = points[ u ]
                x2, y2 = points[ v ]
                cost = abs( x1 - x2 ) + abs( y1 - y2 )
                edgeList.append( (cost, u, v) )
        edgeList.sort()

        uf = UnionFind()
        for vertex in range( N ):
            uf.add( vertex )
        
        edgesAdded = 0
        totalCost = 0
        for (cost, u, v) in edgeList:
            if not uf.inSameComponent( u, v ):
                uf.merge( u, v )
                totalCost += cost
                edgesAdded += 1
            if edgesAdded == N - 1:
                break
        return totalCost

class ConnectPointsTest( unittest.TestCase ):
    def test_ConnectPoints_Sample( self ):
        points = [[0,0],[2,2],[3,10],[5,2],[7,0]]
        self.assertEqual( ConnectPoints().minCostConnectPoints( points ), 20 )

        points = [[3,12],[-2,5],[-4,1]]
        self.assertEqual( ConnectPoints().minCostConnectPoints( points ), 18 )

        points = [[0,0],[1,1],[1,0],[-1,1]]
        self.assertEqual( ConnectPoints().minCostConnectPoints( points ), 4 )

        points = [[-1000000,-1000000],[1000000,1000000]]
        self.assertEqual( ConnectPoints().minCostConnectPoints( points ), 4000000 )

        points = [[0,0]]
        self.assertEqual( ConnectPoints().minCostConnectPoints( points ), 0 )

'''
Unique Email Addresses

Every valid email consists of a local name and a domain name, separated by the '@' sign. Besides lowercase letters, the email may contain one or more '.' or '+'.

For example, in "alice@leetcode.com", "alice" is the local name, and "leetcode.com" is the domain name.
If you add periods '.' between some characters in the local name part of an email address, mail sent there will be forwarded to the same address without dots in the local name. Note that this rule does not apply to domain names.

For example, "alice.z@leetcode.com" and "alicez@leetcode.com" forward to the same email address.
If you add a plus '+' in the local name, everything after the first plus sign will be ignored. This allows certain emails to be filtered. Note that this rule does not apply to domain names.

For example, "m.y+name@email.com" will be forwarded to "my@email.com".
It is possible to use both of these rules at the same time.

Given an array of strings emails where we send one email to each email[i], return the number of different addresses that actually receive mails.
'''

class UniqueMail:
    def numUniqueEmails(self, emails: List[str]) -> int:
        def _analyze( emailAddress ):
            validCharacters = list()
            inHostname = False
            passOver = False
            dotCharacter, plusCharacter, atCharacter = '.', '+', '@'

            for character in emailAddress:
                if character == atCharacter:
                    inHostname = True
                if character == plusCharacter:
                    passOver = True
                if inHostname or ( not passOver and character not in dotCharacter ):
                    validCharacters.append( character )
            return ''.join( validCharacters )
        
        uniqueEmails = set()
        for emailAddress in emails:
            uniqueEmails.add( _analyze( emailAddress ) )
        return len( uniqueEmails )

class UniqueMailTest( unittest.TestCase ):
    def test_UniqueMail_Sample( self ):
        emails = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
        self.assertEqual( UniqueMail().numUniqueEmails( emails ), 2 )

        emails = ["a@leetcode.com","b@leetcode.com","c@leetcode.com"]
        self.assertEqual( UniqueMail().numUniqueEmails( emails ), 3 )

'''
The Maze

There is a ball in a maze with empty spaces (represented as 0) and walls (represented as 1). The ball can go through the empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the m x n maze, the ball's start position and the destination, where start = [startrow, startcol] and destination = [destinationrow, destinationcol], return true if the ball can stop at the destination, otherwise return false.

You may assume that the borders of the maze are all walls.
'''

class TheMaze:
    def __init__( self, maze: List[List[int]] ):
        self.maze = maze
        self.rows, self.cols = len( maze ), len( maze[ 0 ] )
        self.emptyCell = 0

    def _getPossibleLocations( self, currentLocation ):
        possibleLocations = list()
        for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
            u, v = currentLocation
            while 0 <= u < self.rows and 0 <= v < self.cols and self.maze[ u ][ v ] == self.emptyCell:
                possibleLocation = u, v
                u, v = u + du, v + dv
            possibleLocations.append( possibleLocation )
        return possibleLocations

    def hasPath(self, start: List[int], destination: List[int]) -> bool:
        startRow, startCol = start
        targetLocation = tuple( destination )
        q = deque()
        q.append( (startRow, startCol) )
        
        visited = [ [ False for _ in range( self.cols ) ] for _ in range( self.rows ) ]
        visited[ startRow ][ startCol ] = True

        while len( q ) > 0:
            currentLocation = q.popleft()
            if currentLocation == targetLocation:
                return True
            for possibleLocation in self._getPossibleLocations( currentLocation ):
                x, y = possibleLocation
                if not visited[ x ][ y ]:
                    visited[ x ][ y ] = True
                    q.append( possibleLocation )
        return False

class TheMazeTest( unittest.TestCase ):
    def test_TheMaze_Sample( self ):
        maze = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
        start = [0,4]
        destination = [4,4]
        self.assertEqual( TheMaze( maze ).hasPath( start, destination ), True )

        maze = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
        start = [0,4]
        destination = [3,2]
        self.assertEqual( TheMaze( maze ).hasPath( start, destination ), False )

        maze = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]]
        start = [4,3]
        destination = [0,1]
        self.assertEqual( TheMaze( maze ).hasPath( start, destination ), False )

'''
The Maze II

There is a ball in a maze with empty spaces (represented as 0) and walls (represented as 1). The ball can go through the empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the m x n maze, the ball's start position and the destination, where start = [startrow, startcol] and destination = [destinationrow, destinationcol], return true if the ball can stop at the destination, otherwise return false.

You may assume that the borders of the maze are all walls.
'''

class TheMazeII( TheMaze ):
    def __init__( self, maze ):
        TheMaze.__init__( self, maze )

    def shortestDistance(self, start: List[int], destination: List[int]) -> int:
        startRow, startCol = start
        targetLocation = tuple( destination )

        q = list()
        q.append( (0, startRow, startCol) )

        distanceTable = [ [ float( 'inf' ) for _ in range( self.cols ) ] for _ in range( self.rows ) ]
        distanceTable[ startRow ][ startCol ] = 0

        while len( q ) > 0:
            distance, u, v = heapq.heappop( q )
            currentLocation = u, v
            if currentLocation == targetLocation:
                return distance
            if distance > distanceTable[ u ][ v ]:
                continue
            for possibleLocation in self._getPossibleLocations( currentLocation ):
                x, y = possibleLocation
                totalDistance = distance + abs( x - u ) + abs( y - v )
                if totalDistance < distanceTable[ x ][ y ]:
                    distanceTable[ x ][ y ] = totalDistance
                    heapq.heappush( q, (totalDistance, x, y) )
        return -1

class TheMazeIITest( unittest.TestCase ):
    def test_TheMazeII_Sample( self ):
        maze = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
        start = [0,4]
        destination = [4,4]
        self.assertEqual( TheMazeII( maze ).shortestDistance( start, destination ), 12 )

        maze = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
        start = [0,4]
        destination = [3,2]
        self.assertEqual( TheMazeII( maze ).shortestDistance( start, destination ), -1 )

        maze = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]]
        start = [4,3]
        destination = [0,1]
        self.assertEqual( TheMazeII( maze ).shortestDistance( start, destination ), -1 )

'''
The Maze III

There is a ball in a maze with empty spaces (represented as 0) and walls (represented as 1). The ball can go through the empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction. There is also a hole in this maze. The ball will drop into the hole if it rolls onto the hole.

Given the m x n maze, the ball's position ball and the hole's position hole, where ball = [ballrow, ballcol] and hole = [holerow, holecol], return a string instructions of all the instructions that the ball should follow to drop in the hole with the shortest distance possible. If there are multiple valid instructions, return the lexicographically minimum one. If the ball can't drop in the hole, return "impossible".

If there is a way for the ball to drop in the hole, the answer instructions should contain the characters 'u' (i.e., up), 'd' (i.e., down), 'l' (i.e., left), and 'r' (i.e., right).

The distance is the number of empty spaces traveled by the ball from the start position (excluded) to the destination (included).

You may assume that the borders of the maze are all walls.
'''

class TheMazeIII:
    def __init__( self, maze ):
        self.maze = maze
        self.rows, self.cols = len( maze ), len( maze[ 0 ] )
        self.emptyCell = 0

    def _getPossibleLocations( self, currentLocation, targetLocation ):
        possibleLocations = list()
        for du, dv, direction in [ (0, 1, 'r'), (0, -1, 'l'), (1, 0, 'd'), (-1, 0, 'u') ]:
            u, v = currentLocation
            while 0 <= u < self.rows and 0 <= v < self.cols and self.maze[ u ][ v ] == self.emptyCell:
                possibleLocation = u, v
                if possibleLocation == targetLocation:
                    break
                u, v = u + du, v + dv
            possibleLocations.append( (possibleLocation, direction) )
        return possibleLocations

    def findShortestWay(self, ball: List[int], hole: List[int]) -> str:
        startRow, startCol = ball
        targetLocation = tuple( hole )

        q = list()
        defaultCost = 0, str()
        q.append( (defaultCost, startRow, startCol) )

        costTable = [ [ (float( 'inf' ), str()) for _ in range( self.cols ) ] for _ in range( self.rows ) ]
        costTable[ startRow ][ startCol ] = defaultCost

        while len( q ) > 0:
            cost, u, v = heapq.heappop( q )
            distance, path = cost
            currentLocation = u, v
            if currentLocation == targetLocation:
                return path
            if cost > costTable[ u ][ v ]:
                continue
            for (x, y), direction in self._getPossibleLocations( currentLocation, targetLocation ):
                totalDistance = distance + abs( x - u ) + abs( y - v )
                newCost = totalDistance, path + direction
                if newCost < costTable[ x ][ y ]:
                    costTable[ x ][ y ] = newCost
                    heapq.heappush( q, (newCost, x, y) )
        return 'impossible'

class TheMazeIIITest( unittest.TestCase ):
    def test_TheMazeIII_Sample( self ):
        maze = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]]
        ball = [4,3]
        hole = [0,1]
        self.assertEqual( TheMazeIII( maze ).findShortestWay( ball, hole ), 'lul' )

        maze = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]]
        ball = [4,3]
        hole = [3,0]
        self.assertEqual( TheMazeIII( maze ).findShortestWay( ball, hole ), 'impossible' )

        maze = [[0,0,0,0,0,0,0],[0,0,1,0,0,1,0],[0,0,0,0,1,0,0],[0,0,0,0,0,0,1]]
        ball = [0,4]
        hole = [3,5]
        self.assertEqual( TheMazeIII( maze ).findShortestWay( ball, hole ), 'dldr' )

'''
Flood Fill

An image is represented by an m x n integer grid image where image[i][j] represents the pixel value of the image.

You are also given three integers sr, sc, and newColor. You should perform a flood fill on the image starting from the pixel image[sr][sc].

To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color), and so on. Replace the color of all of the aforementioned pixels with newColor.

Return the modified image after performing the flood fill.
'''

class FloodFill:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:
        color = image[ sr ][ sc ]
        rows, cols = len( image ), len( image[ 0 ] )
        
        q = deque()
        if color != newColor:
            q.append( (sr, sc) )
        
        while len( q ) > 0:
            u, v = q.popleft()
            if image[ u ][ v ] != color:
                continue
            image[ u ][ v ] = newColor
            for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                x, y = u + du, v + dv
                if 0 <= x < rows and 0 <= y < cols and image[ x ][ y ] == color:
                    q.append( (x, y) )
        return image

class FloodFillTest( unittest.TestCase ):
    def test_FloodFill_Sample( self ):
        image = [[1,1,1],[1,1,0],[1,0,1]]
        sr, sc, newColor = 1, 1, 2
        newImage = [[2,2,2],[2,2,0],[2,0,1]]
        self.assertEqual( FloodFill().floodFill( image, sr, sc, newColor ), newImage )

        image = [[0,0,0],[0,0,0]]
        sr, sc, newColor = 0, 0, 2
        newImage = [[2,2,2],[2,2,2]]
        self.assertEqual( FloodFill().floodFill( image, sr, sc, newColor ), newImage )

'''
Group Anagrams

Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
'''

class GroupAnagrams:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        anagramGroups = list()
        signatureDict = dict()

        for word in strs:
            frequencyList = [ 0 for _ in range( len( string.ascii_lowercase ) ) ]
            for character in word:
                frequencyList[ ord( character ) - ord( 'a' ) ] += 1
            signature = '_'.join( map( str, frequencyList ) )
            if signature not in signatureDict:
                signatureDict[ signature ] = len( anagramGroups )
                anagramGroups.append( [ word ] )
            else:
                anagramGroups[ signatureDict[ signature ] ].append( word )
        return anagramGroups

class GroupAnagramsTest( unittest.TestCase ):
    def test_GroupAnagrams_Sample( self ):
        strs = ["eat","tea","tan","ate","nat","bat"]
        group = [["eat","tea","ate"],["tan","nat"],["bat"]]
        self.assertEqual( GroupAnagrams().groupAnagrams( strs ), group )

'''
Valid Sudoku

Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules.
'''

class Sudoku:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        emptyCell = '.'
        N, blockSize = 9, 3
        rowData = [ set() for _ in range( N ) ]
        colData = [ set() for _ in range( N ) ]
        gridData = [ set() for _ in range( N ) ]

        for row, col in itertools.product( range( N ), range( N ) ):
            gridNumber = col // blockSize + ( row // blockSize ) * blockSize
            data = board[ row ][ col ]
            if data == emptyCell:
                continue
            if data in rowData[ row ] or data in colData[ col ] or data in gridData[ gridNumber ]:
                return False
            rowData[ row ].add( data )
            colData[ col ].add( data )
            gridData[ gridNumber ].add( data )
        return True

class SudokuTest( unittest.TestCase ):
    def test_Sudoku_Sample( self ):
        board = [["5","3",".",".","7",".",".",".","."],
                 ["6",".",".","1","9","5",".",".","."],
                 [".","9","8",".",".",".",".","6","."],
                 ["8",".",".",".","6",".",".",".","3"],
                 ["4",".",".","8",".","3",".",".","1"],
                 ["7",".",".",".","2",".",".",".","6"],
                 [".","6",".",".",".",".","2","8","."],
                 [".",".",".","4","1","9",".",".","5"],
                 [".",".",".",".","8",".",".","7","9"]]
        self.assertEqual( Sudoku().isValidSudoku( board ), True )

        board = [["8","3",".",".","7",".",".",".","."],
                 ["6",".",".","1","9","5",".",".","."],
                 [".","9","8",".",".",".",".","6","."],
                 ["8",".",".",".","6",".",".",".","3"],
                 ["4",".",".","8",".","3",".",".","1"],
                 ["7",".",".",".","2",".",".",".","6"],
                 [".","6",".",".",".",".","2","8","."],
                 [".",".",".","4","1","9",".",".","5"],
                 [".",".",".",".","8",".",".","7","9"]]
        self.assertEqual( Sudoku().isValidSudoku( board ), False )

'''
Paint House

There is a row of n houses, where each house can be painted one of three colors: red, blue, or green. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

The cost of painting each house with a certain color is represented by an n x 3 cost matrix costs.

For example, costs[0][0] is the cost of painting house 0 with the color red; costs[1][2] is the cost of painting house 1 with color green, and so on...
Return the minimum cost to paint all houses.
'''

class PaintHouse:
    def minCost(self, costs: List[List[int]]) -> int:
        costRed, costBlue, costGreen = 0, 0, 0
        for (red, blue, green) in costs:
            newCostRed = red + min( costBlue, costGreen )
            newCostBlue = blue + min( costRed, costGreen )
            newCostGreen = green + min( costRed, costBlue )
            costRed, costBlue, costGreen = newCostRed, newCostBlue, newCostGreen
        return min( costRed, costBlue, costGreen )

class PaintHouseTest( unittest.TestCase ):
    def test_PaintHouse_Sample( self ):
        costs = [[17,2,17],[16,16,5],[14,3,19]]
        self.assertEqual( PaintHouse().minCost( costs ), 10 )

        costs = [[7,6,2]]
        self.assertEqual( PaintHouse().minCost( costs ), 2 )

'''
Paint House II

There are a row of n houses, each house can be painted with one of the k colors. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

The cost of painting each house with a certain color is represented by an n x k cost matrix costs.

For example, costs[0][0] is the cost of painting house 0 with color 0; costs[1][2] is the cost of painting house 1 with color 2, and so on...
Return the minimum cost to paint all houses.
'''

class PaintHouseII:
    def minCostII(self, costs: List[List[int]]) -> int:
        k = len( costs[ 0 ] )
        slots = [ [ 0 for _ in range( k ) ] for _ in range( 2 ) ]

        for i, costList in enumerate( costs ):
            currentSlot = i & 0x1
            previousSlot = currentSlot ^ 0x1

            A = B = float( 'inf' )
            for cost in slots[ previousSlot ]:
                if cost <= A:
                    A, B = cost, A
                elif cost < B:
                    B = cost
            for j, cost in enumerate( costList ):
                slots[ currentSlot ][ j ] = cost + ( B if slots[ previousSlot ][ j ] == A else A )
        return min( slots[ currentSlot ] )

class PaintHouseIITest( unittest.TestCase ):
    def test_PaintHouseII_Sample( self ):
        costs = [[1,5,3],[2,9,4]]
        self.assertEqual( PaintHouseII().minCostII( costs ), 5 )

        costs = [[1,3],[2,4]]
        self.assertEqual( PaintHouseII().minCostII( costs ), 5 )

'''
Max Area of Island

You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

The area of an island is the number of cells with a value 1 in the island.

Return the maximum area of an island in grid. If there is no island, return 0.
'''

class Island:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        emptyCell = 0
        rows, cols = len( grid ), len( grid[ 0 ] )
        maximumArea = 0

        def _bfs( u, v ):
            count = 0
            q = deque()
            q.append( (u, v) )
            grid[ u ][ v ] = emptyCell

            while len( q ) > 0:
                u, v = q.popleft()
                count += 1
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and grid[ x ][ y ] != emptyCell:
                        grid[ x ][ y ] = emptyCell
                        q.append( (x, y) )
            return count

        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] != emptyCell:
                area = _bfs( u, v )
                maximumArea = max( maximumArea, area )
        return maximumArea

'''
Island Perimeter

You are given row x col grid representing a map where grid[i][j] = 1 represents land and grid[i][j] = 0 represents water.

Grid cells are connected horizontally/vertically (not diagonally). The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells).

The island doesn't have "lakes", meaning the water inside isn't connected to the water around the island. One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100. Determine the perimeter of the island.
'''

class IslandPerimeter:
    def islandPerimeter(self, grid: List[List[int]]) -> int:
        count = 0
        emptyCell = 0
        rows, cols = len( grid ), len( grid[ 0 ] )
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] != emptyCell:
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if not 0 <= x < rows or not 0 <= y < cols:
                        count += 1
                    elif grid[ x ][ y ] == emptyCell:
                        count += 1
        return count

class IslandPerimeterTest( unittest.TestCase ):
    def test_IslandPerimeter_Sample( self ):
        grid = [[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]
        self.assertEqual( IslandPerimeter().islandPerimeter( grid ), 16 )

        grid = [[1]]
        self.assertEqual( IslandPerimeter().islandPerimeter( grid ), 4 )

        grid = [[1,0]]
        self.assertEqual( IslandPerimeter().islandPerimeter( grid ), 4 )

class IslandTest( unittest.TestCase ):
    def test_Island_Sample( self ):
        grid = [
            [0,0,1,0,0,0,0,1,0,0,0,0,0],
            [0,0,0,0,0,0,0,1,1,1,0,0,0],
            [0,1,1,0,1,0,0,0,0,0,0,0,0],
            [0,1,0,0,1,1,0,0,1,0,1,0,0],
            [0,1,0,0,1,1,0,0,1,1,1,0,0],
            [0,0,0,0,0,0,0,0,0,0,1,0,0],
            [0,0,0,0,0,0,0,1,1,1,0,0,0],
            [0,0,0,0,0,0,0,1,1,0,0,0,0]]
        self.assertEqual( Island().maxAreaOfIsland( grid ), 6 )

        grid = [[0,0,0,0,0,0,0,0]]
        self.assertEqual( Island().maxAreaOfIsland( grid ), 0 )

'''
Is Graph Bipartite?

There is an undirected graph with n nodes, where each node is numbered between 0 and n - 1. You are given a 2D array graph, where graph[u] is an array of nodes that node u is adjacent to. More formally, for each v in graph[u], there is an undirected edge between node u and node v. The graph has the following properties:

There are no self-edges (graph[u] does not contain u).
There are no parallel edges (graph[u] does not contain duplicate values).
If v is in graph[u], then u is in graph[v] (the graph is undirected).
The graph may not be connected, meaning there may be two nodes u and v such that there is no path between them.
A graph is bipartite if the nodes can be partitioned into two independent sets A and B such that every edge in the graph connects a node in set A and a node in set B.

Return true if and only if it is bipartite.
'''

class IsGraphBipartite:
    def isBipartite(self, graph: List[List[int]]) -> bool:
        vertexCount = len( graph )
        vertexColor = [ None for _ in range( vertexCount ) ]
        colorWhite, colorBlack = 0, 1

        def _colorComplement( color ):
            return colorWhite if color == colorBlack else colorBlack

        def _color( vertex ):
            vertexColor[ vertex ] = colorWhite
            q = deque()
            q.append( vertex )

            while len( q ) > 0:
                vertex = q.popleft()
                for adjacentVertex in graph[ vertex ]:
                    if vertexColor[ adjacentVertex ] == vertexColor[ vertex ]:
                        return False
                    elif vertexColor[ adjacentVertex ] is None:
                        vertexColor[ adjacentVertex ] = _colorComplement( vertexColor[ vertex ] )
                        q.append( adjacentVertex )
            return True 

        for vertex in range( vertexCount ):
            if vertexColor[ vertex ] is None:
                if not _color( vertex ):
                    return False
        return True

class IsGraphBipartiteTest( unittest.TestCase ):
    def test_IsGraphBipartite_Sample( self ):
        graph = [[1,2,3],[0,2],[0,1,3],[0,2]]
        self.assertEqual( IsGraphBipartite().isBipartite( graph ), False )

        graph = [[1,3],[0,2],[1,3],[0,2]]
        self.assertEqual( IsGraphBipartite().isBipartite( graph ), True )

'''
Keys and Rooms

There are n rooms labeled from 0 to n - 1 and all the rooms are locked except for room 0. Your goal is to visit all the rooms. However, you cannot enter a locked room without having its key.

When you visit a room, you may find a set of distinct keys in it. Each key has a number on it, denoting which room it unlocks, and you can take all of them with you to unlock the other rooms.

Given an array rooms where rooms[i] is the set of keys that you can obtain if you visited room i, return true if you can visit all the rooms, or false otherwise.
'''

class KeysAndRooms:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:
        roomCount = len( rooms )
        keysObtained = [ False for _ in range( roomCount ) ]

        q = deque()
        q.append( 0 )
        keysObtained[ 0 ] = True

        while len( q ) > 0:
            currentRoom = q.popleft()
            for key in rooms[ currentRoom ]:
                if not keysObtained[ key ]:
                    keysObtained[ key ] = True
                    q.append( key )
        return all( keysObtained )

class KeysAndRoomsTest( unittest.TestCase ):
    def test_KeysAndRooms_Sample( self ):
        rooms = [[1],[2],[3],[]]
        self.assertEqual( KeysAndRooms().canVisitAllRooms( rooms ), True )

        rooms = [[1,3],[3,0,1],[2],[0]]
        self.assertEqual( KeysAndRooms().canVisitAllRooms( rooms ), False )

'''
Longest Increasing Path in a Matrix

Given an m x n integers matrix, return the length of the longest increasing path in matrix.

From each cell, you can either move in four directions: left, right, up, or down. You may not move diagonally or move outside the boundary (i.e., wrap-around is not allowed).
'''

class LongestIncreasingPath:
    def longestIncreasingPath(self, matrix: List[List[int]]) -> int:
        rows, cols = len( matrix ), len( matrix[ 0 ] )
        pathLengthTable = [ [ None for _ in range( cols ) ] for _ in range( rows ) ]

        def _dfs( u, v ):
            stack = list()
            stack.append( (u, v, 'EVALUATE') )

            while len( stack ) > 0:
                u, v, command = stack.pop()
                if command == 'EVALUATE':
                    if pathLengthTable[ u ][ v ] is not None:
                        continue
                    stack.append( (u, v, 'POP') )
                    for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                        x, y = u + du, v + dv
                        if 0 <= x < rows and 0 <= y < cols and matrix[ x ][ y ] > matrix[ u ][ v ]:
                            stack.append( (x, y, 'EVALUATE') )
                elif command == 'POP':
                    pathLengthTable[ u ][ v ] = 1
                    for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                        x, y = u + du, v + dv
                        if 0 <= x < rows and 0 <= y < cols and matrix[ x ][ y ] > matrix[ u ][ v ]:
                            pathLengthTable[ u ][ v ] = max( pathLengthTable[ u ][ v ], 1 + pathLengthTable[ x ][ y ] )

        longestPathLength = 1
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if pathLengthTable[ u ][ v ] is None:
                _dfs( u, v )
            longestPathLength = max( longestPathLength, pathLengthTable[ u ][ v ] )
        return longestPathLength

class LongestIncreasingPathTest( unittest.TestCase ):
    def test_LongestIncreasingPath_Sample( self ):
        matrix = [[9,9,4],[6,6,8],[2,1,1]]
        self.assertEqual( LongestIncreasingPath().longestIncreasingPath( matrix ), 4 )

        matrix = [[3,4,5],[3,2,6],[2,2,1]]
        self.assertEqual( LongestIncreasingPath().longestIncreasingPath( matrix ), 4 )

        matrix = [[1]]
        self.assertEqual( LongestIncreasingPath().longestIncreasingPath( matrix ), 1 )

class OpenTheLock:
    def openLock(self, deadends: List[str], target: str) -> int:
        deadendsSet = set( deadends )
        
        q = deque()
        visited = set()
        
        startState = "0000"
        if startState not in deadends:
            q.append( startState )
            visited.add( startState )

        steps = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                currentState = q.popleft()
                if currentState == target:
                    return steps
                characterList = list( map( int, currentState) )

                possibleStates = list()
                for i in range( len( characterList ) ):
                    x = characterList[ i ]
                    characterList[ i ] = ( x + 1 ) % 10
                    possibleStates.append( ''.join( map( str, characterList ) ) )
                    characterList[ i ] = ( x - 1 ) % 10
                    possibleStates.append( ''.join( map( str, characterList ) ) )
                    characterList[ i ] = x
                for possibleState in possibleStates:
                    if possibleState not in deadendsSet and possibleState not in visited:
                        visited.add( possibleState )
                        q.append( possibleState )
            steps += 1
        return -1

class OpenTheLockTest( unittest.TestCase ):
    def test_OpenTheLock_Sample( self ):
        deadends = ["0201","0101","0102","1212","2002"]
        target = "0202"
        self.assertEqual( OpenTheLock().openLock( deadends, target ), 6 )

        deadends = ["8888"]
        target = "0009"
        self.assertEqual( OpenTheLock().openLock( deadends, target ), 1 )

        deadends = ["8887","8889","8878","8898","8788","8988","7888","9888"]
        target = "8888"
        self.assertEqual( OpenTheLock().openLock( deadends, target ), -1 )

        deadends = ["0000"]
        target = "8888"
        self.assertEqual( OpenTheLock().openLock( deadends, target ), -1 )

'''
Shortest Path to Get All Keys

You are given an m x n grid grid where:

'.' is an empty cell.
'#' is a wall.
'@' is the starting point.
Lowercase letters represent keys.
Uppercase letters represent locks.
You start at the starting point and one move consists of walking one space in one of the four cardinal directions. You cannot walk outside the grid, or walk into a wall.

If you walk over a key, you can pick it up and you cannot walk over a lock unless you have its corresponding key.

For some 1 <= k <= 6, there is exactly one lowercase and one uppercase letter of the first k letters of the English alphabet in the grid. This means that there is exactly one key for each lock, and one lock for each key; and also that the letters used to represent the keys and locks were chosen in the same order as the English alphabet.

Return the lowest number of moves to acquire all keys. If it is impossible, return -1.
'''

class AllKeys:
    def __init__( self, grid ):
        self.rows, self.cols = len( grid ), len( grid[ 0 ] )
        self.grid = grid

    def shortestPathAllKeys(self ) -> int:
        blockedCell, startCell = '#', '@'
        numberOfKeys = 0
        startLocation = None
        for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
            cellType = self.grid[ u ][ v ]
            if cellType == startCell:
                startLocation = u, v
            elif cellType.islower():
                numberOfKeys += 1

        u, v = startLocation
        q = deque()
        q.append( (u, v, str()) )

        visited = set()
        visited.add( (u, v, str()) )

        steps = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                u, v, keysObtained = q.popleft()
                if len( keysObtained ) == numberOfKeys:
                    return steps
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if not 0 <= x < self.rows or not 0 <= y < self.cols:
                        continue
                    cellType = self.grid[ x ][ y ]
                    if cellType == blockedCell:
                        continue
                    if cellType.isupper() and cellType.lower() not in keysObtained:
                        continue
                    newKeysObtained = keysObtained
                    if cellType.islower() and cellType not in keysObtained:
                        newKeysObtained = ''.join( sorted( keysObtained + cellType ) )
                    if (x, y, newKeysObtained) not in visited:
                        visited.add( (x, y, newKeysObtained) )
                        q.append( (x, y, newKeysObtained) )
            steps += 1
        return -1

class AllKeysTest( unittest.TestCase ):
    def test_AllKeys_Sample( self ):
        grid = ["@.a.#",
                "###.#",
                "b.A.B"]
        self.assertEqual( AllKeys( grid ).shortestPathAllKeys(), 8 )

        grid = ["@..aA",
                "..B#.",
                "....b"]
        self.assertEqual( AllKeys( grid ).shortestPathAllKeys(), 6 )

        grid = ["@Aa"]
        self.assertEqual( AllKeys( grid ).shortestPathAllKeys(), -1 )

'''
Jump Game

You are given an integer array nums. You are initially positioned at the array's first index, and each element in the array represents your maximum jump length at that position.

Return true if you can reach the last index, or false otherwise.
'''
class JumpGame:
    def canJump(self, nums: List[int]) -> bool:
        maximumReachableIndex = 0
        for i, step in enumerate( nums ):
            if i > maximumReachableIndex:
                return False
            maximumReachableIndex = max( maximumReachableIndex, i + step )
        return maximumReachableIndex >= len( nums ) - 1

class JumpGameTest( unittest.TestCase ):
    def test_JumpGame_Sample( self ):
        nums = [2,3,1,1,4]
        self.assertEqual( JumpGame().canJump( nums ), True )

        nums = [3,2,1,0,4]
        self.assertEqual( JumpGame().canJump( nums ), False )

'''
Minimum Window Substring

Given two strings s and t of lengths m and n respectively, return the minimum window substring of s such that every character in t (including duplicates) is included in the window. If there is no such substring, return the empty string "".

The testcases will be generated such that the answer is unique.

A substring is a contiguous sequence of characters within the string.
'''

class MinimumWindowSubstring:
    def minWindow(self, s: str, t: str) -> str:
        def _update( frequencyDict, character, dx ):
            if character not in frequencyDict:
                return
            frequencyDict[ character ] += dx

        frequencyDict = Counter( t )

        i = j = 0
        minimumWindowSize = float( 'inf' )
        windowStartIndex = None
        count = len( frequencyDict )
        while j < len( s ):
            character = s[ j ]
            j += 1
            
            _update( frequencyDict, character, -1 )
            if frequencyDict.get( character ) == 0:
                count -= 1
            while count == 0:
                windowSize = j - i
                if windowSize < minimumWindowSize:
                    minimumWindowSize = windowSize
                    windowStartIndex = i
                character = s[ i ]
                i += 1
                _update( frequencyDict, character, 1 )
                if frequencyDict.get( character ) == 1:
                    count += 1
        return "" if minimumWindowSize == float( 'inf' ) else \
               s[ windowStartIndex : windowStartIndex + minimumWindowSize ]

class MinimumWindowSubstringTest( unittest.TestCase ):
    def test_MinimumWindowSubstring_Sample( self ):
        s = "ADOBECODEBANC"
        t = "ABC"
        self.assertEqual( MinimumWindowSubstring().minWindow( s, t ), "BANC" )

        s = "a"
        t = "a"
        self.assertEqual( MinimumWindowSubstring().minWindow( s, t ), "a" )

        s = "a"
        t = "aa"
        self.assertEqual( MinimumWindowSubstring().minWindow( s, t ), "" )

'''
Longest Substring with At Most Two Distinct Characters

Given a string s, return the length of the longest substring that contains at most two distinct characters.
'''

class LongestSubstring:
    def lengthOfLongestSubstringTwoDistinct(self, s: str) -> int:
        frequencyDict = dict()
        i = j = 0
        longestSize = 0
        while j < len( s ):
            character = s[ j ]
            j += 1
            
            if character not in frequencyDict:
                frequencyDict[ character ] = 0
            frequencyDict[ character ] += 1

            while len( frequencyDict ) > 2:
                character = s[ i ]
                i += 1

                frequencyDict[ character ] -= 1
                if frequencyDict[ character ] == 0:
                    del frequencyDict[ character ]
            
            longestSize = max( longestSize, j - i )
        return longestSize

class LongestSubstringTest( unittest.TestCase ):
    def test_LongestSubstring_Sample( self ):
        s = "eceba"
        self.assertEqual( LongestSubstring().lengthOfLongestSubstringTwoDistinct( s ), 3 )

        s = "ccaabbb"
        self.assertEqual( LongestSubstring().lengthOfLongestSubstringTwoDistinct( s ), 5 )

'''
K Closest Points to Origin

Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane and an integer k, return the k closest points to the origin (0, 0).

The distance between two points on the X-Y plane is the Euclidean distance (i.e., √(x1 - x2)2 + (y1 - y2)2).

You may return the answer in any order. The answer is guaranteed to be unique (except for the order that it is in).
'''

class ClosestPointsToOrigin:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        distanceList = [ (x * x + y * y, i) for i, (x, y) in enumerate( points ) ]
        random.shuffle( distanceList )
        
        def _pivot( i, j ):
            pivotDistance, _ = distanceList[ j ]
            k = i
            while i < j:
                d, _ = distanceList[ i ]
                if d <= pivotDistance:
                    distanceList[ k ], distanceList[ i ] = distanceList[ i ], distanceList[ k ]
                    k += 1
                i += 1
            distanceList[ j ], distanceList[ k ] = distanceList[ k ], distanceList[ j ]
            return k

        i, j = 0, len( points ) - 1
        requiredElements = k
        while True:
            m = _pivot( i, j )
            elementCount = m - i + 1
            if elementCount == requiredElements:
                break
            if elementCount > requiredElements:
                j = m - 1
            else:
                i = m + 1
                requiredElements = requiredElements - elementCount
        return [ points[ i ] for (_, i) in distanceList[ : k ] ]

class ClosestPointsToOriginTest( unittest.TestCase ):
    def test_ClosestPointsToOrigin_Sample( self ):
        points = [[1,3],[-2,2]]
        k = 1
        self.assertEqual( ClosestPointsToOrigin().kClosest( points, k ), [[-2,2]] )

        points = [[3,3],[5,-1],[-2,4]]
        k = 2
        A = [[3,3],[-2,4]]
        B = [[-2,4],[3,3]]
        self.assertIn( ClosestPointsToOrigin().kClosest( points, k ), (A, B) )

'''
Longest Palindromic Substring

Given a string s, return the longest palindromic substring in s.
'''

class LongestPalindromicSubstring:
    def longestPalindrome(self, s: str) -> str:
        maximumLength = 1
        startIndex = 0

        def _palindrome( i, j ):
            L = 0
            while i >= 0 and j < len( s ) and s[ i ] == s[ j ]:
                L = max( L, j - i + 1 )
                i -= 1
                j += 1
            return L, i + 1
        for i in range( len( s ) ):
            L, index = _palindrome( i, i )
            if L > maximumLength:
                maximumLength, startIndex = L, index
        for i in range( len( s ) - 1 ):
            L, index = _palindrome( i, i + 1 )
            if L > maximumLength:
                maximumLength, startIndex = L, index
        return s[ startIndex : startIndex + maximumLength ]

class LongestPalindromicSubstringTest( unittest.TestCase ):
    def test_LongestPalindromicSubstring_Sample( self ):
        s = "babad"
        self.assertIn( LongestPalindromicSubstring().longestPalindrome( s ), [ "bab", "aba" ] )

        s = "cbbd"
        self.assertEqual( LongestPalindromicSubstring().longestPalindrome( s ), "bb" )

        s = "a"
        self.assertEqual( LongestPalindromicSubstring().longestPalindrome( s ), "a" )

        s = "ac"
        self.assertEqual( LongestPalindromicSubstring().longestPalindrome( s ), "a" )

'''
Minimum Path Sum

Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right, which minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.
'''
class MinimumPathSum:
    def minPathSum(self, grid: List[List[int]]) -> int:
        costList = list()
        for i, costData in enumerate( grid ):
            if i == 0:
                for cost in costData:
                    costList.append( cost )
                for j in range( 1, len( costList ) ):
                    costList[ j ] += costList[ j - 1 ]
                continue
            newCostList = list()
            for j, cost in enumerate( costData ):
                if j == 0:
                    newCostList.append( cost + costList[ j ] )
                else:
                    newCostList.append( cost + min( newCostList[ j - 1 ], costList[ j ] ) )
            costList = newCostList
        return costList[ -1 ]

class MinimumPathSumTest( unittest.TestCase ):
    def test_MinimumPathSum_Sample( self ):
        grid = [[1,3,1],[1,5,1],[4,2,1]]
        self.assertEqual( MinimumPathSum().minPathSum( grid ), 7 )

        grid = [[1,2,3],[4,5,6]]
        self.assertEqual( MinimumPathSum().minPathSum( grid ), 12 )

'''
Making A Large Island

You are given an n x n binary matrix grid. You are allowed to change at most one 0 to be 1.

Return the size of the largest island in grid after applying this operation.

An island is a 4-directionally connected group of 1s.
'''

class MakingALargeIsland:
    def largestIsland(self, grid: List[List[int]]) -> int:
        rows, cols = len( grid ), len( grid[ 0 ] )
        emptyCell = 0

        nextIslandId = 0
        borderCellDict = defaultdict( lambda : set() )
        sizeDict = dict()
        visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]

        def _dfs( u, v, visited, islandId ):
            count = 0
            visited[ u ][ v ] = True
            stack = list()
            stack.append( (u, v) )

            while len( stack ) > 0:
                u, v = stack.pop()
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if not 0 <= x < rows or not 0 <= y < cols:
                        continue
                    if grid[ x ][ y ] != emptyCell and not visited[ x ][ y ]:
                        visited[ x ][ y ] = True
                        stack.append( (x, y) )
                    elif grid[ x ][ y ] == emptyCell:
                        borderCellDict[ (x, y) ].add( islandId )
                count += 1
            return count

        maximumArea = 1
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if not grid[ u ][ v ] == emptyCell and not visited[ u ][ v ]:
                count = _dfs( u, v, visited, nextIslandId )
                maximumArea = max( maximumArea, count )
                sizeDict[ nextIslandId ] = count
                nextIslandId += 1

        for islandIdList in borderCellDict.values():
            sizeList = [ sizeDict[ island ] for island in islandIdList ]
            maximumArea = max( maximumArea, 1 + sum( sizeList ) )
        return maximumArea

class MakingALargeIslandTest( unittest.TestCase ):
    def test_MakingALargeIsland_Sample( self ):
        grid = [[1,0],[0,1]]
        self.assertEqual( MakingALargeIsland().largestIsland( grid ), 3 )

        grid = [[1,1],[1,0]]
        self.assertEqual( MakingALargeIsland().largestIsland( grid ), 4 )

        grid = [[1,1],[1,1]]
        self.assertEqual( MakingALargeIsland().largestIsland( grid ), 4 )

'''
Shortest Bridge

In a given 2D binary array grid, there are two islands.  (An island is a 4-directionally connected group of 1s not connected to any other 1s.)

Now, we may change 0s to 1s so as to connect the two islands together to form 1 island.

Return the smallest number of 0s that must be flipped.  (It is guaranteed that the answer is at least 1.)
'''

class ShortestBridge:
    def shortestBridge(self, grid: List[List[int]]) -> int:
        rows, cols = len( grid ), len( grid[ 0 ] )
        emptyCell = 0
        visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]

        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] != emptyCell:
                break

        boundaryCells = list()
        q = deque()
        q.append( (u, v) )
        visited[ u ][ v ] = True

        while len( q ) > 0:
            u, v = q.popleft()
            for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                x, y = u + du, v + dv
                if not 0 <= x < rows or not 0 <= y < cols or visited[ x ][ y ]:
                    continue
                visited[ x ][ y ] = True
                if grid[ x ][ y ] == emptyCell:
                    boundaryCells.append( (x, y) )
                else:
                    q.append( (x, y) )

        q.clear()
        for boundaryCell in boundaryCells:
            q.append( boundaryCell )
        
        bridgeLength = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                u, v = q.popleft()
                if grid[ u ][ v ] != emptyCell:
                    return bridgeLength
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if not 0 <= x < rows or not 0 <= y < cols:
                        continue
                    if not visited[ x ][ y ]:
                        visited[ x ][ y ] = True
                        q.append( (x, y) )
            bridgeLength += 1

class ShortestBridgeTest( unittest.TestCase ):
    def test_ShortestBridge_Sample( self ):
        grid = [[0,1],[1,0]]
        self.assertEqual( ShortestBridge().shortestBridge( grid ), 1 )

        grid = [[0,1,0],[0,0,0],[0,0,1]]
        self.assertEqual( ShortestBridge().shortestBridge( grid ), 2 )

        grid = [[1,1,1,1,1],[1,0,0,0,1],[1,0,1,0,1],[1,0,0,0,1],[1,1,1,1,1]]
        self.assertEqual( ShortestBridge().shortestBridge( grid ), 1 )

'''
Walls and Gates

You are given an m x n grid rooms initialized with these three possible values.

-1 A wall or an obstacle.
0 A gate.
INF Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, it should be filled with INF.
'''

class WallsAndGates:
    def wallsAndGates(self, rooms: List[List[int]]) -> None:
        obstacleCell, gateCell, emptyCell = -1, 0, 2147483647
        
        rows, cols = len( rooms ), len( rooms[ 0 ] )
        
        q = deque()
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if rooms[ u ][ v ] == gateCell:
                q.append( (u, v) )

        distance = 1
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                u, v = q.popleft()
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and rooms[ x ][ y ] == emptyCell:
                        rooms[ x ][ y ] = distance
                        q.append( (x, y) )
            distance += 1

class WallsAndGatesTest( unittest.TestCase ):
    def test_WallsAndGates_Sample( self ):
        rooms = [
            [2147483647,-1,0,2147483647],
            [2147483647,2147483647,2147483647,-1],
            [2147483647,-1,2147483647,-1],
            [0,-1,2147483647,2147483647]
        ]
        WallsAndGates().wallsAndGates( rooms )
        self.assertEqual( rooms, [[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]] )

        rooms = [[-1]]
        WallsAndGates().wallsAndGates( rooms )
        self.assertEqual( rooms, [[-1]] )

        rooms = [[2147483647]]
        WallsAndGates().wallsAndGates( rooms )
        self.assertEqual( rooms, [[2147483647]] )

        rooms = [[0]]
        WallsAndGates().wallsAndGates( rooms )
        self.assertEqual( rooms, [[0]] )

'''
Surrounded Regions

Given an m x n matrix board containing 'X' and 'O', capture all regions that are 4-directionally surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.
'''

class SurroundedRegions:
    def solve(self, board: List[List[str]]) -> None:
        rows, cols = len( board ), len( board[ 0 ] )
        visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]

        def _dfs( u, v ):
            stack = list()
            stack.append( (u, v) )
            
            while len( stack ) > 0:
                u, v = stack.pop()
                if visited[ u ][ v ]:
                    continue
                visited[ u ][ v ] = True
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0 ) ]:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and board[ x ][ y ] == 'O' and not visited[ x ][ y ]:
                        stack.append( (x, y) )

        for u, v in itertools.product( range( rows ), range( cols ) ):
            if ( u == 0 or u == rows - 1 or v == 0 or v == cols - 1 ) and board[ u ][ v ] == 'O' and not visited[ u ][ v ]:
                _dfs( u, v )

        for u, v in itertools.product( range( rows ), range( cols ) ):
            board[ u ][ v ] = 'O' if visited[ u ][ v ] else 'X'

class SurroundedRegionsTest( unittest.TestCase ):
    def test_SurroundedRegions_Sample( self ):
        board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
        SurroundedRegions().solve( board )
        expectedBoard = [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
        self.assertEqual( board, expectedBoard )

        board = [["X"]]
        SurroundedRegions().solve( board )
        expectedBoard = [["X"]]
        self.assertEqual( board, expectedBoard )

'''
Number of Islands II

You are given an empty 2D binary grid grid of size m x n. The grid represents a map where 0's represent water and 1's represent land. Initially, all the cells of grid are water cells (i.e., all the cells are 0's).

We may perform an add land operation which turns the water at position into a land. You are given an array positions where positions[i] = [ri, ci] is the position (ri, ci) at which we should operate the ith operation.

Return an array of integers answer where answer[i] is the number of islands after turning the cell (ri, ci) into a land.

An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.
'''

class NumberOfIslandsII:
    def numIslands2(self, m: int, n: int, positions: List[List[int]]) -> List[int]:
        grid = [ [ 0 for _ in range( n ) ] for _ in range( m ) ]

        uf = UnionFind()
        islandCount = list()

        for (u, v) in positions:
            grid[ u ][ v ] = 1
            vertexA = u, v
            uf.add( vertexA )
            for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                vertexB = x, y = u + du, v + dv
                if 0 <= x < m and 0 <= y < n and grid[ x ][ y ] == 1:
                    uf.merge( vertexA, vertexB )
            islandCount.append( uf.disjointSetCount() )
        return islandCount

class NumberOfIslandsIITest( unittest.TestCase ):
    def test_NumberOfIslandsII_Sample( self ):
        m, n = 3, 3
        positions = [[0,0],[0,1],[1,2],[2,1]]
        self.assertEqual( NumberOfIslandsII().numIslands2( m, n, positions ), [1,1,2,3] )

        m, n = 1, 1
        positions = [[0,0]]
        self.assertEqual( NumberOfIslandsII().numIslands2( m, n, positions ), [1] )

'''
Shortest Distance from All Buildings

You are given an m x n grid grid of values 0, 1, or 2, where:

each 0 marks an empty land that you can pass by freely,
each 1 marks a building that you cannot pass through, and
each 2 marks an obstacle that you cannot pass through.
You want to build a house on an empty land that reaches all buildings in the shortest total travel distance. You can only move up, down, left, and right.

Return the shortest travel distance for such a house. If it is not possible to build such a house according to the above rules, return -1.

The total travel distance is the sum of the distances between the houses of the friends and the meeting point.

The distance is calculated using Manhattan Distance, where distance(p1, p2) = |p2.x - p1.x| + |p2.y - p1.y|.
'''

class ShortestDistance:
    def shortestDistance(self, grid: List[List[int]]) -> int:
        rows, cols = len( grid ), len( grid[ 0 ] )
        distanceTable = [ [ 0 for _ in range( cols ) ] for _ in range( rows ) ]
        reachabilityTable = [ [ 0 for _ in range( cols ) ] for _ in range( rows ) ]
        emptyCell, buildingCell, obstacleCell = 0, 1, 2

        def _bfs( u, v ):
            visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]
            visited[ u ][ v ] = True
           
            distance = 0

            q = deque()
            q.append( (u, v) )

            while len( q ) > 0:
                N = len( q )
                for _ in range( N ):
                    u, v = q.popleft()
                    distanceTable[ u ][ v ] += distance
                    reachabilityTable[ u ][ v ] += 1
                    for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                        x, y = u + du, v + dv
                        if 0 <= x < rows and 0 <= y < cols and grid[ x ][ y ] == emptyCell and not visited[ x ][ y ]:
                            visited[ x ][ y ] = True
                            q.append( (x, y) )
                distance += 1

        totalBuildings = 0
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] == buildingCell:
                totalBuildings += 1
                _bfs( u, v )

        bestDistance = float( 'inf' )
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] == emptyCell and reachabilityTable[ u ][ v ] == totalBuildings:
                bestDistance = min( bestDistance, distanceTable[ u ][ v ] )
        return -1 if bestDistance == float( 'inf' ) else bestDistance

class ShortestDistanceTest( unittest.TestCase ):
    def test_ShortestDistance_Sample( self ):
        grid = [[1,0,2,0,1],[0,0,0,0,0],[0,0,1,0,0]]
        self.assertEqual( ShortestDistance().shortestDistance( grid ), 7 )

        grid = [[1,0]]
        self.assertEqual( ShortestDistance().shortestDistance( grid ), 1 )

        grid = [[1]]
        self.assertEqual( ShortestDistance().shortestDistance( grid ), -1 )

'''
As Far from Land as Possible

Given an n x n grid containing only values 0 and 1, where 0 represents water and 1 represents land, find a water cell such that its distance to the nearest land cell is maximized, and return the distance. If no land or water exists in the grid, return -1.

The distance used in this problem is the Manhattan distance: the distance between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.
'''

class FarFromLand:
    def maxDistance(self, grid: List[List[int]]) -> int:
        rows, cols = len( grid ), len( grid[ 0 ] )
        visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]
        landCell, waterCell = 1, 0

        q = deque()
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] == landCell:
                q.append( (u, v) )
        
        distance = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                u, v = q.popleft()
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and grid[ x ][ y ] == waterCell and not visited[ x ][ y ]:
                        visited[ x ][ y ] = True
                        q.append( (x, y) )
            if len( q ) > 0:
                distance += 1
        return -1 if distance == 0 else distance

class FarFromLandTest( unittest.TestCase ):
    def test_FarFromLand_Sample( self ):
        grid = [[1,0,1],[0,0,0],[1,0,1]]
        self.assertEqual( FarFromLand().maxDistance( grid ), 2 )

        grid = [[1,0,0],[0,0,0],[0,0,0]]
        self.assertEqual( FarFromLand().maxDistance( grid ), 4 )

'''
Valid Palindrome III

Given a string s and an integer k, return true if s is a k-palindrome.

A string is k-palindrome if it can be transformed into a palindrome by removing at most k characters from it.
'''

class ValidPalindromeIII:
    def isValidPalindrome(self, s: str, k: int) -> bool:
        cache = dict()

        def _maximumLengthPalindrome( i, j ):
            cacheKey = (i, j)
            if cacheKey in cache:
                return cache[ cacheKey ]
            if i == j:
                L = 1
            elif i > j:
                L = 0
            elif s[ i ] == s[ j ]:
                L = 2 + _maximumLengthPalindrome( i + 1, j - 1 )
            else:
                L = max( _maximumLengthPalindrome( i + 1, j ), _maximumLengthPalindrome( i, j - 1 ) )
            cache[ cacheKey ] = L
            return cache[ cacheKey ]

        return _maximumLengthPalindrome( 0, len( s ) - 1 ) >= len( s ) - k

class ValidPalindromeIIITest( unittest.TestCase ):
    def test_ValidPalindromeIII_Sample( self ):
        s = "abcdeca"
        k = 2
        self.assertEqual( ValidPalindromeIII().isValidPalindrome( s, k ), True )

        s = "abbababa"
        k = 1
        self.assertEqual( ValidPalindromeIII().isValidPalindrome( s, k ), True )

'''
Path With Maximum Minimum Value

Given an m x n integer matrix grid, return the maximum score of a path starting at (0, 0) and ending at (m - 1, n - 1) moving in the 4 cardinal directions.

The score of a path is the minimum value in that path.

For example, the score of the path 8 → 4 → 5 → 9 is 4.
'''

class PathWithMaximumMinimumValue:
    def maximumMinimumPath(self, grid: List[List[int]]) -> int:
        rows, cols = len( grid ), len( grid[ 0 ] )
        scoreTable = [ [ float( 'inf' ) for _ in range( cols ) ] for _ in range( rows ) ]

        startScore = - grid[ 0 ][ 0 ]
        scoreTable[ 0 ][ 0 ] = startScore
        
        q = list()
        q.append( (startScore, 0, 0) )

        while len( q ) > 0:
            score, u, v = heapq.heappop( q )
            if u == rows - 1 and v == cols - 1:
                return - score
            for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                x, y = u + du, v + dv
                if not 0 <= x < rows or not 0 <= y < cols:
                    continue
                newScore = - min( - score, grid[ x ][ y ] )
                if newScore < scoreTable[ x ][ y ]:
                    scoreTable[ x ][ y ] = newScore
                    heapq.heappush( q, (newScore, x, y) )

class PathWithMaximumMinimumValueTest( unittest.TestCase ):
    def test_PathWithMaximumMinimumValue_Sample( self ):
        grid = [[5,4,5],[1,2,6],[7,4,6]]
        self.assertEqual( PathWithMaximumMinimumValue().maximumMinimumPath( grid ), 4 )

        grid = [[2,2,1,2,2,2],[1,2,2,2,1,2]]
        self.assertEqual( PathWithMaximumMinimumValue().maximumMinimumPath( grid ), 2 )

        grid = [[3,4,6,3,4],[0,2,1,1,7],[8,8,3,2,7],[3,2,4,9,8],[4,1,2,0,0],[4,6,5,4,3]]
        self.assertEqual( PathWithMaximumMinimumValue().maximumMinimumPath( grid ), 3 )

'''
Grid Game

You are given a 0-indexed 2D array grid of size 2 x n, where grid[r][c] represents the number of points at position (r, c) on the matrix. Two robots are playing a game on this matrix.

Both robots initially start at (0, 0) and want to reach (1, n-1). Each robot may only move to the right ((r, c) to (r, c + 1)) or down ((r, c) to (r + 1, c)).

At the start of the game, the first robot moves from (0, 0) to (1, n-1), collecting all the points from the cells on its path. For all cells (r, c) traversed on the path, grid[r][c] is set to 0. Then, the second robot moves from (0, 0) to (1, n-1), collecting the points on its path. Note that their paths may intersect with one another.

The first robot wants to minimize the number of points collected by the second robot. In contrast, the second robot wants to maximize the number of points it collects. If both robots play optimally, return the number of points collected by the second robot.
'''

class GridGame:
    def gridGame(self, grid: List[List[int]]) -> int:
        topRow, bottomRow = grid
        topRowSuffixSum = list()
        bottomRowPrefixSum = list()

        for point in reversed( topRow ):
            suffix = 0 if len( topRowSuffixSum ) == 0 else topRowSuffixSum[ -1 ]
            topRowSuffixSum.append( suffix + point )
        topRowSuffixSum.reverse()

        for point in bottomRow:
            prefix = 0 if len( bottomRowPrefixSum ) == 0 else bottomRowPrefixSum[ -1 ]
            bottomRowPrefixSum.append( prefix + point )

        minimumPoints = float( 'inf' )
        for i in range( len( topRow ) ):
            A = bottomRowPrefixSum[ i - 1 ] if i > 0 else 0
            B = topRowSuffixSum[ i + 1 ] if i + 1 < len( topRow ) else 0
            minimumPoints = min( minimumPoints, max( A, B ) )
        return minimumPoints

class GridGameTest( unittest.TestCase ):
    def test_GridGame_Sample( self ):
        grid = [[2,5,4],[1,5,1]]
        self.assertEqual( GridGame().gridGame( grid ), 4 )

        grid = [[3,3,1],[8,5,2]]
        self.assertEqual( GridGame().gridGame( grid ), 4 )

        grid = [[1,3,1,15],[1,3,3,1]]
        self.assertEqual( GridGame().gridGame( grid ), 7 )

'''
Buildings With an Ocean View

There are n buildings in a line. You are given an integer array heights of size n that represents the heights of the buildings in the line.

The ocean is to the right of the buildings. A building has an ocean view if the building can see the ocean without obstructions. Formally, a building has an ocean view if all the buildings to its right have a smaller height.

Return a list of indices (0-indexed) of buildings that have an ocean view, sorted in increasing order.
'''

class OceanView:
    def findBuildings(self, heights: List[int]) -> List[int]:
        maximumHeight = - float( 'inf' )
        oceanViewList = list()

        i = len( heights )
        while i > 0:
            i = i - 1
            if heights[ i ] > maximumHeight:
                oceanViewList.append( i )
            maximumHeight = max( maximumHeight, heights[ i ] )
        oceanViewList.reverse()
        return oceanViewList

class OceanViewTest( unittest.TestCase ):
    def test_OceanView_Sample( self ):
        heights = [4,2,3,1]
        self.assertEqual( OceanView().findBuildings( heights ), [0,2,3] )

        heights = [4,3,2,1]
        self.assertEqual( OceanView().findBuildings( heights ), [0,1,2,3] )

        heights = [1,3,2,4]
        self.assertEqual( OceanView().findBuildings( heights ), [3] )

        heights = [2,2,2,2]
        self.assertEqual( OceanView().findBuildings( heights ), [3] )

'''
Kill Process

You have n processes forming a rooted tree structure. You are given two integer arrays pid and ppid, where pid[i] is the ID of the ith process and ppid[i] is the ID of the ith process's parent process.

Each process has only one parent process but may have multiple children processes. Only one process has ppid[i] = 0, which means this process has no parent process (the root of the tree).

When a process is killed, all of its children processes will also be killed.

Given an integer kill representing the ID of a process you want to kill, return a list of the IDs of the processes that will be killed. You may return the answer in any order.
'''

class KillProcess:
    def killProcess(self, pid: List[int], ppid: List[int], kill: int) -> List[int]:
        adjacencyDict = defaultdict( lambda : list() )
        for i in range( len( ppid ) ):
            parentNode = ppid[ i ]
            childNode = pid[ i ]
            adjacencyDict[ parentNode ].append( childNode )

        killList = list()
        def _dfs( processId ):
            killList.append( processId )
            for childProcessId in adjacencyDict[ processId ]:
                _dfs( childProcessId )
        _dfs( kill )
        return killList

class KillProcessTest( unittest.TestCase ):
    def test_KillProcess_Sample( self ):
        pid = [1,3,10,5]
        ppid = [3,0,5,3]
        kill = 5
        self.assertEqual( KillProcess().killProcess( pid, ppid, kill ), [5, 10] )

        pid = [1]
        ppid = [0]
        kill = 1
        self.assertEqual( KillProcess().killProcess( pid, ppid, kill ), [1] )

'''
Find Anagram Mappings

You are given two integer arrays nums1 and nums2 where nums2 is an anagram of nums1. Both arrays may contain duplicates.

Return an index mapping array mapping from nums1 to nums2 where mapping[i] = j means the ith element in nums1 appears in nums2 at index j. If there are multiple answers, return any of them.

An array a is an anagram of an array b means b is made by randomizing the order of the elements in a.
'''

class AnagramMapping:
    def anagramMappings(self, nums1: List[int], nums2: List[int]) -> List[int]:
        indexDict = dict()
        for i, number in enumerate( nums2 ):
            indexDict[ number ] = i
        return [ indexDict[ number ] for number in nums1 ]

class AnagramMappingTest( unittest.TestCase ):
    def test_AnagramMapping_Sample( self ):
        nums1 = [12,28,46,32,50]
        nums2 = [50,12,32,46,28]
        self.assertEqual( AnagramMapping().anagramMappings( nums1, nums2 ), [1,4,3,2,0] )

        nums1 = [84,46]
        nums2 = [84,46]
        self.assertEqual( AnagramMapping().anagramMappings( nums1, nums2 ), [0,1] )

'''
Minimum Moves to Move a Box to Their Target Location

A storekeeper is a game in which the player pushes boxes around in a warehouse trying to get them to target locations.

The game is represented by an m x n grid of characters grid where each element is a wall, floor, or box.

Your task is to move the box 'B' to the target position 'T' under the following rules:

The character 'S' represents the player. The player can move up, down, left, right in grid if it is a floor (empty cell).
The character '.' represents the floor which means a free cell to walk.
The character '#' represents the wall which means an obstacle (impossible to walk there).
There is only one box 'B' and one target cell 'T' in the grid.
The box can be moved to an adjacent free cell by standing next to the box and then moving in the direction of the box. This is a push.
The player cannot walk through the box.
Return the minimum number of pushes to move the box to the target. If there is no way to reach the target, return -1.
'''

class MinimumBoxMove:
    def minPushBox(self, grid: List[List[str]]) -> int:
        emptyCell, blockedCell, startCell, boxCell, targetCell = '.#SBT'
        rows, cols = len( grid ), len( grid[ 0 ] )

        startLocation = None
        boxLocation = None
        targetLocation = None
        for u, v in itertools.product( range( rows ), range( cols ) ):
            if grid[ u ][ v ] == startCell:
                startLocation = u, v
            elif grid[ u ][ v ] == boxCell:
                boxLocation = u, v
            elif grid[ u ][ v ] == targetCell:
                targetLocation = u, v
        
        visited = set()
        q = deque()
        q.append( (0, startLocation, boxLocation) )

        while len( q ) > 0:
            distancePushed, currentLocation, currentBoxLocation = q.popleft()
            if currentBoxLocation == targetLocation:
                return distancePushed
            if (currentLocation, currentBoxLocation) in visited:
                continue
            visited.add( (currentLocation, currentBoxLocation) )
            
            bu, bv = currentBoxLocation
            u, v = currentLocation
            for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                x, y = newLocation = u + du, v + dv
                if not 0 <= x < rows or not 0 <= y < cols:
                    continue
                if grid[ x ][ y ] == blockedCell:
                    continue
                newDistancePushed = distancePushed
                newBoxLocation = currentBoxLocation
                if newLocation == currentBoxLocation:
                    new_bu, new_bv = bu + du, bv + dv
                    if not 0 <= new_bu < rows or not 0 <= new_bv < cols or grid[ new_bu ][ new_bv ] == blockedCell:
                        continue
                    newDistancePushed += 1
                    newBoxLocation = new_bu, new_bv
                if (newLocation, newBoxLocation) in visited:
                    continue
                if newDistancePushed == distancePushed:
                    q.appendleft( (newDistancePushed, newLocation, newBoxLocation) )
                else:
                    q.append( (newDistancePushed, newLocation, newBoxLocation) )
        return -1

class MinimumBoxMoveTest( unittest.TestCase ):
    def test_MinimumBoxMove_Sample( self ):
        grid = [
            ["#","#","#","#","#","#"],
            ["#","T","#","#","#","#"],
            ["#",".",".","B",".","#"],
            ["#",".","#","#",".","#"],
            ["#",".",".",".","S","#"],
            ["#","#","#","#","#","#"]
        ]
        self.assertEqual( MinimumBoxMove().minPushBox( grid ), 3 )

        grid = [
            ["#","#","#","#","#","#"],
            ["#","T","#","#","#","#"],
            ["#",".",".","B",".","#"],
            ["#","#","#","#",".","#"],
            ["#",".",".",".","S","#"],
            ["#","#","#","#","#","#"]
        ]
        self.assertEqual( MinimumBoxMove().minPushBox( grid ), -1 )

        grid = [
            ["#","#","#","#","#","#"],
            ["#","T",".",".","#","#"],
            ["#",".","#","B",".","#"],
            ["#",".",".",".",".","#"],
            ["#",".",".",".","S","#"],
            ["#","#","#","#","#","#"]
        ]
        self.assertEqual( MinimumBoxMove().minPushBox( grid ), 5 )

        grid = [
            ["#","#","#","#","#","#","#"],
            ["#","S","#",".","B","T","#"],
            ["#","#","#","#","#","#","#"]
        ]
        self.assertEqual( MinimumBoxMove().minPushBox( grid ), -1 )

'''
Shortest Word Distance

Given an array of strings wordsDict and two different strings that already exist in the array word1 and word2, return the shortest distance between these two words in the list.
'''

class ShortestWordDistance:
    def shortestDistance(self, wordsDict: List[str], word1: str, word2: str) -> int:
        distance = float( 'inf' )
        word1Index = - float( 'inf' )
        word2Index = - float( 'inf' )
        for index, word in enumerate( wordsDict ):
            if word1 == word:
                word1Index = index
            elif word2 == word:
                word2Index = index
            distance = min( distance, abs( word1Index - word2Index ) )
        return distance

class ShortestWordDistanceTest( unittest.TestCase ):
    def test_ShortestWordDistance_Sample( self ):
        wordsDict = ["practice", "makes", "perfect", "coding", "makes"]
        word1 = "coding"
        word2 = "practice"
        self.assertEqual( ShortestWordDistance().shortestDistance( wordsDict, word1, word2 ), 3 )

        wordsDict = ["practice", "makes", "perfect", "coding", "makes"]
        word1 = "makes"
        word2 = "coding"
        self.assertEqual( ShortestWordDistance().shortestDistance( wordsDict, word1, word2 ), 1 )

'''
Number of Enclaves

You are given an m x n binary matrix grid, where 0 represents a sea cell and 1 represents a land cell.

A move consists of walking from one land cell to another adjacent (4-directionally) land cell or walking off the boundary of the grid.

Return the number of land cells in grid for which we cannot walk off the boundary of the grid in any number of moves.
'''

class NumberOfEnclaves:
    def numEnclaves(self, grid: List[List[int]]) -> int:
        rows, cols = len( grid ), len( grid[ 0 ] )

        def _floodFill( row, col ):
            stack = list()
            stack.append( (row, col) )

            while len( stack ) > 0:
                u, v = stack.pop()
                if grid[ u ][ v ] == 1:
                    grid[ u ][ v ] = 0
                    for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                        x, y = u + du, v + dv
                        if 0 <= x < rows and 0 <= y < cols and grid[ x ][ y ] == 1:
                            stack.append( (x, y) )
        
        for row in (0, rows - 1):
            for col in range( cols ):
                if grid[ row ][ col ] == 1:
                    _floodFill( row, col )
        for col in (0, cols - 1):
            for row in range( rows ):
                if grid[ row ][ col ] == 1:
                    _floodFill( row, col )

        totalCells = 0
        for gridRow in grid:
            totalCells += gridRow.count( 1 )
        return totalCells

class NumberOfEnclavesTest( unittest.TestCase ):
    def test_NumberOfEnclaves_Sample( self ):
        grid = [[0,0,0,0],[1,0,1,0],[0,1,1,0],[0,0,0,0]]
        self.assertEqual( NumberOfEnclaves().numEnclaves( grid ), 3 )

        grid = [[0,1,1,0],[0,0,1,0],[0,0,1,0],[0,0,0,0]]
        self.assertEqual( NumberOfEnclaves().numEnclaves( grid ), 0 )

'''
Nearest Exit from Entrance in Maze

You are given an m x n matrix maze (0-indexed) with empty cells (represented as '.') and walls (represented as '+'). You are also given the entrance of the maze, where entrance = [entrancerow, entrancecol] denotes the row and column of the cell you are initially standing at.

In one step, you can move one cell up, down, left, or right. You cannot step into a cell with a wall, and you cannot step outside the maze. Your goal is to find the nearest exit from the entrance. An exit is defined as an empty cell that is at the border of the maze. The entrance does not count as an exit.

Return the number of steps in the shortest path from the entrance to the nearest exit, or -1 if no such path exists.
'''

class NearestExit:
    def nearestExit(self, maze: List[List[str]], entrance: List[int]) -> int:
        rows, cols = len( maze ), len( maze[ 0 ] )
        emptyCell = '.'
        
        x, y = entrance
        q = deque()
        q.append( (x, y) )
        
        visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]
        visited[ x ][ y ] = True

        distance = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                u, v = q.popleft()
                if distance != 0 and ( u == 0 or u == rows - 1 or v == 0 or v == cols - 1 ):
                    return distance
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and maze[ x ][ y ] == emptyCell and not visited[ x ][ y ]:
                        visited[ x ][ y ] = True
                        q.append( (x, y) )
            distance += 1
        return -1

class NearestExitTest( unittest.TestCase ):
    def test_NearestExit_Sample( self ):
        maze = [["+","+",".","+"],[".",".",".","+"],["+","+","+","."]]
        entrance = [1,2]
        self.assertEqual( NearestExit().nearestExit( maze, entrance ), 1 )

        maze = [["+","+","+"],[".",".","."],["+","+","+"]]
        entrance = [1,0]
        self.assertEqual( NearestExit().nearestExit( maze, entrance ), 2 )

        maze = [[".","+"]]
        entrance = [0,0]
        self.assertEqual( NearestExit().nearestExit( maze, entrance ), -1 )

'''
Rotating the Box

ou are given an m x n matrix of characters box representing a side-view of a box. Each cell of the box is one of the following:

A stone '#'
A stationary obstacle '*'
Empty '.'
The box is rotated 90 degrees clockwise, causing some of the stones to fall due to gravity. Each stone falls down until it lands on an obstacle, another stone, or the bottom of the box. Gravity does not affect the obstacles' positions, and the inertia from the box's rotation does not affect the stones' horizontal positions.

It is guaranteed that each stone in box rests on an obstacle, another stone, or the bottom of the box.

Return an n x m matrix representing the box after the rotation described above.
'''

class RotatingTheBox:
    def rotateTheBox(self, box: List[List[str]]) -> List[List[str]]:
        stoneCell, emptyCell = '#', '.'
        for boxRow in box:
            for i in reversed( range( len( boxRow ) ) ):
                # If boxRow[ i ] is a stone, push it as far right as possible.
                if boxRow[ i ] != stoneCell:
                    continue
                j = i
                while j + 1 < len( boxRow ) and boxRow[ j + 1 ] == emptyCell:
                    j = j + 1
                boxRow[ i ], boxRow[ j ] = boxRow[ j ], boxRow[ i ]
        rows, cols = len( box ), len( box[ 0 ] )
        newBox = [ [ None for _ in range( rows ) ] for _ in range( cols ) ]
        for u in range( rows ):
            for v in range( cols ):
                newBox[ v ][ rows - 1 - u ] = box[ u ][ v ]
        return newBox

class RotatingTheBoxTest( unittest.TestCase ):
    def test_RotatingTheBox_Sample( self ):
        box = [
            ["#",".","#"]
        ]
        rotatedBox = [
            ["."],
            ["#"],
            ["#"]
        ]
        self.assertEqual( RotatingTheBox().rotateTheBox( box ), rotatedBox )

        box = [
            ["#",".","*","."],
            ["#","#","*","."]
        ]
        rotatedBox = [
            ["#","."],
            ["#","#"],
            ["*","*"],
            [".","."]
        ]
        self.assertEqual( RotatingTheBox().rotateTheBox( box ), rotatedBox )

        box = [
            ["#","#","*",".","*","."],
            ["#","#","#","*",".","."],
            ["#","#","#",".","#","."]
        ]
        rotatedBox = [
            [".","#","#"],
            [".","#","#"],
            ["#","#","*"],
            ["#","*","."],
            ["#",".","*"],
            ["#",".","."]
        ]
        self.assertEqual( RotatingTheBox().rotateTheBox( box ), rotatedBox )

'''
Shortest Path with Alternating Colors

Consider a directed graph, with nodes labelled 0, 1, ..., n-1.  In this graph, each edge is either red or blue, and there could be self-edges or parallel edges.

Each [i, j] in red_edges denotes a red directed edge from node i to node j.  Similarly, each [i, j] in blue_edges denotes a blue directed edge from node i to node j.

Return an array answer of length n, where each answer[X] is the length of the shortest path from node 0 to node X such that the edge colors alternate along the path (or -1 if such a path doesn't exist).
'''

class ShortestPathWithAlternatingColors:
    def shortestAlternatingPaths(self, n: int, red_edges: List[List[int]], blue_edges: List[List[int]]) -> List[int]:
        redGraph = [ list() for _ in range( n ) ]
        blueGraph = [ list() for _ in range( n ) ]
        colorRed, colorBlue, colorNone, numberOfColors = 0, 1, 2, 3

        for (u, v) in red_edges:
            redGraph[ u ].append( v )
        for (u, v) in blue_edges:
            blueGraph[ u ].append( v )

        q = deque()
        q.append( (0, colorNone) )

        pathLengthList = [ -1 for _ in range( n ) ]
        visited = [ [ False for _ in range( numberOfColors ) ] for _ in range( n ) ]
        visited[ 0 ][ colorNone ] = True

        distance = 0
        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                vertex, previousColor = q.popleft()
                if pathLengthList[ vertex ] == -1:
                    pathLengthList[ vertex ] = distance
                if previousColor == colorNone or previousColor == colorBlue:
                    for adjacentVertex in redGraph[ vertex ]:
                        if not visited[ adjacentVertex ][ colorRed ]:
                            visited[ adjacentVertex ][ colorRed ] = True
                            q.append( (adjacentVertex, colorRed) )
                if previousColor == colorNone or previousColor == colorRed:
                    for adjacentVertex in blueGraph[ vertex ]:
                        if not visited[ adjacentVertex ][ colorBlue ]:
                            visited[ adjacentVertex ][ colorBlue ] = True
                            q.append( (adjacentVertex, colorBlue) )
            distance += 1
        return pathLengthList

class ShortestPathWithAlternatingColorsTest( unittest.TestCase ):
    def test_ShortestPathWithAlternatingColors_Sample( self ):
        n = 3
        red_edges = [[0,1],[1,2]]
        blue_edges = []
        self.assertEqual( ShortestPathWithAlternatingColors().shortestAlternatingPaths( n, red_edges, blue_edges ), [0,1,-1] )

        n = 3
        red_edges = [[0,1]]
        blue_edges = [[2,1]]
        self.assertEqual( ShortestPathWithAlternatingColors().shortestAlternatingPaths( n, red_edges, blue_edges ), [0,1,-1] )

        n = 3
        red_edges = [[1,0]]
        blue_edges = [[2,1]]
        self.assertEqual( ShortestPathWithAlternatingColors().shortestAlternatingPaths( n, red_edges, blue_edges ), [0,-1,-1] )

        n = 3
        red_edges = [[0,1]]
        blue_edges = [[1,2]]
        self.assertEqual( ShortestPathWithAlternatingColors().shortestAlternatingPaths( n, red_edges, blue_edges ), [0,1,2] )

        n = 3
        red_edges = [[0,1],[0,2]]
        blue_edges = [[1,0]]
        self.assertEqual( ShortestPathWithAlternatingColors().shortestAlternatingPaths( n, red_edges, blue_edges ), [0,1,1] )

'''
Binary Tree Longest Consecutive Sequence

Given the root of a binary tree, return the length of the longest consecutive sequence path.

The path refers to any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The longest consecutive path needs to be from parent to child (cannot be the reverse).
'''

class TreeNode:
    def __init__( self, val=0, left=None, right=None ):
        self.val = val
        self.left = left
        self.right = right

def buildTree( treeData ):
    root = None
    index = 0
    
    q = deque()
    if index < len( treeData ):
        root = TreeNode( treeData[ index ] )
        index += 1
        q.append( root )

    while index < len( treeData ):
        treeNode = q.popleft()
        leftEntry = treeData[ index ]
        index += 1
        rightEntry = None
        if index < len( treeData ):
            rightEntry = treeData[ index ]
            index += 1
        if leftEntry is not None:
            leftSubtree = TreeNode( leftEntry )
            treeNode.left = leftSubtree
            q.append( leftSubtree )
        if rightEntry is not None:
            rightSubtree = TreeNode( rightEntry )
            treeNode.right = rightSubtree
            q.append( rightSubtree )
    return root

class BinaryTreeLongestConsecutiveSequence:
    def longestConsecutive(self, root: Optional[TreeNode]) -> int:
        maximumLength = 1
        def _consecutive( root ):
            nonlocal maximumLength
            if root is None:
                return 0
            length = 1
            L = _consecutive( root.left )
            if root.left is not None and root.val + 1 == root.left.val:
                length = max( length, L + 1 )
            R = _consecutive( root.right )
            if root.right is not None and root.val + 1 == root.right.val:
                length = max( length, R + 1 )
            maximumLength = max( maximumLength, length )
            return length
        _consecutive( root )
        return maximumLength

class BinaryTreeLongestConsecutiveSequenceTest( unittest.TestCase ):
    def test_BinaryTreeLongestConsecutiveSequence_Sample( self ):
        treeData = [1,None,3,2,4,None,None,None,5]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeLongestConsecutiveSequence().longestConsecutive( root ), 3 )

        treeData = [2,None,3,2,None,1]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeLongestConsecutiveSequence().longestConsecutive( root ), 2 )

'''
Binary Tree Longest Consecutive Sequence II

Given the root of a binary tree, return the length of the longest consecutive path in the tree.

A consecutive path is a path where the values of the consecutive nodes in the path differ by one. This path can be either increasing or decreasing.

For example, [1,2,3,4] and [4,3,2,1] are both considered valid, but the path [1,2,4,3] is not valid.
On the other hand, the path can be in the child-Parent-child order, where not necessarily be parent-child order.
'''

class BinaryTreeLongestConsecutiveSequenceII:
    def longestConsecutive(self, root: Optional[TreeNode]) -> int:
        maximumLength = 1
        def _consecutive( root ):
            nonlocal maximumLength
            if root is None:
                return 0, 0
            increasingLeft, decreasingLeft = _consecutive( root.left )
            increasingRight, decreasingRight = _consecutive( root.right )
            increasing = decreasing = 1
            if root.left is not None and root.val + 1 == root.left.val:
                increasing = max( increasing, increasingLeft + 1 )
            if root.right is not None and root.val + 1 == root.right.val:
                increasing = max( increasing, increasingRight + 1 )
            if root.left is not None and root.val - 1 == root.left.val:
                decreasing = max( decreasing, decreasingLeft + 1 )
            if root.right is not None and root.val - 1 == root.right.val:
                decreasing = max( decreasing, decreasingRight + 1 )
            maximumLength = max( maximumLength, increasing, decreasing, increasing + decreasing - 1 )
            return increasing, decreasing
        _consecutive( root )
        return maximumLength

class BinaryTreeLongestConsecutiveSequenceIITest( unittest.TestCase ):
    def test_BinaryTreeLongestConsecutiveSequenceII_Sample( self ):
        treeData = [1,2,3]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeLongestConsecutiveSequenceII().longestConsecutive( root ), 2 )

        treeData = [2,1,3]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeLongestConsecutiveSequenceII().longestConsecutive( root ), 3 )

'''
Path with Maximum Probability

You are given an undirected weighted graph of n nodes (0-indexed), represented by an edge list where edges[i] = [a, b] is an undirected edge connecting the nodes a and b with a probability of success of traversing that edge succProb[i].

Given two nodes start and end, find the path with the maximum probability of success to go from start to end and return its success probability.

If there is no path from start to end, return 0. Your answer will be accepted if it differs from the correct answer by at most 1e-5.
'''

class PathWithMaximumProbability:
    def maxProbability(self, n: int, edges: List[List[int]], succProb: List[float], start: int, end: int) -> float:
        graph = [ list() for _ in range( n ) ]
        for index, (u, v) in enumerate( edges ):
            graph[ u ].append( (v, index) )
            graph[ v ].append( (u, index) )
        
        probabilityTable = [ None for _ in range( n ) ]
        probabilityTable[ start ] = 1.0
        
        q = list()
        q.append( (start, - 1.0) )

        while len( q ) > 0:
            vertex, probability = heapq.heappop( q )
            probability = - probability

            if vertex == end:
                return probability
            if probabilityTable[ vertex ] is not None and probabilityTable[ vertex ] > probability:
                continue
            for adjacentVertex, index in graph[ vertex ]:
                successProbability = succProb[ index ]
                totalProbability = probability * successProbability
                if probabilityTable[ adjacentVertex ] is None or totalProbability > probabilityTable[ adjacentVertex ]:
                    probabilityTable[ adjacentVertex ] = totalProbability
                    heapq.heappush( q, (adjacentVertex, - totalProbability) )
        return 0

class PathWithMaximumProbabilityTest( unittest.TestCase ):
    def test_maxProbability_Sample( self ):
        n = 3
        edges = [[0,1],[1,2],[0,2]]
        succProb = [0.5,0.5,0.2]
        start, end = 0, 2
        self.assertAlmostEqual( PathWithMaximumProbability().maxProbability( n, edges, succProb, start, end ), 0.25 )

        n = 3
        edges = [[0,1],[1,2],[0,2]]
        succProb = [0.5,0.5,0.3]
        start, end = 0, 2
        self.assertAlmostEqual( PathWithMaximumProbability().maxProbability( n, edges, succProb, start, end ), 0.3 )

        n = 3
        edges = [[0,1]]
        succProb = [0.5]
        start, end = 0, 2
        self.assertAlmostEqual( PathWithMaximumProbability().maxProbability( n, edges, succProb, start, end ), 0 )

'''
You are given the root of a binary tree. We install cameras on the tree nodes where each camera at a node can monitor its parent, itself, and its immediate children.

Return the minimum number of cameras needed to monitor all nodes of the tree.
'''

class BinaryTreeCameras:
    def minCameraCover(self, root: Optional[TreeNode]) -> int:
        return

class BinaryTreeCamerasTest( unittest.TestCase ):
    def test_BinaryTreeCameras_Sample( self ):
        treeData = [0,0,None,0,0]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeCameras().minCameraCover( root ), 1 )

        treeData = [0,0,None,0,None,0,None,None,0]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeCameras().minCameraCover( root ), 2 )

'''
Cat and Mouse

A game on an undirected graph is played by two players, Mouse and Cat, who alternate turns.

The graph is given as follows: graph[a] is a list of all nodes b such that ab is an edge of the graph.

The mouse starts at node 1 and goes first, the cat starts at node 2 and goes second, and there is a hole at node 0.

During each player's turn, they must travel along one edge of the graph that meets where they are.  For example, if the Mouse is at node 1, it must travel to any node in graph[1].

Additionally, it is not allowed for the Cat to travel to the Hole (node 0.)

Then, the game can end in three ways:

If ever the Cat occupies the same node as the Mouse, the Cat wins.
If ever the Mouse reaches the Hole, the Mouse wins.
If ever a position is repeated (i.e., the players are in the same position as a previous turn, and it is the same player's turn to move), the game is a draw.
Given a graph, and assuming both players play optimally, return

1 if the mouse wins the game,
2 if the cat wins the game, or
0 if the game is a draw.
'''

class CatAndMouse:
    def catMouseGame(self, graph: List[List[int]]) -> int:
        n = len( graph )
        mousePosition, catPosition, homePosition = 1, 2, 0
        mouseCanWin = catCanWin = draw = False
        mouseToMove = True
        visited = [ [ [ False for _ in range( n ) ] for _ in range( n ) ] for _ in range( 2 ) ]
        toMoveIndex = {
            True : 0,
            False : 1
        }

        q = deque()
        q.append( (mousePosition, catPosition, mouseToMove) )
        visited[ toMoveIndex[ mouseToMove ] ][ mousePosition ][ catPosition ] = True

        while len( q ) > 0:
            N = len( q )
            for _ in range( N ):
                mousePosition, catPosition, mouseToMove = q.popleft()
                if mouseToMove:
                    for newMousePosition in graph[ mousePosition ]:
                        if not visited[ toMoveIndex[ False ] ][ newMousePosition ][ catPosition ]:
                            visited[ toMoveIndex[ False ] ][ newMousePosition ][ catPosition ] = True
                            q.append( (newMousePosition, catPosition, False) )
                else:
                    for newCatPosition in graph[ catPosition ]:
                        if newCatPosition == homePosition:
                            continue
                        if not visited[ toMoveIndex[ True ] ][ mousePosition ][ newCatPosition ]:
                            visited[ toMoveIndex[ True ] ][ mousePosition ][ newCatPosition ] = True
                            q.append( (mousePosition, newCatPosition, True) )

class CatAndMouseTest( unittest.TestCase ):
    def test_CatAndMouse_Sample( self ):
        graph = [[2,5],[3],[0,4,5],[1,4,5],[2,3],[0,2,3]]
        self.assertEqual( CatAndMouse().catMouseGame( graph ), 0 )

        graph = [[1,3],[0],[3],[0,2]]
        self.assertEqual( CatAndMouse().catMouseGame( graph ), 0 )

'''
Number of Atoms

Given a string formula representing a chemical formula, return the count of each atom.

The atomic element always starts with an uppercase character, then zero or more lowercase letters, representing the name.

One or more digits representing that element's count may follow if the count is greater than 1. If the count is 1, no digits will follow.

For example, "H2O" and "H2O2" are possible, but "H1O2" is impossible.
Two formulas are concatenated together to produce another formula.

For example, "H2O2He3Mg4" is also a formula.
A formula placed in parentheses, and a count (optionally added) is also a formula.

For example, "(H2O2)" and "(H2O2)3" are formulas.
Return the count of all elements as a string in the following form: the first name (in sorted order), followed by its count (if that count is more than 1), followed by the second name (in sorted order), followed by its count (if that count is more than 1), and so on.
'''

class AtomTokenizer:
    TOKEN_ATOM = 0
    TOKEN_OPEN_BRACKET = 1
    TOKEN_CLOSE_BRACKET = 2
    TOKEN_COUNT = 3

    def __init__( self, formula ):
        self.formula = formula
        self.readIndex = 0

    def next( self ):
        if self.readIndex >= len( self.formula ):
            return None
        token = self.formula[ self.readIndex ]
        self.readIndex += 1
        if token == '(':
            return AtomTokenizer.TOKEN_OPEN_BRACKET, None
        elif token == ')':
            return AtomTokenizer.TOKEN_CLOSE_BRACKET, None
        if token.isupper():
            atomName = token
            while self.readIndex < len( self.formula ) and self.formula[ self.readIndex ].islower():
                atomName += self.formula[ self.readIndex ]
                self.readIndex += 1
            return AtomTokenizer.TOKEN_ATOM, atomName
        if token.isnumeric():
            count = int( token )
            while self.readIndex < len( self.formula ) and self.formula[ self.readIndex ].isnumeric():
                count = count * 10 + int( self.formula[ self.readIndex ] )
                self.readIndex += 1
            return AtomTokenizer.TOKEN_COUNT, count

class NumberOfAtoms:
    def countOfAtoms(self, formula: str) -> str:
        tokenizer = AtomTokenizer( formula )
        stack = list()
        contextStack = list()
        
        while True:
            tokenInfo = tokenizer.next()
            if tokenInfo is None:
                break
            tokenType, tokenValue = tokenInfo
            if tokenType == AtomTokenizer.TOKEN_OPEN_BRACKET:
                stack.append( tokenInfo )
            elif tokenType == AtomTokenizer.TOKEN_CLOSE_BRACKET:
                _, count = tokenizer.next()
                while True:
                    stackTokenType, stackTokenValue = stack.pop()
                    if stackTokenType == AtomTokenizer.TOKEN_OPEN_BRACKET:
                        break
                    
class NumberOfAtomsTest( unittest.TestCase ):
    def test_NumberOfAtoms_Sample( self ):
        formula = "H2O"
        self.assertEqual( NumberOfAtoms().countOfAtoms( formula ), "H2O" )

        formula = "Mg(OH)2"
        self.assertEqual( NumberOfAtoms().countOfAtoms( formula ), "H2MgO2" )

        formula = "K4(ON(SO3)2)2"
        self.assertEqual( NumberOfAtoms().countOfAtoms( formula ), "K4N2O14S4" )

        formula = "Be32"
        self.assertEqual( NumberOfAtoms().countOfAtoms( formula ), "Be32" )

'''
Employee Importance

You have a data structure of employee information, including the employee's unique ID, importance value, and direct subordinates' IDs.

You are given an array of employees "employees" where:

employees[i].id is the ID of the ith employee.
employees[i].importance is the importance value of the ith employee.
employees[i].subordinates is a list of the IDs of the direct subordinates of the ith employee.
Given an integer id that represents an employee's ID, return the total importance value of this employee and all their direct and indirect subordinates.
'''

class Employee:
    def __init__(self, id: int, importance: int, subordinates: List[int]):
        self.id = id
        self.importance = importance
        self.subordinates = subordinates

    @staticmethod
    def constructEmployeeList( employeeInfoList ):
        employeeList = list()
        for id, importance, subordinates in employeeInfoList:
            employeeList.append( Employee( id, importance, subordinates ) )
        return employeeList

class EmployeeImportance:
    def getImportance(self, employees: List['Employee'], id: int) -> int:
        employeeMap = dict()
        for employee in employees:
            id_ = employee.id
            employeeMap[ id_ ] = (employee.importance, employee.subordinates)

        def _dfs( employeeId ):
            importance, subordinatesList = employeeMap[ employeeId ]
            for id_ in subordinatesList:
                importance += _dfs( id_ )
            return importance
        return _dfs( id )

class EmployeeImportanceTest( unittest.TestCase ):
    def test_EmployeeImportance( self ):
        employees = Employee.constructEmployeeList( [[1,5,[2,3]],[2,3,[]],[3,3,[]]] )
        id = 1
        self.assertEqual( EmployeeImportance().getImportance( employees, id ), 11 )

        employees = Employee.constructEmployeeList( [[1,2,[5]],[5,-3,[]]] )
        id = 5
        self.assertEqual( EmployeeImportance().getImportance( employees, id ), -3 )

'''
Path Sum III

Given the root of a binary tree and an integer targetSum, return the number of paths where the sum of the values along the path equals targetSum.

The path does not need to start or end at the root or a leaf, but it must go downwards (i.e., traveling only from parent nodes to child nodes).
'''

class PathSumIII:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> int:
        count = 0
        def _pathSum( root ):
            nonlocal count
            if root is None:
                return defaultdict( int )
            newSumDict = defaultdict( int )
            for sum_, count_ in _pathSum( root.left ).items():
                newSumDict[ root.val + sum_ ] += count_
            for sum_, count_ in _pathSum( root.right ).items():
                newSumDict[ root.val + sum_ ] += count_
            newSumDict[ root.val ] += 1
            count += newSumDict[ targetSum ]
            return newSumDict
        _pathSum( root )
        return count

class PathSumIIITest( unittest.TestCase ):
    def test_PathSumIII( self ):
        treeData = [10,5,-3,3,2,None,11,3,-2,None,1]
        root = buildTree( treeData )
        targetSum = 8
        self.assertEqual( PathSumIII().pathSum( root, targetSum ), 3 )

        treeData = [5,4,8,11,None,13,4,7,2,None,None,5,1]
        root = buildTree( treeData )
        targetSum = 22
        self.assertEqual( PathSumIII().pathSum( root, targetSum ), 3 )

'''
Count Sub Islands

You are given two m x n binary matrices grid1 and grid2 containing only 0's (representing water) and 1's (representing land). An island is a group of 1's connected 4-directionally (horizontal or vertical). Any cells outside of the grid are considered water cells.

An island in grid2 is considered a sub-island if there is an island in grid1 that contains all the cells that make up this island in grid2.

Return the number of islands in grid2 that are considered sub-islands.
'''

class SubIslands:
    def countSubIslands(self, grid1: List[List[int]], grid2: List[List[int]]) -> int:
        subIslandCount = 0
        rows, cols = len( grid1 ), len( grid1[ 0 ] )
        assert rows == len( grid2 ) and cols == len( grid2[ 0 ] )

        def _bfs( u, v ):
            q = deque()
            q.append( (u, v) )
            
            count = 0
            isSubIsland = True
            
            while len( q ) > 0:
                u, v = q.popleft()
                if grid2[ u ][ v ] == 0:
                    continue
                grid2[ u ][ v ] = 0
                if grid1[ u ][ v ] == 0:
                    isSubIsland = False
                count += 1
                
                for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                    x, y = u + du, v + dv
                    if 0 <= x < rows and 0 <= y < cols and grid2[ x ][ y ] == 1:
                        q.append( (x, y) )
            return count > 0 and isSubIsland

        for u, v in itertools.product( range( rows ), range( cols ) ):
            if _bfs( u, v ):
                subIslandCount += 1

        return subIslandCount

class SubIslandsTest( unittest.TestCase ):
    def test_SubIslands( self ):
        grid1 = [[1,1,1,0,0],[0,1,1,1,1],[0,0,0,0,0],[1,0,0,0,0],[1,1,0,1,1]]
        grid2 = [[1,1,1,0,0],[0,0,1,1,1],[0,1,0,0,0],[1,0,1,1,0],[0,1,0,1,0]]
        self.assertEqual( SubIslands().countSubIslands( grid1, grid2 ), 3 )

        grid1 = [[1,0,1,0,1],[1,1,1,1,1],[0,0,0,0,0],[1,1,1,1,1],[1,0,1,0,1]]
        grid2 = [[0,0,0,0,0],[1,1,1,1,1],[0,1,0,1,0],[0,1,0,1,0],[1,0,0,0,1]]
        self.assertEqual( SubIslands().countSubIslands( grid1, grid2 ), 2 )

'''
Maximum Product of Splitted Binary Tree

Given the root of a binary tree, split the binary tree into two subtrees by removing one edge such that the product of the sums of the subtrees is maximized.

Return the maximum product of the sums of the two subtrees. Since the answer may be too large, return it modulo 10^9 + 7.
'''

def treeSum( root ):
    totalSum = 0
    
    stack = list()
    stack.append( root )
    
    while len( stack ) > 0:
        root = stack.pop()
        if root is not None:
            totalSum += root.val
            stack.append( root.left )
            stack.append( root.right )
    return totalSum

class MaximumProductSplitBinaryTree:
    def maxProduct(self, root: Optional[TreeNode]) -> int:
        modulo = 10 ** 9 + 7
        product = 1
        totalSum = treeSum( root )

        def _maxProduct( root ):
            nonlocal product
            if root is None:
                return 0
            sumLeftSubtree = _maxProduct( root.left )
            sumRightSubtree = _maxProduct( root.right )
            product = max( product, ( totalSum - sumLeftSubtree ) * sumLeftSubtree )
            product = max( product, ( totalSum - sumRightSubtree ) * sumRightSubtree )
            return sumLeftSubtree + sumRightSubtree + root.val
        _maxProduct( root )

        return product % modulo

class MaximumProductSplitBinaryTreeTest( unittest.TestCase ):
    def test_MaximumProductSplitBinaryTree( self ):
        treeData = [1,2,3,4,5,6]
        root = buildTree( treeData )
        self.assertEqual( MaximumProductSplitBinaryTree().maxProduct( root ), 110 )

        treeData = [1,None,2,3,4,None,None,5,6]
        root = buildTree( treeData )
        self.assertEqual( MaximumProductSplitBinaryTree().maxProduct( root ), 90 )

'''
Count Nodes With the Highest Score

There is a binary tree rooted at 0 consisting of n nodes. The nodes are labeled from 0 to n - 1. You are given a 0-indexed integer array parents representing the tree, where parents[i] is the parent of node i. Since node 0 is the root, parents[0] == -1.

Each node has a score. To find the score of a node, consider if the node and the edges connected to it were removed. The tree would become one or more non-empty subtrees. The size of a subtree is the number of the nodes in it. The score of the node is the product of the sizes of all those subtrees.

Return the number of nodes that have the highest score.
'''

class HighestScoreInTree:
    def countHighestScoreNodes(self, parents: List[int]) -> int:
        N = len( parents )
        treeNodeList = [ TreeNode() for _ in range( N ) ]
        root, * _ = treeNodeList
        
        for id, parentId in enumerate( parents ):
            if parentId == -1:
                continue
            parentNode = treeNodeList[ parentId ]
            if parentNode.left is None:
                parentNode.left = treeNodeList[ id ]
            elif parentNode.right is None:
                parentNode.right = treeNodeList[ id ]
            else:
                assert False, "Not a Binary Tree"

        scoreDict = defaultdict( int )
        def _score( root ):
            if root is None:
                return 0
            leftSubtree = _score( root.left )
            rightSubtree = _score( root.right )
            count = leftSubtree + rightSubtree + 1

            A = max( 1, leftSubtree )
            B = max( 1, rightSubtree )
            C = max( 1, N - count )
            score = A * B * C
            scoreDict[ score ] += 1

            return count
        
        _score( root )
        maximumScore = max( scoreDict.keys() )
        return scoreDict[ maximumScore ]

class HighestScoreInTreeTest( unittest.TestCase ):
    def test_HighestScoreInTree( self ):
        parents = [-1,2,0,2,0]
        self.assertEqual( HighestScoreInTree().countHighestScoreNodes( parents ), 3 )

        parents = [-1,2,0]
        self.assertEqual( HighestScoreInTree().countHighestScoreNodes( parents ), 2 )

'''
Find the Town Judge

In a town, there are n people labeled from 1 to n. There is a rumor that one of these people is secretly the town judge.

If the town judge exists, then:

The town judge trusts nobody.
Everybody (except for the town judge) trusts the town judge.
There is exactly one person that satisfies properties 1 and 2.
You are given an array trust where trust[i] = [ai, bi] representing that the person labeled ai trusts the person labeled bi.

Return the label of the town judge if the town judge exists and can be identified, or return -1 otherwise.
'''

class TownJudge:
    def findJudge(self, n: int, trust: List[List[int]]) -> int:
        outDegree = [ 0 for _ in range( n + 1 ) ]
        inDegree = [ 0 for _ in range( n + 1 ) ]

        for a, b in trust:
            outDegree[ a ] += 1
            inDegree[ b ] += 1

        judge = None
        for i in range( 1, n + 1 ):
            if outDegree[ i ] == 0 and inDegree[ i ] == n - 1:
                if judge is not None:
                    return -1
                judge = i
        return judge if judge is not None else -1

class TownJudgeTest( unittest.TestCase ):
    def test_TownJudge( self ):
        n = 2
        trust = [[1,2]]
        self.assertEqual( TownJudge().findJudge( n, trust ), 2 )

        n = 3
        trust = [[1,3],[2,3]]
        self.assertEqual( TownJudge().findJudge( n, trust ), 3 )

        n = 3
        trust = [[1,3],[2,3],[3,1]]
        self.assertEqual( TownJudge().findJudge( n, trust ), -1 )

'''
Satisfiability of Equality Equations

You are given an array of strings equations that represent relationships between variables where each string equations[i] is of length 4 and takes one of two different forms: "xi==yi" or "xi!=yi".Here, xi and yi are lowercase letters (not necessarily different) that represent one-letter variable names.

Return true if it is possible to assign integers to variable names so as to satisfy all the given equations, or false otherwise.
'''

class EqualityEquations:
    def equationsPossible(self, equations: List[str]) -> bool:
        equalComponents = UnionFind()
        unequalEquationsList = list()

        for equationString in equations:
            x, token, _, y = equationString
            equalComponents.add( x )
            equalComponents.add( y )
            if token == '=':
                equalComponents.merge( x, y )
            else:
                unequalEquationsList.append( (x, y) )

        for x, y in unequalEquationsList:
            if equalComponents.inSameComponent( x, y ):
                return False
        return True

class EqualityEquationsTest( unittest.TestCase ):
    def test_EqualityEquations( self ):
        equations = ["a==b","b!=a"]
        self.assertEqual( EqualityEquations().equationsPossible( equations ), False )

        equations = ["b==a","a==b"]
        self.assertEqual( EqualityEquations().equationsPossible( equations ), True )

'''
Equal Tree Partition

Given the root of a binary tree, return true if you can partition the tree into two trees with equal sums of values after removing exactly one edge on the original tree.
'''

class EqualTreePartition:
    def checkEqualTree(self, root: Optional[TreeNode]) -> bool:
        totalSum = treeSum( root )
        if totalSum % 2 != 0:
            return False
        
        equalTree = False
        def _partition( root ):
            nonlocal equalTree
            if root is None:
                return 0
            leftSubtree = _partition( root.left )
            rightSubtree = _partition( root.right )
            if leftSubtree == totalSum // 2 and root.left is not None:
                equalTree = True
            if rightSubtree == totalSum // 2 and root.right is not None:
                equalTree = True
            return leftSubtree + rightSubtree + root.val
        _partition( root )
        return equalTree

class EqualTreePartitionTest( unittest.TestCase ):
    def test_EqualTreePartition( self ):
        treeData = [5,10,10,None,None,2,3]
        root = buildTree( treeData )
        self.assertEqual( EqualTreePartition().checkEqualTree( root ), True )

        treeData = [1,2,10,None,None,2,20]
        root = buildTree( treeData )
        self.assertEqual( EqualTreePartition().checkEqualTree( root ), False )

'''
Number of Nodes in the Sub-Tree With the Same Label

You are given a tree (i.e. a connected, undirected graph that has no cycles) consisting of n nodes numbered from 0 to n - 1 and exactly n - 1 edges. The root of the tree is the node 0, and each node of the tree has a label which is a lower-case character given in the string labels (i.e. The node with the number i has the label labels[i]).

The edges array is given on the form edges[i] = [ai, bi], which means there is an edge between nodes ai and bi in the tree.

Return an array of size n where ans[i] is the number of nodes in the subtree of the ith node which have the same label as node i.

A subtree of a tree T is the tree consisting of a node in T and all of its descendant nodes.
'''

class GenericTreeNode:
    def __init__( self, val=0 ):
        self.val = 0
        self.children = list()

    def add( self, genericTreeNode ):
        assert isinstance( genericTreeNode, GenericTreeNode )
        self.children.append( genericTreeNode )

class SubTreeWithSameLabel:
    def countSubTrees(self, n: int, edges: List[List[int]], labels: str) -> List[int]:
        treeNodeList = [ GenericTreeNode() for _ in range( n ) ]
        root, * _ = treeNodeList
        
        for index, treeNode in enumerate( treeNodeList ):
            characterIndex = ord( labels[ index ] ) - ord( 'a' )
            treeNodeList[ index ].val = index, characterIndex
        
        withParent = set()
        withParent.add( 0 ) # 0 is the root of the tree.
        for a, b in edges:
            if a in withParent:
                treeNodeList[ a ].add( treeNodeList[ b ] )
                withParent.add( b )
            else:
                treeNodeList[ b ].add( treeNodeList[ a ] )
                withParent.add( a )

        sameLabelCount = [ 0 for _ in range( n ) ]
        def _fillLables( root ):
            subtreeCountList = [ 0 for _ in range( len( string.ascii_lowercase ) ) ]
            if root is None:
                subtreeCountList
            for childNode in root.children:
                for index, count in enumerate( _fillLables( childNode ) ):
                    subtreeCountList[ index ] += count
            
            index, characterIndex = root.val
            subtreeCountList[ characterIndex ] += 1
            
            sameLabelCount[ index ] = subtreeCountList[ characterIndex ]
            return subtreeCountList
        
        _fillLables( root )
        return sameLabelCount

class SubTreeWithSameLabelTest( unittest.TestCase ):
    def test_SubTreeWithSameLabel( self ):
        n = 7
        edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]]
        labels = "abaedcd"
        self.assertEqual( SubTreeWithSameLabel().countSubTrees( n, edges, labels ), [2,1,1,1,1,1,1] )

        n = 4
        edges = [[0,1],[1,2],[0,3]]
        labels = "bbbb"
        self.assertEqual( SubTreeWithSameLabel().countSubTrees( n, edges, labels ), [4,2,1,1] )

        n = 5
        edges = [[0,1],[0,2],[1,3],[0,4]]
        labels = "aabab"
        self.assertEqual( SubTreeWithSameLabel().countSubTrees( n, edges, labels ), [3,2,1,1,1] )

'''
Coloring A Border

You are given an m x n integer matrix grid, and three integers row, col, and color. Each value in the grid represents the color of the grid square at that location.

Two squares belong to the same connected component if they have the same color and are next to each other in any of the 4 directions.

The border of a connected component is all the squares in the connected component that are either 4-directionally adjacent to a square not in the component, or on the boundary of the grid (the first or last row or column).

You should color the border of the connected component that contains the square grid[row][col] with color.

Return the final grid.
'''

class ColoringBorder:
    def colorBorder(self, grid: List[List[int]], row: int, col: int, color: int) -> List[List[int]]:
        rows, cols = len( grid ), len( grid[ 0 ] )
        visited = [ [ False for _ in range( cols ) ] for _ in range( rows ) ]

        oldColor = grid[ row ][ col ]
        q = deque()
        q.append( (row, col) )
        visited[ row ][ col ] = True

        borderCells = list()

        while len( q ) > 0:
            u, v = q.popleft()
            onBorder = False
            for du, dv in [ (0, 1), (0, -1), (1, 0), (-1, 0) ]:
                x, y = u + du, v + dv
                if not 0 <= x < rows or not 0 <= y < cols or grid[ x ][ y ] != oldColor:
                    onBorder = True
                    continue
                if not visited[ x ][ y ]:
                    q.append( (x, y) )
                    visited[ x ][ y ] = True
            if onBorder:
                borderCells.append( (u, v) )
        
        for u, v in borderCells:
            grid[ u ][ v ] = color
        return grid

class ColoringBorderTest( unittest.TestCase ):
    def test_ColoringBorder( self ):
        grid = [[1,1],[1,2]]
        row, col, color = 0, 0, 3
        self.assertEqual( ColoringBorder().colorBorder( grid, row, col, color ), [[3,3],[3,2]] )

        grid = [[1,2,2],[2,3,2]]
        row, col, color = 0, 1, 3
        self.assertEqual( ColoringBorder().colorBorder( grid, row, col, color ), [[1,3,3],[2,3,3]] )

        grid = [[1,1,1],[1,1,1],[1,1,1]]
        row, col, color = 1, 1, 2
        self.assertEqual( ColoringBorder().colorBorder( grid, row, col, color ), [[2,2,2],[2,1,2],[2,2,2]] )

'''
Find Eventual Safe States

There is a directed graph of n nodes with each node labeled from 0 to n - 1. The graph is represented by a 0-indexed 2D integer array graph where graph[i] is an integer array of nodes adjacent to node i, meaning there is an edge from node i to each node in graph[i].

A node is a terminal node if there are no outgoing edges. A node is a safe node if every possible path starting from that node leads to a terminal node.

Return an array containing all the safe nodes of the graph. The answer should be sorted in ascending order.
'''

class EventualSafeState:
    def eventualSafeNodes(self, graph: List[List[int]]) -> List[int]:
        N = len( graph )
        STATE_NEW, STATE_VISITING, STATE_VISITED = 0, 1, 2
        state = [ STATE_NEW for _ in range( N ) ]
        safeNodes = set( range( N ) )

        def _dfs( nodeToExplore ):
            stack = list()
            stack.append( nodeToExplore )

            while len( stack ) > 0:
                currentNode = stack.pop()
                if state[ currentNode ] == STATE_NEW:
                    state[ currentNode ] = STATE_VISITING
                    stack.append( currentNode )
                    for adjacentNode in graph[ currentNode ]:
                        if ( state[ adjacentNode ] == STATE_VISITED and adjacentNode not in safeNodes ) or \
                             state[ adjacentNode ] == STATE_VISITING:
                            for node in stack:
                                if state[ node ] == STATE_VISITING:
                                    safeNodes.discard( node )
                        elif state[ adjacentNode ] == STATE_NEW:
                            stack.append( adjacentNode )
                elif state[ currentNode ] == STATE_VISITING:
                    state[ currentNode ] = STATE_VISITED
        for nodeToExplore in range( N ):
            _dfs( nodeToExplore )
        return sorted( safeNodes )

class EventualSafeStateTest( unittest.TestCase ):
    def test_EventualSafeState( self ):
        graph = [[1,2],[2,3],[5],[0],[5],[],[]]
        self.assertEqual( EventualSafeState().eventualSafeNodes( graph ), [2,4,5,6] )

        graph = [[1,2,3,4],[1,2],[3,4],[0,4],[]]
        self.assertEqual( EventualSafeState().eventualSafeNodes( graph ), [4] )

'''
Largest Color Value in a Directed Graph

There is a directed graph of n colored nodes and m edges. The nodes are numbered from 0 to n - 1.

You are given a string colors where colors[i] is a lowercase English letter representing the color of the ith node in this graph (0-indexed). You are also given a 2D array edges where edges[j] = [aj, bj] indicates that there is a directed edge from node aj to node bj.

A valid path in the graph is a sequence of nodes x1 -> x2 -> x3 -> ... -> xk such that there is a directed edge from xi to xi+1 for every 1 <= i < k. The color value of the path is the number of nodes that are colored the most frequently occurring color along that path.

Return the largest color value of any valid path in the given graph, or -1 if the graph contains a cycle.
'''

class LargestColorValue:
    def largestPathValue(self, colors: str, edges: List[List[int]]) -> int:
        N = len( colors )
        graph = [ list() for _ in range( N ) ]
        for u, v in edges:
            graph[ u ].append( v )

        STATE_NEW, STATE_VISITING, STATE_VISITED = 0, 1, 2
        state = [ STATE_NEW for _ in range( N ) ]
        colorDictList = [ defaultdict( int ) for _ in range( N ) ]
        largestColorValue = - float( 'inf' )

        def _dfs( nodeToExplore ):
            nonlocal largestColorValue
            stack = list()
            stack.append( nodeToExplore )

            while len( stack ) > 0:
                currentNode = stack.pop()
                if state[ currentNode ] == STATE_NEW:
                    state[ currentNode ] = STATE_VISITING
                    stack.append( currentNode )
                    for adjacentNode in graph[ currentNode ]:
                        if state[ adjacentNode ] == STATE_VISITING:
                            return False
                        elif state[ adjacentNode ] == STATE_NEW:
                            stack.append( adjacentNode )
                elif state[ currentNode ] == STATE_VISITING:
                    state[ currentNode ] = STATE_VISITED
                    for adjacentNode in graph[ currentNode ]:
                        for color, count in colorDictList[ adjacentNode ].items():
                            colorDictList[ currentNode ][ color ] = max( colorDictList[ currentNode ][ color ], count )
                    colorDictList[ currentNode ][ colors[ currentNode ] ] += 1
                    largestColorValue = max( largestColorValue, max( colorDictList[ currentNode ].values() ) )
            return True

        for nodeToExplore in range( N ):
            if not _dfs( nodeToExplore ):
                return -1
        return largestColorValue

class LargestColorValueTest( unittest.TestCase ):
    def test_LargestColorValue( self ):
        colors = "abaca"
        edges = [[0,1],[0,2],[2,3],[3,4]]
        self.assertEqual( LargestColorValue().largestPathValue( colors, edges ), 3 )

        colors = "a"
        edges = [[0,0]]
        self.assertEqual( LargestColorValue().largestPathValue( colors, edges ), -1 )

'''
Simplify Path

Given a string path, which is an absolute path (starting with a slash '/') to a file or directory in a Unix-style file system, convert it to the simplified canonical path.

In a Unix-style file system, a period '.' refers to the current directory, a double period '..' refers to the directory up a level, and any multiple consecutive slashes (i.e. '//') are treated as a single slash '/'. For this problem, any other format of periods such as '...' are treated as file/directory names.

The canonical path should have the following format:

The path starts with a single slash '/'.
Any two directories are separated by a single slash '/'.
The path does not end with a trailing '/'.
The path only contains the directories on the path from the root directory to the target file or directory (i.e., no period '.' or double period '..')
Return the simplified canonical path.
'''

class SimplifyPath:
    def simplifyPath(self, path: str) -> str:
        currentDirectory = '.'
        previousDirectory = '..'
        
        directoryList = list()
        def _process( directoryName ):
            if directoryName == '.':
                pass
            elif directoryName == '..' and len( directoryList ) > 0:
                directoryList.pop()
            elif directoryName == '..':
                pass
            elif len( directoryName ) > 0:
                directoryList.append( directoryName )

        i = 0
        for j in range( len( path ) ):
            if path[ j ] == '/':
                _process( path[ i : j ] )
                i = j + 1
        _process( path[ i : len( path ) ] )
        return '/' + '/'.join( directoryList )

class SimplifyPathTest( unittest.TestCase ):
    def test_SimplifyPath( self ):
        path = "/home/"
        self.assertEqual( SimplifyPath().simplifyPath( path ), "/home" )

        path = "/../"
        self.assertEqual( SimplifyPath().simplifyPath( path ), "/" )

        path = "/home//foo/"
        self.assertEqual( SimplifyPath().simplifyPath( path ), "/home/foo" )

'''
Vertical Order Traversal of a Binary Tree

Given the root of a binary tree, calculate the vertical order traversal of the binary tree.

For each node at position (row, col), its left and right children will be at positions (row + 1, col - 1) and (row + 1, col + 1) respectively. The root of the tree is at (0, 0).

The vertical order traversal of a binary tree is a list of top-to-bottom orderings for each column index starting from the leftmost column and ending on the rightmost column. There may be multiple nodes in the same row and same column. In such a case, sort these nodes by their values.

Return the vertical order traversal of the binary tree.
'''

class VerticalOrderTraversal:
    def verticalTraversal(self, root: Optional[TreeNode]) -> List[List[int]]:
        traversalList = list()
        leftIndex, rightIndex = 0, 0
        
        def _traverse( root, depth, position, update=False ):
            nonlocal leftIndex
            nonlocal rightIndex
            if root is None:
                return
            if update:
                traversalList[ position - leftIndex ].append( (depth, root.val) )
            leftIndex = min( leftIndex, position )
            rightIndex = max( rightIndex, position )
            _traverse( root.left, depth + 1, position - 1, update )
            _traverse( root.right, depth + 1, position + 1, update )
        _traverse( root, 0, 0 )
        width = rightIndex - leftIndex + 1
        for _ in range( width ):
            traversalList.append( list() )
        _traverse( root, 0, 0, update=True )

        verticalTraversalList = list()
        for verticalList in traversalList:
            verticalTraversalList.append( [ item for _, item in sorted( verticalList ) ] )
        return verticalTraversalList

class VerticalOrderTraversalTest( unittest.TestCase ):
    def test_VerticalOrderTraversal( self ):
        treeData = [3,9,20,None,None,15,7]
        root = buildTree( treeData )
        self.assertEqual( VerticalOrderTraversal().verticalTraversal( root ), [[9],[3,15],[20],[7]] )

        treeData = [1,2,3,4,5,6,7]
        root = buildTree( treeData )
        self.assertEqual( VerticalOrderTraversal().verticalTraversal( root ), [[4],[2],[1,5,6],[3],[7]] )

        treeData = [1,2,3,4,6,5,7]
        root = buildTree( treeData )
        self.assertEqual( VerticalOrderTraversal().verticalTraversal( root ), [[4],[2],[1,5,6],[3],[7]] )

'''
Tree Diameter

The diameter of a tree is the number of edges in the longest path in that tree.

There is an undirected tree of n nodes labeled from 0 to n - 1. You are given a 2D array edges where edges.length == n - 1 and edges[i] = [ai, bi] indicates that there is an undirected edge between nodes ai and bi in the tree.

Return the diameter of the tree.
'''

class TreeDiameter:
    def treeDiameter(self, edges: List[List[int]]) -> int:
        n = len( edges )
        tree = [ list() for _ in range( n + 1 ) ]
        for u, v in edges:
            tree[ u ].append( v )
            tree[ v ].append( u )
        
        diameter = 0
        def _dfs( vertex, parentVertex ):
            nonlocal diameter
            depthList = list()
            for adjacentVertex in tree[ vertex ]:
                if adjacentVertex == parentVertex:
                    continue
                depthList.append( _dfs( adjacentVertex, vertex ) )
            if len( depthList ) == 0:
                return 0
            elif len( depthList ) == 1:
                depth = depthList.pop() + 1
                diameter = max( diameter, depth )
                return depth
            d1, d2, * _ = sorted( depthList, reverse=True )
            diameter = max( diameter, d1 + d2 + 2 )
            return max( d1, d2 ) + 1
        _dfs( 0, None )
        return diameter

class TreeDiameterTest( unittest.TestCase ):
    def test_TreeDiameter( self ):
        edges = [[0,1],[0,2]]
        self.assertEqual( TreeDiameter().treeDiameter( edges ), 2 )

        edges = [[0,1],[1,2],[2,3],[1,4],[4,5]]
        self.assertEqual( TreeDiameter().treeDiameter( edges ), 4 )

'''
Candy Crush

This question is about implementing a basic elimination algorithm for Candy Crush.

Given an m x n integer array board representing the grid of candy where board[i][j] represents the type of candy. A value of board[i][j] == 0 represents that the cell is empty.

The given board represents the state of the game following the player's move. Now, you need to restore the board to a stable state by crushing candies according to the following rules:

If three or more candies of the same type are adjacent vertically or horizontally, crush them all at the same time - these positions become empty.
After crushing all candies simultaneously, if an empty space on the board has candies on top of itself, then these candies will drop until they hit a candy or bottom at the same time. No new candies will drop outside the top boundary.
After the above steps, there may exist more candies that can be crushed. If so, you need to repeat the above steps.
If there does not exist more candies that can be crushed (i.e., the board is stable), then return the current board.
You need to perform the above rules until the board becomes stable, then return the stable board.
'''

class CandyCrush:
    def candyCrush(self, board: List[List[int]]) -> List[List[int]]:
        rows, cols = len( board ), len( board[ 0 ] )
        consecutiveCandies = 3
        crushPositions = list()
        
        def horizontalScan():
            for row in range( rows ):
                v1 = v2 = 0
                while True:
                    if v1 >= cols:
                        break
                    while v2 < cols and board[ row ][ v2 ] == board[ row ][ v1 ]:
                        v2 += 1
                    if v2 - v1 >= consecutiveCandies and board[ row ][ v1 ] != 0:
                        for v in range( v1, v2 ):
                            crushPositions.append( (row, v) )
                    v1 = v2
        def verticalScan():
            for col in range( cols ):
                u1 = u2 = 0
                while True:
                    if u1 >= rows:
                        break
                    while u2 < rows and board[ u2 ][ col ] == board[ u1 ][ col ]:
                        u2 += 1
                    if u2 - u1 >= consecutiveCandies and board[ u1 ][ col ] != 0:
                        for u in range( u1, u2 ):
                            crushPositions.append( (u, col) )
                    u1 = u2
        def candyCrush():
            for col in range( cols ):
                r = w = rows - 1
                while r >= 0 and board[ r ][ col ] != 0:
                    r = w = r - 1
                while r >= 0:
                    if board[ r ][ col ] != 0:
                        board[ w ][ col ], board[ r ][ col ] = board[ r ][ col ], 0
                        w = w - 1
                    r = r - 1

        while True:
            crushPositions.clear()
            horizontalScan()
            verticalScan()
            if len( crushPositions ) == 0:
                break
            for u, v in crushPositions:
                board[ u ][ v ] = 0
            candyCrush()
        return board

class CandyCrushTest( unittest.TestCase ):
    def test_CandyCrush( self ):
        board = [[110,5,112,113,114],[210,211,5,213,214],[310,311,3,313,314],[410,411,412,5,414],[5,1,512,3,3],[610,4,1,613,614],[710,1,2,713,714],[810,1,2,1,1],[1,1,2,2,2],[4,1,4,4,1014]]
        outputBoard = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[110,0,0,0,114],[210,0,0,0,214],[310,0,0,113,314],[410,0,0,213,414],[610,211,112,313,614],[710,311,412,613,714],[810,411,512,713,1014]]
        self.assertEqual( CandyCrush().candyCrush( board ), outputBoard )

        board = [[1,3,5,5,2],[3,4,3,3,1],[3,2,4,5,2],[2,4,4,5,5],[1,4,4,1,1]]
        outputBoard = [[1,3,0,0,0],[3,4,0,5,2],[3,2,0,3,1],[2,4,0,5,2],[1,4,3,1,1]]
        self.assertEqual( CandyCrush().candyCrush( board ), outputBoard )

'''
Stickers to Spell Word

We are given n different types of stickers. Each sticker has a lowercase English word on it.

You would like to spell out the given string target by cutting individual letters from your collection of stickers and rearranging them. You can use each sticker more than once if you want, and you have infinite quantities of each sticker.

Return the minimum number of stickers that you need to spell out target. If the task is impossible, return -1.

Note: In all test cases, all words were chosen randomly from the 1000 most common US English words, and target was chosen as a concatenation of two random words.
'''

class StickersToSpellWord:
    def minStickers(self, stickers: List[str], target: str) -> int:
        pass

class StickersToSpellWordTest( unittest.TestCase ):
    def test_StickersToSpellWord( self ):
        stickers = ["with","example","science"]
        target = "thehat"
        self.assertEqual( StickersToSpellWord().minStickers( stickers, target ), 3 )

        stickers = ["notice","possible"]
        target = "basicbasic"
        self.assertEqual( StickersToSpellWord().minStickers( stickers, target ), -1 )

'''
Maximum Path Quality of a Graph

There is an undirected graph with n nodes numbered from 0 to n - 1 (inclusive). You are given a 0-indexed integer array values where values[i] is the value of the ith node. You are also given a 0-indexed 2D integer array edges, where each edges[j] = [uj, vj, timej] indicates that there is an undirected edge between the nodes uj and vj, and it takes timej seconds to travel between the two nodes. Finally, you are given an integer maxTime.

A valid path in the graph is any path that starts at node 0, ends at node 0, and takes at most maxTime seconds to complete. You may visit the same node multiple times. The quality of a valid path is the sum of the values of the unique nodes visited in the path (each node's value is added at most once to the sum).

Return the maximum quality of a valid path.

Note: There are at most four edges connected to each node.
'''

class MaximumPathQuality:
    def maximalPathQuality(self, values: List[int], edges: List[List[int]], maxTime: int) -> int:
        pass

class MaximumPathQualityTest( unittest.TestCase ):
    def test_MaximumPathQuality( self ):
        values = [0,32,10,43]
        edges = [[0,1,10],[1,2,15],[0,3,10]]
        maxTime = 49
        self.assertEqual( MaximumPathQuality().maximalPathQuality( values, edges, maxTime ), 75 )

        values = [5,10,15,20]
        edges = [[0,1,10],[1,2,10],[0,3,10]]
        maxTime = 30
        self.assertEqual( MaximumPathQuality().maximalPathQuality( values, edges, maxTime ), 25 )

        values = [1,2,3,4]
        edges = [[0,1,10],[1,2,11],[2,3,12],[1,3,13]]
        maxTime = 50
        self.assertEqual( MaximumPathQuality().maximalPathQuality( values, edges, maxTime ), 7 )

'''
Flip Game II

You are playing a Flip Game with your friend.

You are given a string currentState that contains only '+' and '-'. You and your friend take turns to flip two consecutive "++" into "--". The game ends when a person can no longer make a move, and therefore the other person will be the winner.

Return true if the starting player can guarantee a win, and false otherwise.
'''

class FlipGameII:
    def canWin(self, currentState: str) -> bool:
        cache = dict()
        def _canWin( state ):
            if state in cache:
                return cache[ state ]
            isWinGuaranteed = False
            for i in range( len( state ) - 1 ):
                if state[ i : i + 2 ] == '++':
                    newState = state[ : i ] + '--' + state[ i + 2 : ]
                    if not _canWin( newState ):
                        isWinGuaranteed = True
                        break
            cache[ state ] = isWinGuaranteed
            return isWinGuaranteed
        return _canWin( currentState )

class FlipGameIITest( unittest.TestCase ):
    def test_FlipGameII( self ):
        currentState = "++++"
        self.assertEqual( FlipGameII().canWin( currentState ), True )

        currentState = "+"
        self.assertEqual( FlipGameII().canWin( currentState ), False )

'''
Flip Game

You are playing a Flip Game with your friend.

You are given a string currentState that contains only '+' and '-'. You and your friend take turns to flip two consecutive "++" into "--". The game ends when a person can no longer make a move, and therefore the other person will be the winner.

Return all possible states of the string currentState after one valid move. You may return the answer in any order. If there is no valid move, return an empty list [].
'''

class FlipGame:
    def generatePossibleNextMoves(self, currentState: str) -> List[str]:
        possibleStateList = list()
        for i in range( len( currentState ) - 1 ):
            if currentState[ i : i + 2 ] == '++':
                possibleState = currentState[ : i ] + '--' + currentState[ i + 2 : ]
                possibleStateList.append( possibleState )
        return possibleStateList

class FlipGameTest( unittest.TestCase ):
    def test_FlipGame( self ):
        currentState = "++++"
        self.assertEqual( set( FlipGame().generatePossibleNextMoves( currentState ) ), set( ["--++","+--+","++--"] ) )

        currentState = "+"
        self.assertEqual( FlipGame().generatePossibleNextMoves( currentState ), [] )

'''
Maximum Good People Based on Statements

There are two types of persons:

The good person: The person who always tells the truth.
The bad person: The person who might tell the truth and might lie.
You are given a 0-indexed 2D integer array statements of size n x n that represents the statements made by n people about each other. More specifically, statements[i][j] could be one of the following:

0 which represents a statement made by person i that person j is a bad person.
1 which represents a statement made by person i that person j is a good person.
2 represents that no statement is made by person i about person j.
Additionally, no person ever makes a statement about themselves. Formally, we have that statements[i][i] = 2 for all 0 <= i < n.

Return the maximum number of people who can be good based on the statements made by the n people.
'''

class MaximumGood:
    def maximumGood(self, statements: List[List[int]]) -> int:
        N = len( statements )
        STATEMENT_BAD, STATEMENT_GOOD, STATEMENT_NONE = 0, 1, 2
        _maximumGood = 0
        globalAssignmentMap = dict()
        
        def _search( i ):
            nonlocal _maximumGood
            if i == N:
                _maximumGood = max( _maximumGood, list( globalAssignmentMap.values() ).count( STATEMENT_GOOD ) )
                return
            if i in globalAssignmentMap:
                return _search( i + 1 )
            
            # Assign STATEMENT_GOOD to i.
            conflictFound = False
            assignmentMap = { i : STATEMENT_GOOD }
            for j in range( N ):
                if statements[ i ][ j ] == STATEMENT_NONE:
                    continue
                if j not in globalAssignmentMap:
                    assignmentMap[ j ] = statements[ i ][ j ]
                elif statements[ i ][ j ] != globalAssignmentMap[ j ]:
                    conflictFound = True
                    break
            if not conflictFound:
                globalAssignmentMap.update( assignmentMap )
                _search( i + 1 )
                for key in assignmentMap:
                    del globalAssignmentMap[ key ]
            # Assign STATEMENT_BAD to i
            globalAssignmentMap[ i ] = STATEMENT_BAD
            _search( i + 1 )
            del globalAssignmentMap[ i ]
        
        _search( 0 )
        return _maximumGood

class MaximumGoodTest( unittest.TestCase ):
    def test_MaximumGood( self ):
        statements = [[2,1,2],[1,2,2],[2,0,2]]
        self.assertEqual( MaximumGood().maximumGood( statements ), 2 )
        
        statements = [[2,0],[0,2]]
        self.assertEqual( MaximumGood().maximumGood( statements ), 0 )

'''
Redundant Connection

In this problem, a tree is an undirected graph that is connected and has no cycles.

You are given a graph that started as a tree with n nodes labeled from 1 to n, with one additional edge added. The added edge has two different vertices chosen from 1 to n, and was not an edge that already existed. The graph is represented as an array edges of length n where edges[i] = [ai, bi] indicates that there is an edge between nodes ai and bi in the graph.

Return an edge that can be removed so that the resulting graph is a tree of n nodes. If there are multiple answers, return the answer that occurs last in the input.
'''

class RedundantConnection:
    def findRedundantConnection(self, edges):
        uf = UnionFind()
        for u, v in edges:
            uf.add( u )
            uf.add( v )
            if uf.inSameComponent( u, v ):
                return [ u, v ]
            uf.merge( u, v )

class RedundantConnectionTest( unittest.TestCase ):
    def test_RedundantConnection( self ):
        edges = [[1,2],[1,3],[2,3]]
        self.assertEqual( RedundantConnection().findRedundantConnection( edges ), [2,3] )

        edges = [[1,2],[2,3],[3,4],[1,4],[1,5]]
        self.assertEqual( RedundantConnection().findRedundantConnection( edges ), [1,4] )

'''
Redundant Connection II

In this problem, a rooted tree is a directed graph such that, there is exactly one node (the root) for which all other nodes are descendants of this node, plus every node has exactly one parent, except for the root node which has no parents.

The given input is a directed graph that started as a rooted tree with n nodes (with distinct values from 1 to n), with one additional directed edge added. The added edge has two different vertices chosen from 1 to n, and was not an edge that already existed.

The resulting graph is given as a 2D-array of edges. Each element of edges is a pair [ui, vi] that represents a directed edge connecting nodes ui and vi, where ui is a parent of child vi.

Return an edge that can be removed so that the resulting graph is a rooted tree of n nodes. If there are multiple answers, return the answer that occurs last in the given 2D-array.
'''

class RedundantConnectionII:
    def findRedundantDirectedConnection(self, edges):
        return None

class RedundantConnectionIITest( unittest.TestCase ):
    def test_RedundantConnectionII( self ):
        edges = [[1,2],[1,3],[2,3]]
        self.assertEqual( RedundantConnectionII().findRedundantDirectedConnection( edges ), [2,3] )

        edges = [[1,2],[2,3],[3,4],[4,1],[1,5]]
        self.assertEqual( RedundantConnectionII().findRedundantDirectedConnection( edges ), [4,1] )

'''
Longest Valid Parentheses

Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.
'''

class LongestValidParentheses:
    def longestValidParentheses(self, s):
        # Add an extra slot which is always 0. This ensures that even if s is empty, we pass a non-empty
        # list to the function max().
        lengthList = [ 0 for _ in range( len( s ) + 1 ) ]

        for index, character in enumerate( s ):
            if character == '(':
                continue
            if index - 1 >= 0 and s[ index - 1 ] == '(':
                lengthList[ index ] = 2 + ( lengthList[ index - 2 ] if index - 2 >= 0 else 0 )
            elif index - 1 >= 0 and s[ index - 1 ] == ')':
                i = index - lengthList[ index - 1 ] - 1
                if i >= 0 and s[ i ] == '(':
                    lengthList[ index ] = 2 + lengthList[ index - 1 ] + ( lengthList[ i - 1 ] if i - 1 >= 0 else 0 )
        return max( lengthList )

class LongestValidParenthesesTest( unittest.TestCase ):
    def test_LongestValidParentheses( self ):
        s = "(()"
        self.assertEqual( LongestValidParentheses().longestValidParentheses( s ), 2 )

        s = ")()())"
        self.assertEqual( LongestValidParentheses().longestValidParentheses( s ), 4 )

        s = ""
        self.assertEqual( LongestValidParentheses().longestValidParentheses( s ), 0 )

'''
Minimum Distance to Type a Word Using Two Fingers

ABCDEF
GHIJKL
MNOPQR
STUVWX
YZ

You have a keyboard layout as shown above in the X-Y plane, where each English uppercase letter is located at some coordinate.

For example, the letter 'A' is located at coordinate (0, 0), the letter 'B' is located at coordinate (0, 1), the letter 'P' is located at coordinate (2, 3) and the letter 'Z' is located at coordinate (4, 1).
Given the string word, return the minimum total distance to type such string using only two fingers.

The distance between coordinates (x1, y1) and (x2, y2) is |x1 - x2| + |y1 - y2|.

Note that the initial positions of your two fingers are considered free so do not count towards your total distance, also your two fingers do not have to start at the first letter or the first two letters.
'''

class MinimumDistanceTwoFingers:
    def minimumDistance(self, word):
        q = list()
        startState = -1, -1, 0
        initialCost = 0

        steps = 0
        costDict = dict()
        costDict[ startState ] = initialCost
        q.append( (initialCost, startState) )

        def _distance( position1, position2 ):
            COLUMNS = 6
            assert position1 >= 0 and position2 >= 0
            x1, y1 = position1 // COLUMNS, position1 % COLUMNS
            x2, y2 = position2 // COLUMNS, position2 % COLUMNS
            return abs( x1 - x2) + abs( y1 - y2 )

        def _letterToPosition( letter ):
            return ord( letter ) - ord( 'A' )

        while len( q ) > 0:
            cost, currentState = heapq.heappop( q )
            position1, position2, index = currentState

            if index == len( word ):
                return cost

            possibleStates = list()
            letterPosition = _letterToPosition( word[ index ] )
            
            if position1 == -1:
                possibleStates.append( (letterPosition, position2, index + 1, cost) )
            elif position2 == -1:
                possibleStates.append( (position1, letterPosition, index + 1, cost) )
                movementCost = _distance( letterPosition, position1 )
                possibleStates.append( (letterPosition, position2, index + 1, cost + movementCost) )
            else:
                movementCost = _distance( letterPosition, position1 )
                possibleStates.append( (letterPosition, position2, index + 1, cost + movementCost) )
                movementCost = _distance( letterPosition, position2 )
                possibleStates.append( (position1, letterPosition, index + 1, cost + movementCost) )
            
            for newPosition1, newPosition2, newIndex, newCost in possibleStates:
                newState = newPosition1, newPosition2, newIndex
                if newState not in costDict or newCost < costDict[ newState ]:
                    costDict[ newState ] = newCost
                    heapq.heappush( q, (newCost, newState) )

class MinimumDistanceTwoFingersTest( unittest.TestCase ):
    def test_MinimumDistanceTwoFingers( self ):
        self.assertEqual( MinimumDistanceTwoFingers().minimumDistance( "CAKE" ), 3 )
        self.assertEqual( MinimumDistanceTwoFingers().minimumDistance( "HAPPY" ), 6 )

'''
Minimum Falling Path Sum

Given an n x n array of integers matrix, return the minimum sum of any falling path through matrix.

A falling path starts at any element in the first row and chooses the element in the next row that is either directly below or diagonally left/right. Specifically, the next element from position (row, col) will be (row + 1, col - 1), (row + 1, col), or (row + 1, col + 1).
'''

class MinimumFallingPathSum:
    def minFallingPathSum(self, matrix):
        rows, cols = len( matrix ), len( matrix[ 0 ] )
        for u in range( 1, rows ):
            for v in range( cols ):
                bestCost = float( 'inf' )
                for dv in [ -1, 0, 1 ]:
                    c = v + dv
                    if 0 <= c < cols:
                        bestCost = min( bestCost, matrix[ u ][ v ] + matrix[ u - 1 ][ c ] )
                matrix[ u ][ v ] = bestCost
        return min( matrix[ -1 ] )

class MinimumFallingPathSumTest( unittest.TestCase ):
    def test_MinimumFallingPathSum( self ):
        matrix = [[2,1,3],[6,5,4],[7,8,9]]
        self.assertEqual( MinimumFallingPathSum().minFallingPathSum( matrix ), 13 )

        matrix = [[-19,57],[-40,-5]]
        self.assertEqual( MinimumFallingPathSum().minFallingPathSum( matrix ), -59 )

'''
Minimum Falling Path Sum II

Given an n x n integer matrix grid, return the minimum sum of a falling path with non-zero shifts.

A falling path with non-zero shifts is a choice of exactly one element from each row of grid such that no two elements chosen in adjacent rows are in the same column.
'''

class MinimumFallingPathSumII:
    def minFallingPathSum(self, grid):
        rows, cols = len( grid ), len( grid[ 0 ] )
        for u in range( 1, rows ):
            for v in range( cols ):
                bestCost = float( 'inf' )
                for c in range( cols ):
                    if c == v:
                        continue
                    bestCost = min( bestCost, grid[ u ][ v ] + grid[ u - 1 ][ c ] )
                grid[ u ][ v ] = bestCost
        return min( grid[ -1 ] )

class MinimumFallingPathSumIITest( unittest.TestCase ):
    def test_MinimumFallingPathSumII( self ):
        grid = [[1,2,3],[4,5,6],[7,8,9]]
        self.assertEqual( MinimumFallingPathSumII().minFallingPathSum( grid ), 13 )

        grid = [[7]]
        self.assertEqual( MinimumFallingPathSumII().minFallingPathSum( grid ), 7 )

        grid = [[-73,61,43,-48,-36],[3,30,27,57,10],[96,-76,84,59,-15],[5,-49,76,31,-7],[97,91,61,-46,67]]
        self.assertEqual( MinimumFallingPathSumII().minFallingPathSum( grid ), -192 )

'''
Maximum Employees to Be Invited to a Meeting

A company is organizing a meeting and has a list of n employees, waiting to be invited. They have arranged for a large circular table, capable of seating any number of employees.

The employees are numbered from 0 to n - 1. Each employee has a favorite person and they will attend the meeting only if they can sit next to their favorite person at the table. The favorite person of an employee is not themself.

Given a 0-indexed integer array favorite, where favorite[i] denotes the favorite person of the ith employee, return the maximum number of employees that can be invited to the meeting.
'''

class MaximumEmployees:
    def maximumInvitations(self, favorite):
        pass

class MaximumEmployeesTest( unittest.TestCase ):
    def test_MaximumEmployees( self ):
        favorite = [2,2,1,2]
        self.assertEqual( MaximumEmployees().maximumInvitations( favorite ), 3 )

        favorite = [1,2,0]
        self.assertEqual( MaximumEmployees().maximumInvitations( favorite ), 3 )

        favorite = [3,0,1,4,1]
        self.assertEqual( MaximumEmployees().maximumInvitations( favorite ), 4 )

'''
Sum of Distances in Tree

There is an undirected connected tree with n nodes labeled from 0 to n - 1 and n - 1 edges.

You are given the integer n and the array edges where edges[i] = [ai, bi] indicates that there is an edge between nodes ai and bi in the tree.

Return an array answer of length n where answer[i] is the sum of the distances between the ith node in the tree and all other nodes.
'''

class SumOfDistancesInTree:
    def sumOfDistancesInTree(self, n, edges):
        tree = [ list() for _ in range( n ) ]
        for u, v in edges:
            tree[ u ].append( v )
            tree[ v ].append( u )

        parentList = [ None for _ in range( n ) ]
        nodeCount = [ 0 for _ in range( n ) ]
        distances = [ 0 for _ in range( n ) ]
        
        def _dfs( u, parentNode ):
            parentList[ u ] = parentNode
            totalCount = 1
            for v in tree[ u ]:
                if v == parentNode:
                    continue
                _dfs( v, u )
                distances[ u ] += distances[ v ] + nodeCount[ v ]
                totalCount += nodeCount[ v ]
            nodeCount[ u ] = totalCount
        _dfs( 0, None )

        additionalDistances = [ 0 for _ in range( n ) ]
        for u in range( n ):
            if parentList[ u ] is not None:
                additionalDistances[ u ] += distances[ parentList[ u ] ] + ( n - nodeCount[ u ] ) - 1
        return [ distances[ i ] + additionalDistances[ i ] for i in range( n ) ]

class SumOfDistancesInTreeTest( unittest.TestCase ):
    def test_SumOfDistancesInTree( self ):
        n = 6
        edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
        self.assertEqual( SumOfDistancesInTree().sumOfDistancesInTree( n, edges ), [8,12,6,10,10,10] )

        n = 1
        edges = []
        self.assertEqual( SumOfDistancesInTree().sumOfDistancesInTree( n, edges ), [0] )

        n = 2
        edges = [[1,0]]
        self.assertEqual( SumOfDistancesInTree().sumOfDistancesInTree( n, edges ), [1,1] )

'''
Remove Max Number of Edges to Keep Graph Fully Traversable

Alice and Bob have an undirected graph of n nodes and 3 types of edges:

Type 1: Can be traversed by Alice only.
Type 2: Can be traversed by Bob only.
Type 3: Can by traversed by both Alice and Bob.
Given an array edges where edges[i] = [typei, ui, vi] represents a bidirectional edge of type typei between nodes ui and vi, find the maximum number of edges you can remove so that after removing the edges, the graph can still be fully traversed by both Alice and Bob. The graph is fully traversed by Alice and Bob if starting from any node, they can reach all other nodes.

Return the maximum number of edges you can remove, or return -1 if it's impossible for the graph to be fully traversed by Alice and Bob.
'''

class RemoveEdges:
    def maxNumEdgesToRemove(self, n, edges):
        EDGE_ALICE, EDGE_BOB, EDGE_BOTH = 1, 2, 3

        edgeTypes = defaultdict( list )

        for graphType, u, v in edges:
            edgeTypes[ graphType ].append( (u, v) )

        ufAlice = UnionFind()
        ufBob = UnionFind()
        for i in range( 1, n + 1 ):
            ufAlice.add( i )
            ufBob.add( i )

        edgesAdded = 0
        for u, v in edgeTypes[ EDGE_BOTH ]:
            if ufAlice.merge( u, v ):
                edgesAdded += 1
                ufBob.merge( u, v )

        for u, v in edgeTypes[ EDGE_ALICE ]:
            if ufAlice.merge( u, v ):
                edgesAdded += 1
        
        for u, v in edgeTypes[ EDGE_BOB ]:
            if ufBob.merge( u, v ):
                edgesAdded += 1

        if ufAlice.disjointSetCount() == ufBob.disjointSetCount() == 1:
            return len( edges ) - edgesAdded
        return -1

class RemoveEdgesTest( unittest.TestCase ):
    def test_RemoveEdges( self ):
        n = 4
        edges = [[3,1,2],[3,2,3],[1,1,3],[1,2,4],[1,1,2],[2,3,4]]
        self.assertEqual( RemoveEdges().maxNumEdgesToRemove( n, edges ), 2 )

        n = 4
        edges = [[3,1,2],[3,2,3],[1,1,4],[2,1,4]]
        self.assertEqual( RemoveEdges().maxNumEdgesToRemove( n, edges ), 0 )

        n = 4
        edges = [[3,2,3],[1,1,2],[2,3,4]]
        self.assertEqual( RemoveEdges().maxNumEdgesToRemove( n, edges ), -1 )

'''
Paths in Maze That Lead to Same Room

A maze consists of n rooms numbered from 1 to n, and some rooms are connected by corridors. You are given a 2D integer array corridors where corridors[i] = [room1i, room2i] indicates that there is a corridor connecting room1i and room2i, allowing a person in the maze to go from room1i to room2i and vice versa.

The designer of the maze wants to know how confusing the maze is. The confusion score of the maze is the number of different cycles of length 3.

For example, 1 → 2 → 3 → 1 is a cycle of length 3, but 1 → 2 → 3 → 4 and 1 → 2 → 3 → 2 → 1 are not.
Two cycles are considered to be different if one or more of the rooms visited in the first cycle is not in the second cycle.

Return the confusion score of the maze.
'''

class PathsInMaze:
    def numberOfPaths(self, n, corridors):
        graph = [ list() for _ in range( n ) ]
        for u, v in corridors:
            u, v = u - 1, v - 1
            graph[ u ].append( v )
            graph[ v ].append( u )

        pathCount = 0

class PathsInMazeTest( unittest.TestCase ):
    def test_PathsInMaze( self ):
        n = 5
        corridors = [[1,2],[5,2],[4,1],[2,4],[3,1],[3,4]]
        self.assertEqual( PathsInMaze().numberOfPaths( n, corridors ), 2 )

        n = 4
        corridors = [[1,2],[3,4]]
        self.assertEqual( PathsInMaze().numberOfPaths( n, corridors ), 0 )

'''
House Robber III

The thief has found himself a new place for his thievery again. There is only one entrance to this area, called root.

Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that all houses in this place form a binary tree. It will automatically contact the police if two directly-linked houses were broken into on the same night.

Given the root of the binary tree, return the maximum amount of money the thief can rob without alerting the police.
'''

class HouseRobberIII:
    def rob(self, root):
        def _rob( root ):
            withRoot, withoutRoot = 0, 0
            
            if root is not None:
                withRootLeft, withoutRootLeft = _rob( root.left )
                withRootRight, withoutRootRight = _rob( root.right )
                withRoot = root.val + withoutRootLeft + withoutRootRight
                withoutRoot = max( withRootLeft, withoutRootLeft ) + max( withRootRight, withoutRootRight )
            return withRoot, withoutRoot
        return max( _rob( root ) )

class HouseRobberIIITest( unittest.TestCase ):
    def test_HouseRobberIII( self ):
        treeData = [3,2,3,None,3,None,1]
        root = buildTree( treeData )
        self.assertEqual( HouseRobberIII().rob( root ), 7 )

        treeData = [3,4,5,1,3,None,1]
        root = buildTree( treeData )
        self.assertEqual( HouseRobberIII().rob( root ), 9 )

'''
Binary Tree Maximum Path Sum

A path in a binary tree is a sequence of nodes where each pair of adjacent nodes in the sequence has an edge connecting them. A node can only appear in the sequence at most once. Note that the path does not need to pass through the root.

The path sum of a path is the sum of the node's values in the path.

Given the root of a binary tree, return the maximum path sum of any non-empty path.
'''

class BinaryTreeMaximumPathSum:
    def maxPathSum(self, root):
        maximumPathSum = - float( 'inf' )
        def _dfs( root ):
            nonlocal maximumPathSum
            total = root.val
            leftTotal = rightTotal = - float( 'inf' )
            if root.left:
                leftTotal = _dfs( root.left )
            if root.right:
                rightTotal = _dfs( root.right )
            total = max( total, total + leftTotal, total + rightTotal )
            maximumPathSum = max( maximumPathSum, total, leftTotal + rightTotal + root.val )
            return total
        _dfs( root )
        return maximumPathSum

class BinaryTreeMaximumPathSumTest( unittest.TestCase ):
    def test_BinaryTreeMaximumPathSum( self ):
        treeData = [1,2,3]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeMaximumPathSum().maxPathSum( root ), 6 )

        treeData = [-10,9,20,None,None,15,7]
        root = buildTree( treeData )
        self.assertEqual( BinaryTreeMaximumPathSum().maxPathSum( root ), 42 )

'''
Possible Bipartition

We want to split a group of n people (labeled from 1 to n) into two groups of any size. Each person may dislike some other people, and they should not go into the same group.

Given the integer n and the array dislikes where dislikes[i] = [ai, bi] indicates that the person labeled ai does not like the person labeled bi, return true if it is possible to split everyone into two groups in this way.
'''

class PossibleBipartition:
    def possibleBipartition(self, n: int, dislikes: List[List[int]]) -> bool:
        graph = [ list() for _ in range( n ) ]
        for u, v in dislikes:
            u, v = u - 1, v - 1
            graph[ u ].append( v )
            graph[ v ].append( u )

        COLOR_NONE, COLOR_BLACK, COLOR_WHITE = 0, 1, 2
        colors = [ COLOR_NONE for _ in range( n ) ]
        visited = [ False for _ in range( n ) ]

        def _oppositeColor( color ):
            assert color != COLOR_NONE
            return COLOR_WHITE if color == COLOR_BLACK else COLOR_BLACK

        def _bfs( u ):
            q = deque()
            q.append( u )
            while len( q ) > 0:
                u = q.popleft()
                if not visited[ u ]:
                    visited[ u ] = True
                    for v in graph[ u ]:
                        if colors[ v ] == colors[ u ]:
                            return False
                        colors[ v ] = _oppositeColor( colors[ u ] )
                        q.append( v )
            return True
        for vertex in range( n ):
            if not visited[ vertex ]:
                colors[ vertex ] = COLOR_WHITE
                if not _bfs( vertex ):
                    return False
        return True

class PossibleBipartitionTest( unittest.TestCase ):
    def test_PossibleBipartition( self ):
        n = 4
        dislikes = [[1,2],[1,3],[2,4]]
        self.assertEqual( PossibleBipartition().possibleBipartition( n, dislikes ), True )

        n = 3
        dislikes = [[1,2],[1,3],[2,3]]
        self.assertEqual( PossibleBipartition().possibleBipartition( n, dislikes ), False )

        n = 5
        dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]]
        self.assertEqual( PossibleBipartition().possibleBipartition( n, dislikes ), False )

'''
Course Schedule

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return true if you can finish all courses. Otherwise, return false.
'''

class CourseSchedule:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        graph = [ list() for _ in range( numCourses ) ]
        for u, v in prerequisites:
            graph[ u ].append( v )

        state = [ 'NEW' for _ in range( numCourses ) ]
        def _dfs( course ):
            if state[ course ] == 'VISITING':
                return False
            if state[ course ] == 'NEW':
                state[ course ] = 'VISITING'
                for prerequisiteCourse in graph[ course ]:
                    if not _dfs( prerequisiteCourse ):
                        return False
                state[ course ] = 'VISITED'
            return True
        for course in range( numCourses ):
            if not _dfs( course ):
                return False
        return True

class CourseScheduleTest( unittest.TestCase ):
    def test_CourseSchedule( self ):
        numCourses = 2
        prerequisites = [[1,0]]
        self.assertEqual( CourseSchedule().canFinish( numCourses, prerequisites ), True )

        numCourses = 2
        prerequisites = [[1,0],[0,1]]
        self.assertEqual( CourseSchedule().canFinish( numCourses, prerequisites ), False )

'''
Strange Printer II

There is a strange printer with the following two special requirements:

On each turn, the printer will print a solid rectangular pattern of a single color on the grid. This will cover up the existing colors in the rectangle.
Once the printer has used a color for the above operation, the same color cannot be used again.
You are given a m x n matrix targetGrid, where targetGrid[row][col] is the color in the position (row, col) of the grid.

Return true if it is possible to print the matrix targetGrid, otherwise, return false.
'''

class StrangePrinterII:
    def isPrintable(self, targetGrid: List[List[int]]) -> bool:
        graph = dict()
        rowMinimum = dict()
        rowMaximum = dict()
        columnMinimum = dict()
        columnMaximum = dict()

        rows, cols = len( targetGrid ), len( targetGrid[ 0 ] )
        for row in range( rows ):
            for col in range( cols ):
                cell = targetGrid[ row ][ col ]
                if cell not in rowMinimum:
                    rowMinimum[ cell ] = columnMinimum[ cell ] = float( 'inf' )
                    rowMaximum[ cell ] = columnMaximum[ cell ] = - float( 'inf' )
                rowMinimum[ cell ] = min( rowMinimum[ cell ], row )
                columnMinimum[ cell ] = min( columnMinimum[ cell ], col )
                rowMaximum[ cell ] = max( rowMaximum[ cell ], row )
                columnMaximum[ cell ] = max( columnMaximum[ cell ], col )

        for cell in rowMinimum.keys():
            graph[ cell ] = set()
            r1, c1 = rowMinimum[ cell ], columnMinimum[ cell ]
            r2, c2 = rowMaximum[ cell ], columnMaximum[ cell ]
            for r in range( r1, r2 + 1 ):
                for c in range( c1, c2 + 1 ):
                    if targetGrid[ r ][ c ] != cell:
                        # The bounding box for "cell" is r1, c1, r2, c2. If we find a different cell (say cell_y) in the bounding box,
                        # then cell has been erased by cell_y.
                        graph[ cell ].add( targetGrid[ r ][ c ] )

        # We now check whether there is a loop in the graph.
        visited = dict()
        for cell in graph.keys():
            visited[ cell ] = 'NEW'
        def _dfs( u ):
            if visited[ u ] == 'NEW':
                visited[ u ] = 'VISITING'
                for v in graph[ u ]:
                    if _dfs( v ):
                        return True
                visited[ u ] = 'VISITED'
            elif visited[ u ] == 'VISITING':
                return True
            return False

        for cell in graph.keys():
            if visited[ cell ] == 'NEW':
                if _dfs( cell ):
                    return False
        return True

class StrangePrinterIITest( unittest.TestCase ):
    def test_StrangePrinterII( self ):
        targetGrid = [[1,1,1,1],[1,2,2,1],[1,2,2,1],[1,1,1,1]]
        self.assertEqual( StrangePrinterII().isPrintable( targetGrid ), True )

        targetGrid = [[1,1,1,1],[1,1,3,3],[1,1,3,4],[5,5,1,4]]
        self.assertEqual( StrangePrinterII().isPrintable( targetGrid ), True )

        targetGrid = [[1,2,1],[2,1,2],[1,2,1]]
        self.assertEqual( StrangePrinterII().isPrintable( targetGrid ), False )

'''
Lonely Pixel I

Given an m x n picture consisting of black 'B' and white 'W' pixels, return the number of black lonely pixels.

A black lonely pixel is a character 'B' that located at a specific position where the same row and same column don't have any other black pixels.
'''

class LonelyPixelI:
    def findLonelyPixel(self, picture: List[List[str]]) -> int:
        rows, cols = len( picture ), len( picture[ 0 ] )
        rowInfo = [ 0 for _ in range( rows ) ]
        columnInfo = [ 0 for _ in range( cols ) ]

        for r in range( rows ):
            for c in range( cols ):
                if picture[ r ][ c ] == "B":
                    rowInfo[ r ] += 1
                    columnInfo[ c ] += 1
        count = 0
        for r in range( rows ):
            for c in range( cols ):
                if picture[ r ][ c ] == "B" and rowInfo[ r ] == columnInfo[ c ] == 1:
                    count += 1
        return count

class LonelyPixelITest( unittest.TestCase ):
    def test_LonelyPixelI( self ):
        picture = [["W","W","B"],["W","B","W"],["B","W","W"]]
        self.assertEqual( LonelyPixelI().findLonelyPixel( picture ), 3 )

        picture = [["B","B","B"],["B","B","W"],["B","B","B"]]
        self.assertEqual( LonelyPixelI().findLonelyPixel( picture ), 0 )

'''
Lonely Pixel II

Given an m x n picture consisting of black 'B' and white 'W' pixels and an integer target, return the number of black lonely pixels.

A black lonely pixel is a character 'B' that located at a specific position (r, c) where:

Row r and column c both contain exactly target black pixels.
For all rows that have a black pixel at column c, they should be exactly the same as row r.
'''

class LonelyPixelII:
    def findBlackPixel(self, picture: List[List[str]], target: int) -> int:
        rowToSignature = list()
        for pictureRow in picture:
            rowToSignature.append( ''.join( pictureRow ) )

        rows, cols = len( picture ), len( picture[ 0 ] )
        rowInfo = [ 0 for _ in range( rows ) ]
        columnInfo = [ 0 for _ in range( cols ) ]
        for row in range( rows ):
            for col in range( cols ):
                if picture[ row ][ col ] == "B":
                    rowInfo[ row ] += 1
                    columnInfo[ col ] += 1

        count = 0
        for col in range( cols ):
            accumulate = 0
            signature = None
            for row in range( rows ):
                if picture[ row ][ col ] == "W":
                    continue
                if rowInfo[ row ] == columnInfo[ col ] == target:
                    accumulate += 1
                if signature is None:
                    signature = rowToSignature[ row ]
                elif signature != rowToSignature[ row ]:
                    accumulate = 0
                    break
            count += accumulate
        return count

class LonelyPixelIITest( unittest.TestCase ):
    def test_LonelyPixelII( self ):
        picture = [["W","B","W","B","B","W"],["W","B","W","B","B","W"],["W","B","W","B","B","W"],["W","W","B","W","B","W"]]
        target = 3
        self.assertEqual( LonelyPixelII().findBlackPixel( picture, target ), 6 )

        picture = [["W","W","B"],["W","W","B"],["W","W","B"]]
        target = 1
        self.assertEqual( LonelyPixelII().findBlackPixel( picture, target ), 0 )

if __name__ == '__main__':
    unittest.main( verbosity=2 )