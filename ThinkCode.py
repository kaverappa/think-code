import unittest
import os
import pathlib
import itertools
from collections import deque
import heapq
from collections import defaultdict
import math
import operator
import string
import bisect
import copy
from fractions import Fraction
from sortedcontainers import SortedList

def getTestFileList( tag, basepath='tests' ):
	return set( [ pathlib.Path( filename ).stem for filename in os.listdir( '{}/{}'.format( basepath, tag ) ) ] )

def getTestFileSuffixList( tag, basepath='tests' ):
	return set( [ pathlib.Path( filename ).suffix for filename in os.listdir( '{}/{}'.format( basepath, tag ) ) ] )

def readString( file ):
	return file.readline().strip()

def readStrings( file ):
	return file.readline().strip().split()

def readRawString( file ):
	newline = '\n'
	return file.readline().strip( newline )

def readAllStrings( file ):
	return [ line.strip() for line in file.readlines() ]

def readAllIntegers( file ):
	return list( map( int, readAllStrings( file ) ) )

def readTokens( file ):
	return readString( file ).split()

def readInteger( file ):
	return int( readString( file ) )

def readIntegers( file ):
	return map( int, file.readline().strip().split() )

'''
Nordic Collegiate Programming Contest 2015
Problem E : Entertainment Box
Problem ID: entertainmentbox
'''

class EntertainmentBox:
	@staticmethod
	def maximumRecording( k, showTimeList ):
		totalRecordings = 0

		timeList = [ (endTime, startTime) for (startTime, endTime) in showTimeList ]
		timeList.sort()

		q = SortedList()
		
		for endTime, startTime in timeList:
			index = q.bisect( startTime ) - 1
			
			if index < 0 and len( q ) < k:
				q.add( endTime )
				totalRecordings += 1
			elif index >= 0:
				del q[ index ]
				q.add( endTime )
				totalRecordings += 1

		return totalRecordings

class EntertainmentBoxTest( unittest.TestCase ):
	def test_EntertainmentBox( self ):
		for testfile in getTestFileList( tag='entertainmentbox' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/entertainmentbox/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/entertainmentbox/{}.ans'.format( testfile ) ) as solutionFile:

			N, k = readIntegers( inputFile )
			showTimeList = [ tuple( readIntegers( inputFile ) ) for _ in range( N ) ]
			totalRecordings = readInteger( solutionFile )

			print( 'Testcase {} N = {} k = {} totalRecordings = {}'.format( testfile, N, k, totalRecordings ) )
			self.assertEqual( EntertainmentBox.maximumRecording( k, showTimeList ), totalRecordings )

	def test_EntertainmentBox_Sample( self ):
		k = 1
		showTimeList = [ (1, 2), (2, 3), (2, 3) ]
		self.assertEqual( EntertainmentBox.maximumRecording( k, showTimeList ), 2 )

		k = 1
		showTimeList = [ (1, 3), (4, 6), (7, 8), (2, 5) ]
		self.assertEqual( EntertainmentBox.maximumRecording( k, showTimeList ), 3 )

		k = 2
		showTimeList = [ (1, 4), (5, 9), (2, 7), (3, 8), (6, 10) ]
		self.assertEqual( EntertainmentBox.maximumRecording( k, showTimeList ), 3 )

'''
Nordic Collegiate Programming Contest 2015
Problem D : Disastrous Downtime
Problem ID: downtime
'''

class DisastrousDowntime:
	@staticmethod
	def minimumServers( k, requestTimeList ):
		processingTime = 1000

		allTimeStamps = list()
		for timeStamp in requestTimeList:
			allTimeStamps.append( (timeStamp, 'START') )
			allTimeStamps.append( (timeStamp + processingTime, 'END') )
		allTimeStamps.sort()

		maximumConcurrentRequests = 0
		concurrentRequests = 0
		for _, tag in allTimeStamps:
			if tag == 'START':
				concurrentRequests += 1
				maximumConcurrentRequests = max( maximumConcurrentRequests, concurrentRequests )
			elif tag == 'END':
				concurrentRequests -= 1
		minimumServersNeeded = maximumConcurrentRequests // k
		if maximumConcurrentRequests % k != 0:
			minimumServersNeeded += 1
		return minimumServersNeeded

class DisastrousDowntimeTest( unittest.TestCase ):
	def test_DisastrousDowntime( self ):
		for testfile in getTestFileList( tag='downtime' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/downtime/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/downtime/{}.ans'.format( testfile ) ) as solutionFile:

			N, k = readIntegers( inputFile )
			requestTimeList = [ readInteger( inputFile ) for _ in range( N ) ]
			minimumServersNeeded = readInteger( solutionFile )

			print( 'Testcase {} N = {} k = {} minimumServersNeeded = {}'.format( testfile, N, k, minimumServersNeeded ) )
			self.assertEqual( DisastrousDowntime.minimumServers( k, requestTimeList ), minimumServersNeeded )

	def test_DisastrousDowntime_Sample( self ):
		k = 1
		requestTimeList = [ 0, 1000 ]
		self.assertEqual( DisastrousDowntime.minimumServers( k, requestTimeList ), 1 )

		k = 2
		requestTimeList = [ 1000, 1010, 1999 ]
		self.assertEqual( DisastrousDowntime.minimumServers( k, requestTimeList ), 2 )

'''
Benelux Algorithm Programming Contest 2014
Problem B : Button Bashing
'''

class ButtonBashing:
	def __init__( self, cookingTime, buttonList ):
		self.cookingTime = cookingTime
		self.buttonList = buttonList
		self.minimumCookingTime, self.maximumCookingTime = 0, 3600

	def go( self ):
		pressList = [ None for _ in range( self.maximumCookingTime + 1 ) ]
		pressList[ 0 ] = 0

		q = deque()
		q.append( 0 )

		bestReading = float( 'inf' )
		while len( q ) > 0:
			currentReading = q.popleft()
			press = pressList[ currentReading ]

			if currentReading == self.cookingTime:
				return (press, 0)

			if currentReading > self.cookingTime:
				bestReading = min( bestReading, currentReading )

			for dx in self.buttonList:
				newReading = currentReading + dx
				newReading = min( newReading, self.maximumCookingTime )
				newReading = max( newReading, self.minimumCookingTime )

				if pressList[ newReading ] is None:
					pressList[ newReading ] = press + 1
					q.append( newReading )
		return (pressList[ bestReading ], bestReading - self.cookingTime)

class ButtonBashingTest( unittest.TestCase ):
	def test_ButtonBashing( self ):
		with open( 'tests/button/B.in' ) as inputFile, open( 'tests/button/B.out' ) as solutionFile:
			N = readInteger( inputFile )
			for i in range( N ):
				N, cookingTime = readIntegers( inputFile )
				buttonList = list( readIntegers( inputFile ) )
				state = tuple( readIntegers( solutionFile ) )

				print( 'Testcase {} N = {} cookingTime = {} state = {}'.format( i + 1, N, cookingTime, state ) )
				self.assertEqual( ButtonBashing( cookingTime, buttonList ).go(), state )

	def test_ButtonBashing_Sample( self ):
		cookingTime = 50
		buttonList = [ -10, 10, 60 ]
		self.assertEqual( ButtonBashing( cookingTime, buttonList ).go(), (2, 0) )

		cookingTime = 50
		buttonList = [ 20 ]
		self.assertEqual( ButtonBashing( cookingTime, buttonList ).go(), (3, 10) )

'''
East Central North America Regional Contest 2019
Problem I : Square Rooms
'''

class SquareOutOfBounds( Exception ):
	pass

class SquareRooms:
	def __init__( self, rows, cols, areaLayout ):
		self.rows, self.cols = rows, cols
		self.areaLayout = areaLayout
		self.elgnatcer = 'elgnatcer'
		self.emptyCell, self.treasureCell, self.blockedCell = '.$#'

		self.table = [ [ 0 for _ in range( cols ) ] for _ in range( rows ) ]
		for i in range( rows ):
			for j in range( cols ):
				treasureCount = 1 if self.areaLayout[ i ][ j ] == self.treasureCell else 0

				A = self.table[ i - 1 ][ j ] if i > 0 else 0
				B = self.table[ i ][ j - 1 ] if j > 0 else 0
				C = self.table[ i - 1 ][ j - 1 ] if i > 0 and j > 0 else 0
				self.table[ i ][ j ] = A + B - C + treasureCount

	def _treasureInSquare( self, i, j, size ):
		u, v = i + size - 1, j + size - 1
		if u >= self.rows or v >= self.cols:
			raise SquareOutOfBounds()

		X = self.table[ u ][ v ]
		A = self.table[ i - 1 ][ v ] if i > 0 else 0
		B = self.table[ u ][ j - 1 ] if j > 0 else 0
		C = self.table[ i - 1 ][ j - 1 ] if i > 0 and j > 0 else 0
		return X - A - B + C

	def _getNextCell( self, fillTable, cellToFill ):
		i, j = cellToFill
		while True:
			j += 1
			if j == self.cols:
				i, j = i + 1, 0
			if i == self.rows:
				return None
			if fillTable[ i ][ j ] is None:
				return i, j

	def _fillSquare( self, fillTable, cellToFill, fillLabels, fillIndex ):
		if cellToFill is None:
			return True

		i, j = cellToFill
		size = 1
		
		while True:
			try:
				treasureCount = self._treasureInSquare( i, j, size )
			except SquareOutOfBounds:
				break
			
			if treasureCount == 0:
				size += 1
				continue
			elif treasureCount > 1:
				break
			
			overlappingFill = False
			for u, v in itertools.product( range( i, i + size ), range( j, j + size ) ):
				if fillTable[ u ][ v ] is not None:
					overlappingFill = True
					break
			if overlappingFill:
				break
			
			for u, v in itertools.product( range( i, i + size ), range( j, j + size ) ):
				fillTable[ u ][ v ] = fillLabels[ fillIndex ]
			
			nextCellToFill = self._getNextCell( fillTable, cellToFill )
			if self._fillSquare( fillTable, nextCellToFill, fillLabels, fillIndex + 1 ):
				return True
			
			for u, v in itertools.product( range( i, i + size ), range( j, j + size ) ):
				fillTable[ u ][ v ] = None

			size += 1

		return False

	def fill( self ):
		fillLabels = string.ascii_uppercase + string.ascii_lowercase
		fillIndex = 0
		fillTable = [ [ None for _ in range( self.cols ) ] for _ in range( self.rows ) ]

		startCell = None
		for i in range( self.rows ):
			for j in range( self.cols ):
				if self.areaLayout[ i ][ j ] == self.blockedCell:
					fillTable[ i ][ j ] = self.blockedCell
				elif startCell is None:
					startCell = i, j
		success = self._fillSquare( fillTable, startCell, fillLabels, fillIndex )
		if not success:
			return self.elgnatcer
		return [ ''.join( fillRow ) for fillRow in fillTable ]

class SquareRoomsTest( unittest.TestCase ):
	def test_SquareRooms( self ):
		for testfile in getTestFileList( tag='squarerooms' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/squarerooms/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/squarerooms/{}.ans'.format( testfile ) ) as solutionFile:

			rows, cols = readIntegers( inputFile )
			areaLayout = [ readString( inputFile ) for _ in range( rows ) ]
			header = readString( solutionFile )
			if header == 'elgnatcer':
				filledLayout = header
			else:
				filledLayout = [ header ]
				for _ in range( rows - 1 ):
					filledLayout.append( readString( solutionFile ) )

			print( 'Testcase {} rows = {} cols = {}'.format( testfile, rows, cols ) )
			self.assertEqual( SquareRooms( rows, cols, areaLayout ).fill(), filledLayout )

	def test_SquareRooms_Sample( self ):
		rows, cols = 7, 8
		areaLayout = [
		'........',
		'.$....$.',
		'$...$...',
		'......#$',
		'.$....#$',
		'...$....',
		'$$.....$'
		]
		filledLayout = [
		'AABBBCCC',
		'AABBBCCC',
		'DDBBBCCC',
		'DDEEEE#F',
		'GGEEEE#H',
		'GGEEEEII',
		'JKEEEEII'
		]
		self.assertEqual( SquareRooms( rows, cols, areaLayout ).fill(), filledLayout )

		rows, cols = 1, 5
		areaLayout = [
		'#$#$#'
		]
		filledLayout = [
		'#A#B#'
		]
		self.assertEqual( SquareRooms( rows, cols, areaLayout ).fill(), filledLayout )

		rows, cols = 1, 5
		areaLayout = [
		'#$.$.'
		]
		self.assertEqual( SquareRooms( rows, cols, areaLayout ).fill(), 'elgnatcer' )

'''
German Collegiate Programming Contest 2012
Problem K : Treasure Diving
'''

class TreasureDiving:
	def __init__( self, caves, tunnels, networkInfo, idolLocations, airCapacity ):
		self.caves = caves
		self.graph = [ list() for _ in range( caves ) ]
		for u, v, airCapacityNeeded in networkInfo:
			self.graph[ u ].append( (v, airCapacityNeeded) )
			self.graph[ v ].append( (u, airCapacityNeeded) )
		self.airCapacity = airCapacity
		
		self.startLocation = 0
		self.treasureCountDict = dict()
		self.treasureCountDict[ self.startLocation ] = 0
		for idolLocation in idolLocations:
			self.treasureCountDict[ idolLocation ] = self.treasureCountDict.get( idolLocation, 0 ) + 1

	def _shortestPath( self, fromCave ):
		distanceList = [ float( 'inf' ) for _ in range( self.caves ) ]
		distanceList[ fromCave ] = 0

		q = list()
		q.append( (0, fromCave) )

		while len( q ) > 0:
			distance, cave = heapq.heappop( q )
			if distance > distanceList[ cave ]:
				continue
			for adjacentCave, additionalDistance in self.graph[ cave ]:
				totalDistance = distance + additionalDistance
				if totalDistance < distanceList[ adjacentCave ]:
					distanceList[ adjacentCave ] = totalDistance
					heapq.heappush( q, (totalDistance, adjacentCave) )
		return distanceList

	def _maximumIdols( self, vertexCount, startLocation, idolGraph, airCapacity, treasureCountDict ):
		maximumIdolsCollected = 0

		bitmap, idolsCollected = 0, 0

		distanceList = [ [ float( 'inf' ) for _ in range( 1 << vertexCount ) ] for _ in range( vertexCount ) ]
		distanceList[ startLocation ][ bitmap ] = 0

		q = list()
		q.append( (0, bitmap, idolsCollected, startLocation) )

		while len( q ) > 0:
			airUsed, bitmap, idolsCollected, currentLocation = heapq.heappop( q )

			if bitmap & ( 1 << currentLocation ) == 0:
				idolsCollected += treasureCountDict[ currentLocation ]
				bitmap = bitmap | ( 1 << currentLocation )

			if currentLocation == startLocation:
				maximumIdolsCollected = max( maximumIdolsCollected, idolsCollected )

			if airUsed > distanceList[ currentLocation ][ bitmap ]:
				continue

			for adjacentLocation, airCapacityNeeded in idolGraph[ currentLocation ]:
				totalAirUsed = airUsed + airCapacityNeeded
				if totalAirUsed > airCapacity:
					continue
				if totalAirUsed < distanceList[ adjacentLocation ][ bitmap ]:
					distanceList[ adjacentLocation ][ bitmap ] = totalAirUsed
					heapq.heappush( q, (totalAirUsed, bitmap, idolsCollected, adjacentLocation) )
		return maximumIdolsCollected

	def maximumIdols( self ):
		keyLocations = list( self.treasureCountDict.keys() )
		startLocation = None
		vertexCount = len( self.treasureCountDict )
		treasureCountDict = dict()
		
		idolGraph = [ list() for _ in range( vertexCount ) ]
		
		for i, keyLocation in enumerate( keyLocations ):
			if keyLocation == self.startLocation:
				startLocation = i
			treasureCountDict[ i ] = self.treasureCountDict[ keyLocation ]
			distanceList = self._shortestPath( keyLocation )
			for j in range( vertexCount ):
				if i == j:
					continue
				distance = distanceList[ keyLocations[ j ] ]
				if distance < float( 'inf' ):
					idolGraph[ i ].append( (j, distance) )
					idolGraph[ j ].append( (i, distance) )

		return self._maximumIdols( vertexCount, startLocation, idolGraph, self.airCapacity, treasureCountDict )

class TreasureDivingTest( unittest.TestCase ):
	def test_TreasureDiving( self ):
		for testfile in getTestFileList( tag='treasurediving' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/treasurediving/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/treasurediving/{}.out'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				caves, tunnels = readIntegers( inputFile )
				networkInfo = [ tuple( readIntegers( inputFile ) ) for _ in range( tunnels ) ]
				_ = readInteger( inputFile )
				idolLocations = list( readIntegers( inputFile ) )
				airCapacity = readInteger( inputFile )

				maximumIdols = readInteger( solutionFile )

				formatString = 'Testcase {}#{} caves = {} tunnels = {} airCapacity = {} maximumIdols = {}'
				print( formatString.format( testfile, i + 1, caves, tunnels, airCapacity, maximumIdols ) )
				self.assertEqual( TreasureDiving( caves, tunnels, networkInfo, idolLocations, airCapacity ).maximumIdols(), maximumIdols )

	def test_TreasureDiving_Sample( self ):
		caves, tunnels = 5, 3
		networkInfo = [ (0, 1, 10), (0, 2, 20), (0, 3, 30) ]
		idolLocations = [ 1, 2, 3, 4 ]
		airCapacity = 30
		self.assertEqual( TreasureDiving( caves, tunnels, networkInfo, idolLocations, airCapacity ).maximumIdols(), 1 )

		caves, tunnels = 5, 3
		networkInfo = [ (0, 1, 10), (0, 2, 20), (0, 3, 30) ]
		idolLocations = [ 1, 2, 3, 4 ]
		airCapacity = 60
		self.assertEqual( TreasureDiving( caves, tunnels, networkInfo, idolLocations, airCapacity ).maximumIdols(), 2 )

		caves, tunnels = 5, 3
		networkInfo = [ (0, 1, 10), (0, 2, 20), (0, 3, 30) ]
		idolLocations = [ 1, 2, 3, 4 ]
		airCapacity = 10000
		self.assertEqual( TreasureDiving( caves, tunnels, networkInfo, idolLocations, airCapacity ).maximumIdols(), 3 )

'''
The 2014 Benelux Algorithm Programming Contest - Preliminaries
BAPC 2014 - Preliminaries
Problem B : Failing Components
'''

class FailingComponents:
	def __init__( self, components, dependencyList, failingComponent ):
		self.components = components
		self.graph = [ list() for _ in range( self.components + 1 ) ]
		for a, b, time in dependencyList:
			self.graph[ b ].append( (a, time) )
		self.failingComponent = failingComponent

	def analyze( self ):
		failedComponentsList = [ False for _ in range( self.components + 1 ) ]

		q = list()
		q.append( (0, self.failingComponent) )

		maximumTimeStamp = 0

		while len( q ) > 0:
			timeStamp, failingComponent = heapq.heappop( q )
			if failedComponentsList[ failingComponent ]:
				continue
			maximumTimeStamp = timeStamp		
			failedComponentsList[ failingComponent ] = True
			
			for component, timeTaken in self.graph[ failingComponent ]:
				if not failedComponentsList[ component ]:
					heapq.heappush( q, (timeStamp + timeTaken, component) )
		totalFailedComponents = failedComponentsList.count( True )
		return (totalFailedComponents, maximumTimeStamp)

class FailingComponentsTest( unittest.TestCase ):
	def test_FailingComponents( self ):
		with open( 'tests/failingcomponents/B.in' ) as inputFile, open( 'tests/failingcomponents/B.out' ) as solutionFile:
			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				components, dependencies, failingComponent = readIntegers( inputFile )
				dependencyList = [ tuple( readIntegers( inputFile ) ) for _ in range( dependencies ) ]
				state = tuple( readIntegers( solutionFile ) )

				formatString = 'Testcase {} components = {} dependencies = {} state = {}'
				print( formatString.format( i + 1, components, dependencies, state ) )
				self.assertEqual( FailingComponents( components, dependencyList, failingComponent ).analyze(), state )

	def test_FailingComponents_Sample( self ):
		components = 3
		dependencyList = [ (2, 1, 5), (3, 2, 5) ]
		failingComponent = 2
		self.assertEqual( FailingComponents( components, dependencyList, failingComponent ).analyze(), (2, 5) )

		components = 3
		dependencyList = [ (2, 1, 2), (3, 1, 8), (3, 2, 4) ]
		failingComponent = 1
		self.assertEqual( FailingComponents( components, dependencyList, failingComponent ).analyze(), (3, 6) )

'''
Nordic Collegiate Programming Contest 2014
Problem A : Amanda Lounges
Problem ID: amanda
'''

class AmandaLounges:
	def __init__( self, airports, routeInfo ):
		self.airports = airports
		self.routeInfo = routeInfo

	def analyze( self ):
		loungePresent = [ None for _ in range( self.airports + 1 ) ]

		q = deque()
		deferEdgeList = list()
		
		for A, B, state in self.routeInfo:
			if state == 1:
				deferEdgeList.append( (A, B) )
				continue

			if state == 0:
				if loungePresent[ A ] == True or loungePresent[ B ] == True:
					return 'impossible'
				loungePresent[ A ] = loungePresent[ B ] = False
			elif state == 2:
				if loungePresent[ A ] == False or loungePresent[ B ] == False:
					return 'impossible'
				loungePresent[ A ] = loungePresent[ B ] = True
			q.append( A )
			q.append( B )

		partialGraph = [ list() for _ in range( self.airports + 1 ) ]
		for A, B in deferEdgeList:
			partialGraph[ A ].append( B )
			partialGraph[ B ].append( A )

		while len( q ) > 0:
			airport = q.popleft()
			for adjacentAirport in partialGraph[ airport ]:
				if loungePresent[ adjacentAirport ] == loungePresent[ airport ]:
					return 'impossible'
				elif loungePresent[ adjacentAirport ] is None:
					loungePresent[ adjacentAirport ] = not loungePresent[ airport ]
					q.append( adjacentAirport )

		class AmandaLoungeConflict( Exception ):
			pass

		def _oppositeColor( color ):
			return 'WHITE' if color == 'BLACK' else 'BLACK'

		def _applyColor( airport, colorList, partialGraph ):
			q = deque()
			q.append( airport )

			colorList[ airport ] = 'WHITE'
			countDict = {
			'WHITE' : 1,
			'BLACK' : 0
			}

			while len( q ) > 0:
				airport = q.popleft()
				for adjacentAirport in partialGraph[ airport ]:
					if colorList[ adjacentAirport ] == colorList[ airport ]:
						raise AmandaLoungeConflict()
					elif loungePresent[ adjacentAirport ] is None and colorList[ adjacentAirport ] is None:
						oppositeColor = _oppositeColor( colorList[ airport ] )
						countDict[ oppositeColor ] += 1
						colorList[ adjacentAirport ] = oppositeColor
						q.append( adjacentAirport )
			return min( countDict[ 'WHITE' ], countDict[ 'BLACK' ] )

		colorList = [ None for _ in range( self.airports + 1 ) ]
		totalLounges = loungePresent.count( True )
		for airport in range( self.airports + 1 ):
			if loungePresent[ airport ] is None and colorList[ airport ] is None:
				try:
					count = _applyColor( airport, colorList, partialGraph )
				except AmandaLoungeConflict:
					return 'impossible'
				totalLounges += count
		return totalLounges

class AmandaLoungesTest( unittest.TestCase ):
	def test_AmandaLounges( self ):
		for testfile in getTestFileList( tag='amanda' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/amanda/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/amanda/{}.ans'.format( testfile ) ) as solutionFile:

			airports, routes = readIntegers( inputFile )
			routeInfo = [ tuple( readIntegers( inputFile ) ) for _ in range( routes ) ]
			state = readString( solutionFile )
			if state != 'impossible':
				state = int( state )

			print( 'Testcase {} airports = {} routes = {} state = {}'.format( testfile, airports, routes, state ) )
			self.assertEqual( AmandaLounges( airports, routeInfo ).analyze(), state )

	def test_AmandaLounges_Sample( self ):
		airports = 4
		routeInfo = [ (1, 2, 2), (2, 3, 1), (3, 4, 1), (4, 1, 2) ]
		self.assertEqual( AmandaLounges( airports, routeInfo ).analyze(), 3 )

		airports = 5
		routeInfo = [ (1, 2, 1), (2, 3, 1), (2, 4, 1), (2, 5, 1), (4, 5, 1) ]
		self.assertEqual( AmandaLounges( airports, routeInfo ).analyze(), 'impossible' )

		airports = 4
		routeInfo = [ (1, 2, 1), (2, 3, 0), (2, 4, 1), (3, 1, 1), (3, 4, 1) ]
		self.assertEqual( AmandaLounges( airports, routeInfo ).analyze(), 2 )

'''
Nordic Collegiate Programming Contest 2014
Problem A : Train Passengers
Problem ID: trainpassengers
'''

class TrainPassengers:
	def __init__( self, capacity, logEntryList ):
		self.capacity = capacity
		self.logEntryList = logEntryList

	def analyze( self ):
		count = 0
		lastEntryIndex = len( self.logEntryList ) - 1
		for index, (departureCount, arrivalCount, waitCount) in enumerate( self.logEntryList ):
			if index == lastEntryIndex and ( waitCount != 0 or arrivalCount != 0 or departureCount != count ):
				return 'impossible'
			if departureCount > count:
				return 'impossible'
			count = count - departureCount + arrivalCount
			if count < 0 or count > self.capacity:
				return 'impossible'
			if waitCount > 0 and count < self.capacity:
				return 'impossible'
		return 'possible'

class TrainPassengersTest( unittest.TestCase ):
	def test_TrainPassengers( self ):
		for testfile in getTestFileList( tag='trainpassengers' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/trainpassengers/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/trainpassengers/{}.ans'.format( testfile ) ) as solutionFile:

			capacity, logEntries = readIntegers( inputFile )
			logEntryList = [ tuple( readIntegers( inputFile ) ) for _ in range( logEntries ) ]
			state = readString( solutionFile )

			print( 'Testcase {} capacity = {} logEntries = {} state = {}'.format( testfile, capacity, logEntries, state ) )
			self.assertEqual( TrainPassengers( capacity, logEntryList ).analyze(), state )

	def test_TrainPassengers_Sample( self ):
		capacity = 1
		logEntryList = [ (0, 1, 1), (1, 0, 0) ]
		self.assertEqual( TrainPassengers( capacity, logEntryList ).analyze(), 'possible' )

		capacity = 1
		logEntryList = [ (1, 0, 0), (0, 1, 0) ]
		self.assertEqual( TrainPassengers( capacity, logEntryList ).analyze(), 'impossible' )

		capacity = 1
		logEntryList = [ (0, 1, 0), (1, 0, 1) ]
		self.assertEqual( TrainPassengers( capacity, logEntryList ).analyze(), 'impossible' )

		capacity = 1
		logEntryList = [ (0, 1, 1), (0, 0, 0) ]
		self.assertEqual( TrainPassengers( capacity, logEntryList ).analyze(), 'impossible' )

'''
North Central North America Regional Contest 2020
Problem D : Substring Characters
'''

class SubstringCharacters:
	@staticmethod
	def countProperMinimalUniqueSubstrings( inputString ):
		properMinimalUniqueSubstrings = set()

		uniqueCharacterCount = len( set( inputString ) )
		if uniqueCharacterCount == len( inputString ):
			return 0

		i = j = 0
		frequencyDict = dict()
		while j < len( inputString ):
			character = inputString[ j ]
			j += 1
			frequencyDict[ character ] = frequencyDict.get( character, 0 ) + 1

			while len( frequencyDict ) == uniqueCharacterCount:
				candidateString = inputString[ i : j ]
				leftEnd, rightEnd = candidateString[ 0 ], candidateString[ -1 ]
				if candidateString.count( leftEnd ) == candidateString.count( rightEnd ) == 1:
					properMinimalUniqueSubstrings.add( candidateString )

				character = inputString[ i ]
				i += 1
				frequencyDict[ character ] -= 1
				if frequencyDict[ character ] == 0:
					del frequencyDict[ character ]
		return len( properMinimalUniqueSubstrings )

class SubstringCharactersTest( unittest.TestCase ):
	def test_SubstringCharacters( self ):
		for testfile in getTestFileList( tag='substrings' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/substrings/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/substrings/{}.ans'.format( testfile ) ) as solutionFile:

			inputStringList = readAllStrings( inputFile )
			expectedCountList = readAllIntegers( solutionFile )

			for inputString, count in zip( inputStringList, expectedCountList ):
				print( 'Testcase {} [{}] -> {}'.format( testfile, inputString, count ) )
				self.assertEqual( SubstringCharacters.countProperMinimalUniqueSubstrings( inputString ), count )

	def test_SubstringCharacters_Sample( self ):
		inputStringList = [
		('aabbabb', 2), ('abAB34aB3ba7', 1), ('104001144', 3), ('aaabcaaa', 2),
		('a', 0), ('bb', 1), ('bd', 0), ('1234567', 0)
		]
		for inputString, count in inputStringList:
			self.assertEqual( SubstringCharacters.countProperMinimalUniqueSubstrings( inputString ), count )

'''
East Central North America Regional Contest 2015
Problem D : Rings
'''

class Rings:
	def __init__( self, rows, cols, areaLayout ):
		self.rows, self.cols = rows, cols
		self.areaLayout = areaLayout
		
		self.emptyCell, self.treeCell = '.T'
		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

	def analyze( self ):
		ringLayout = [ list( areaLayoutRow ) for areaLayoutRow in self.areaLayout ]

		q = deque()
		for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
			if ringLayout[ u ][ v ] == self.treeCell:
				onBoundary = False
				for du, dv in self.adjacentCellDelta:
					x, y = u + du, v + dv
					if not 0 <= x < self.rows or not 0 <= y < self.cols or ringLayout[ x ][ y ] != self.treeCell:
						onBoundary = True
						break
				if onBoundary:
					q.append( (u, v) )

		ringNumber = 1
		while len( q ) > 0:
			N = len( q )
			while N > 0:
				N = N - 1
				u, v = q.popleft()
				if ringLayout[ u ][ v ] != self.treeCell:
					continue
				ringLayout[ u ][ v ] = ringNumber
				for du, dv in self.adjacentCellDelta:
					x, y = u + du, v + dv
					if 0 <= x < self.rows and 0 <= y < self.cols:
						q.append( (x, y) )
			ringNumber += 1

		emptyToken = '..' if ringNumber < 10 else '...'
		justify = 2 if ringNumber < 10 else 3

		for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
			if ringLayout[ u ][ v ] == self.emptyCell:
				ringLayout[ u ][ v ] = emptyToken
			else:
				ringLayout[ u ][ v ] = str( ringLayout[ u ][ v ] ).rjust( justify, self.emptyCell )

		return [ ''.join( ringLayoutRow ) for ringLayoutRow in ringLayout ]

class RingsTest( unittest.TestCase ):
	def test_Rings( self ):
		for testfile in getTestFileList( tag='rings' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/rings/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/rings/{}.ans'.format( testfile ) ) as solutionFile:

			rows, cols = readIntegers( inputFile )
			areaLayout = [ readString( inputFile ) for _ in range( rows ) ]
			ringNumbers = [ readString( solutionFile ) for _ in range( rows ) ]

			print( 'Testcase {} rows = {} cols = {}'.format( testfile, rows, cols ) )
			self.assertEqual( Rings( rows, cols, areaLayout ).analyze(), ringNumbers )

	def test_Rings_Sample( self ):
		rows, cols = 6, 6
		areaLayout = [
		'.TT...',
		'TTTT..',
		'TTTTT.',
		'TTTTT.',
		'TTTTTT',
		'..T...'
		]
		ringNumbers = [
		'...1.1......',
		'.1.2.2.1....',
		'.1.2.3.2.1..',
		'.1.2.3.2.1..',
		'.1.1.2.1.1.1',
		'.....1......'
		]
		self.assertEqual( Rings( rows, cols, areaLayout ).analyze(), ringNumbers )

		rows, cols = 3, 4
		areaLayout = [
		'TT..',
		'TT..',
		'....'
		]
		ringNumbers = [
		'.1.1....',
		'.1.1....',
		'........'
		]
		self.assertEqual( Rings( rows, cols, areaLayout ).analyze(), ringNumbers )

'''
East Central North America Regional Contest 2015
Problem E : Squawk Virus
'''

class SquawkVirus:
	def __init__( self, users, links, initialUser, timeStamp ):
		self.users = users
		self.graph = [ list() for _ in range( users ) ]
		for userA, userB in links:
			self.graph[ userA ].append( userB )
			self.graph[ userB ].append( userA )
		self.initialUser = initialUser
		self.timeStamp = timeStamp

	def squawksSent( self ):
		time = 0

		squawksSentDict = dict()
		squawksSentDict[ self.initialUser ] = 1
		
		while time < self.timeStamp:
			nextSquawksSentDict = dict()
			
			for user, squawks in squawksSentDict.items():
				for adjacentUser in self.graph[ user ]:
					if adjacentUser not in nextSquawksSentDict:
						nextSquawksSentDict[ adjacentUser ] = 0
					nextSquawksSentDict[ adjacentUser ] += squawks

			squawksSentDict = nextSquawksSentDict
			time += 1
		return sum( squawksSentDict.values() )

class SquawkVirusTest( unittest.TestCase ):
	def test_SquawkVirus( self ):
		for testfile in getTestFileList( tag='squawk' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/squawk/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/squawk/{}.ans'.format( testfile ) ) as solutionFile:

			users, linkCount, initialUser, timeStamp = readIntegers( inputFile )
			links = [ tuple( readIntegers( inputFile ) ) for _ in range( linkCount ) ]
			squawks = readInteger( solutionFile )

			formatString = 'Testcase {} users = {} linkCount = {} timeStamp = {} squawks = {}'
			print( formatString.format( testfile, users, linkCount, timeStamp, squawks ) )
			self.assertEqual( SquawkVirus( users, links, initialUser, timeStamp ).squawksSent(), squawks )

	def test_SquawkVirus_Sample( self ):
		users = 4
		links = [ (0, 1), (1, 2), (2, 3) ]
		initialUser = 1
		timeStamp = 4
		self.assertEqual( SquawkVirus( users, links, initialUser, timeStamp ).squawksSent(), 8 )

		users = 5
		links = [ (0, 1), (0, 3), (1, 2), (2, 3), (2, 4) ]
		initialUser = 0
		timeStamp = 3
		self.assertEqual( SquawkVirus( users, links, initialUser, timeStamp ).squawksSent(), 10 )

'''
East Central North America Regional Contest 2018
Problem A : Car Vet
'''

class CarVet:
	def __init__( self, rows, cols, lotLayout, uncoverLocation ):
		self.rows, self.cols = rows, cols
		self.lotLayout = lotLayout

		row, col = uncoverLocation
		self.uncoverLocation = row - 1, col - 1
		
		self.emptyCell, self.blockedCell = -1, -2
		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]
		
		self.emptyLocation = None
		for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
			if self.lotLayout[ u ][ v ] == self.emptyCell:
				self.emptyLocation = u, v
				break

		self.movedCarsList = None

	def _move( self, location, movedCarsList, movedCarsSet ):
		callStack = list()
		callStack.append( ([ location ], 'EVALUATE' ) )

		while len( callStack ) > 0:
			argumentList, command = callStack.pop()

			if command == 'EVALUATE':
				emptyLocation = argumentList.pop()
				if emptyLocation == self.uncoverLocation:
					if self.movedCarsList is None or len( movedCarsList ) < len( self.movedCarsList ):
						self.movedCarsList = copy.deepcopy( movedCarsList )
					elif len( movedCarsList ) == len( self.movedCarsList ) and movedCarsList < self.movedCarsList:
						self.movedCarsList = copy.deepcopy( movedCarsList )
					continue

				u, v = emptyLocation
				for du, dv in self.adjacentCellDelta:
					x1, y1 = u + du, v + dv
					x2, y2 = x1 + du, y1 + dv
					if 0 <= x2 < self.rows and 0 <= y2 < self.cols and self.lotLayout[ x1 ][ y1 ] == self.lotLayout[ x2 ][ y2 ]:
						car = self.lotLayout[ x2 ][ y2 ]
						if car != self.blockedCell and car not in movedCarsSet:
							newEmptyLocation = x2, y2

							callStack.append( ([ emptyLocation, newEmptyLocation ], 'POP') )
							callStack.append( ([ newEmptyLocation ], 'EVALUATE' ) )
							callStack.append( ([ emptyLocation, newEmptyLocation ], 'APPLY') )			
			
			elif command == 'APPLY':
				carLocation, emptyLocation = argumentList
				u, v = carLocation
				x2, y2 = emptyLocation
				car = self.lotLayout[ x2 ][ y2 ]
				self.lotLayout[ x2 ][ y2 ] = self.emptyCell
				self.lotLayout[ u ][ v ] = car
				movedCarsSet.add( car )
				movedCarsList.append( car )

			elif command == 'POP':
				emptyLocation, carLocation = argumentList
				u, v = emptyLocation
				x2, y2 = carLocation
				car = movedCarsList.pop()
				movedCarsSet.remove( car )
				self.lotLayout[ u ][ v ] = self.emptyCell
				self.lotLayout[ x2 ][ y2 ] = car

	def analyze( self ):
		movedCarsList = list()
		movedCarsSet = set()
		self._move( self.emptyLocation, movedCarsList, movedCarsSet )
		return 'impossible' if self.movedCarsList is None else self.movedCarsList

class CarVetTest( unittest.TestCase ):
	def test_CarVet( self ):
		for testfile in getTestFileList( tag='carvet' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/carvet/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/carvet/{}.ans'.format( testfile ) ) as solutionFile:

			rows, cols = readIntegers( inputFile )
			lotLayout = [ list( readIntegers( inputFile ) ) for _ in range( rows ) ]
			uncoverLocation = tuple( readIntegers( inputFile ) )
			state = readString( solutionFile )
			if state != 'impossible':
				state = list( map( int, state.split() ) )

			print( 'Testcase {} rows = {} cols = {} uncoverLocation = {}'.format( testfile, rows, cols, uncoverLocation ) )
			self.assertEqual( CarVet( rows, cols, lotLayout, uncoverLocation ).analyze(), state )

	def test_CarVet_Sample( self ):
		rows, cols = 4, 4
		lotLayout = [
		[  8,  8, -1,  9 ],
		[  4, 10, 10,  9 ],
		[  4,  3,  3, -2 ],
		[ 16, 16,  2,  2 ]
		]
		uncoverLocation = 3, 3
		self.assertEqual( CarVet( rows, cols, lotLayout, uncoverLocation ).analyze(), [ 8, 4, 3 ] )

		rows, cols = 4, 4
		lotLayout = [
		[  8,  8, -2,  9 ],
		[  4, 10, 10,  9 ],
		[  4,  3,  3, -1 ],
		[ 16, 16,  2,  2 ]
		]
		uncoverLocation = 3, 3
		self.assertEqual( CarVet( rows, cols, lotLayout, uncoverLocation ).analyze(), 'impossible' )

'''
North Central North America Regional Contest 2020
Problem A : LogDB
'''

class LogDB:
	def __init__( self, factStringList, queryStringList ):
		self.factStringList = factStringList
		self.queryStringList = queryStringList

	def _match( self, parameterList, parameterTuple ):
		variableDict = dict()
		for queryToken, parameterName in zip( parameterList, parameterTuple ):
			if queryToken == '_' or queryToken == parameterName:
				continue
			if not queryToken.startswith( '_' ) and queryToken != parameterName:
				return False
			# The queryToken starts with "_".
			if queryToken not in variableDict:
				variableDict[ queryToken ] = parameterName
			elif variableDict[ queryToken ] != parameterName:
				return False
		return True

	def _count( self, factDict, factName, parameterList ):
		count = 0
		factKey = (factName, len( parameterList ))
		if factKey not in factDict:
			return count
		for parameterTuple, frequency in factDict[ factKey ].items():
			if self._match( parameterList, parameterTuple ):
				count += frequency
		return count

	def _factParser( self, inputString, callbackFunction ):
		factName = None
		parameterList = list()

		i = j = 0
		while j < len( inputString ):
			character = inputString[ j ]
			if character == '(':
				factName = inputString[ i : j ].strip()
				i = j = j + 1
			elif character == ',' or character == ')':
				parameterList.append( inputString[ i : j ].strip() )
				i = j = j + 1
				if character == ')':
					callbackFunction( factName, parameterList )
					parameterList.clear()
			else:
				j = j + 1

	def analyze( self ):
		factDict = dict()
		def addToFactDict( factName, parameterList ):
			nonlocal factDict
			factKey = (factName, len( parameterList ))
			if factKey not in factDict:
				factDict[ factKey ] = dict()
			parameterKey = tuple( parameterList )
			factDict[ factKey ][ parameterKey ] = factDict[ factKey ].get( parameterKey, 0 ) + 1 

		for factString in self.factStringList:
			self._factParser( factString, addToFactDict )
					
		resultList = list()
		def processQuery( factName, parameterList ):
			nonlocal resultList
			resultList.append( self._count( factDict, factName, parameterList ) )
		
		for queryString in self.queryStringList:
			self._factParser( queryString, processQuery )
		
		return resultList

class LogDBTest( unittest.TestCase ):
	def test_LogDB( self ):
		for testfile in getTestFileList( tag='logdb' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/logdb/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/logdb/{}.ans'.format( testfile ) ) as solutionFile:

			factStringList = list()
			queryStringList = list()

			listToPopulate = factStringList
			for inputString in readAllStrings( inputFile ):
				if len( inputString ) == 0:
					listToPopulate = queryStringList
					continue
				listToPopulate.append( inputString )

			resultList = readAllIntegers( solutionFile )

			print( 'Testcase {} facts = {} queries = {}'.format( testfile, len( factStringList ), len( queryStringList ) ) )
			self.assertEqual( LogDB( factStringList, queryStringList ).analyze(), resultList )

	def test_LogDB_Sample( self ):
		factStringList = [
		'test(arg1, arg2,arg3) test(1,2,3,not4)5(five) five_seven(john_smith,10_17_57)',
		'foo(1,2,3,4) foo(5,6,7,8) foo(1,2,7,8)arc(80) foo(abc,xyz)',
		'bar(1,2) bar (7,8) zoom(8,7)',
		'arc(80) nofoo(alpha,d1,d2,d1,d4,d1)',
		'foo(bar,spam) foo(more,less)'
		]
		queryStringList = [
		'test(_,_,_,_)',
		'arc(80)',
		'  foo(bar,_)',
		'  foo(_,spam)',
		'  foo(_,less)',
		'  nofoo(_,_p1,_,_p1,_,_p1)',
		'  foo(bar,less)',
		'foo(_,_,_,_)',
		'foo(_,_,_30,_40)',
		'foo(_,_,_30,_40)',
		'foo(_,_,_30,_40)',
		'foo(_x,_,_30,_40)',
		'foo(_,_,_30,_40)'
		]
		resultList = [ 1, 2, 1, 1, 1, 1, 0, 3, 3, 3, 3, 3, 3 ]
		self.assertEqual( LogDB( factStringList, queryStringList ).analyze(), resultList )

class ShortestPaths:
	@staticmethod
	def allPathsShortestPath( distanceMatrix ):
		vertexCount = len( distanceMatrix )
		allPathsShortestPathMatrix = copy.deepcopy( distanceMatrix )
		
		for k in range( vertexCount ):
			for u in range( vertexCount ):
				for v in range( vertexCount ):
					possiblePathLength = allPathsShortestPathMatrix[ u ][ k ] + allPathsShortestPathMatrix[ k ][ v ]
					allPathsShortestPathMatrix[ u ][ v ] = min( allPathsShortestPathMatrix[ u ][ v ], possiblePathLength )
		return allPathsShortestPathMatrix

	@staticmethod
	def TSP( distanceMatrix ):
		vertexCount = len( distanceMatrix )

		vertexState = [ [ None for _ in range( 1 << vertexCount ) ] for _ in range( vertexCount ) ]

		def _tsp( vertex, bitmap, vertexState ):
			if vertexState[ vertex ][ bitmap ] is not None:
				return vertexState[ vertex ][ bitmap ]
			
			cost = float( 'inf' )
			for adjacentVertex in range( vertexCount ):
				if bitmap & ( 1 << sourceVertex ) > 0:
					newBitmap = bitmap & ~ ( 1 << sourceVertex )
					cost = min( cost, _tsp( sourceVertex, newBitmap, vertexState ) + distanceMatrix[ adjacentVertex ][ vertex ] )
			vertexState[ vertex ][ bitmap ] = cost
			return cost

		startVertex = 0
		return _tsp( startVertex, ( 1 << vertexCount ) - 1 )

	@staticmethod
	def singleSourceShortestPath( graph, startVertex, disableEdges=None ):
		vertexCount = len( graph )
		distanceList = [ float( 'inf' ) for _ in range( vertexCount ) ]
		pathList = [ list() for _ in range( vertexCount ) ]

		if disableEdges is None:
			disableEdges = set()

		q = list()
		q.append( (0, startVertex) )

		distanceList[ startVertex ] = 0

		while len( q ) > 0:
			distance, currentVertex = heapq.heappop( q )
			if distance > distanceList[ currentVertex ]:
				continue
			for adjacentVertex, additionalDistance in graph[ currentVertex ]:
				if (currentVertex, adjacentVertex) in disableEdges:
					continue

				totalDistance = distance + additionalDistance
				if totalDistance < distanceList[ adjacentVertex ]:
					distanceList[ adjacentVertex ] = totalDistance
					pathList[ adjacentVertex ].clear()
					pathList[ adjacentVertex ].append( currentVertex )
					heapq.heappush( q, (totalDistance, adjacentVertex) )
				elif totalDistance == distanceList[ adjacentVertex ]:
					pathList[ adjacentVertex ].append( currentVertex )
		return distanceList, pathList

'''
The UK & Ireland Programming Contest 2020
Problem F : Family Fares
'''

class FamilyFares:
	def __init__( self, stationCount, groupTicketCost, connectionList, memberList ):
		self.stationCount = stationCount
		self.groupTicketCost = groupTicketCost
		self.graph = [ list() for _ in range( self.stationCount + 1 ) ]
		for A, B, length in connectionList:
			self.graph[ A ].append( (B, length) )
			self.graph[ B ].append( (A, length) )
		self.memberList = memberList
		self.resortStation = 1

	def _popcount( self, bitmap ):
		count = 0
		while bitmap > 0:
			bitmap = bitmap & ( bitmap - 1 )
			count += 1
		return count

	def minimumCost( self ):
		distanceList, pathList = ShortestPaths.singleSourceShortestPath( self.graph, self.resortStation )

		numberOfTravelersList = [ 0 for _ in range( self.stationCount + 1 ) ]

		totalIndividualTicketsCost = 0

		for index, station in enumerate( self.memberList ):
			numberOfTravelersList[ station ] |= ( 1 << index )
			totalIndividualTicketsCost += distanceList[ station ]
		
		q = list()
		processedStations = set()
		for station in self.memberList:
			if station not in processedStations:
				q.append( (- distanceList[ station ], station) )
			processedStations.add( station )
		heapq.heapify( q )

		while len( q ) > 0:
			_, station = heapq.heappop( q )
			for fromStation in pathList[ station ]:
				numberOfTravelersList[ fromStation ] |= numberOfTravelersList[ station ]
				if fromStation not in processedStations:
					processedStations.add(fromStation)
					heapq.heappush( q, (- distanceList[ fromStation ], fromStation) )

		bestCost = totalIndividualTicketsCost
		for station in range( self.stationCount + 1 ):
			travelerCount = self._popcount( numberOfTravelersList[ station ] )
			possibleCost = totalIndividualTicketsCost - ( distanceList[ station ] * travelerCount ) + \
			               ( self.groupTicketCost * travelerCount )
			bestCost = min( bestCost, possibleCost )
		return bestCost

class FamilyFaresTest( unittest.TestCase ):
	def test_FamilyFares( self ):
		for testfile in getTestFileList( tag='familyfares' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/familyfares/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/familyfares/{}.ans'.format( testfile ) ) as solutionFile:

			stationCount, connections, _, groupTicketCost = readIntegers( inputFile )
			memberList = list( readIntegers( inputFile ) )
			connectionList = [ tuple( readIntegers( inputFile ) ) for _ in range( connections ) ]
			bestCost = readInteger( solutionFile )

			print( 'Testcase {} stationCount = {} connections = {} bestCost = {}'.format( testfile, stationCount, connections, bestCost ) )
			self.assertEqual( FamilyFares( stationCount, groupTicketCost, connectionList, memberList ).minimumCost(), bestCost )

	def test_FamilyFares_Sample( self ):
		stationCount = 6
		groupTicketCost = 10
		connectionList = [ (1, 2, 10), (2, 3, 10), (3, 4, 10), (4, 5, 2), (4, 6, 3) ]
		memberList = [ 4, 5, 6 ]
		self.assertEqual( FamilyFares( stationCount, groupTicketCost, connectionList, memberList ).minimumCost(), 35 )

		stationCount = 7
		groupTicketCost = 10
		connectionList = [ (1, 2, 100), (2, 3, 100), (3, 4, 10), (1, 5, 80), (3, 5, 30), (3, 6, 10), (6, 7, 5) ]
		memberList = [ 5, 4, 4, 7 ]
		self.assertEqual( FamilyFares( stationCount, groupTicketCost, connectionList, memberList ).minimumCost(), 145 )

		stationCount = 4
		groupTicketCost = 10
		connectionList = [ (1, 2, 20), (2, 4, 5), (1, 3, 20), (3, 4, 5), (1, 4, 30) ]
		memberList = [ 2, 4 ]
		self.assertEqual( FamilyFares( stationCount, groupTicketCost, connectionList, memberList ).minimumCost(), 25 )

'''
Virginia Tech High School Programming Contest 2018
Problem B : Pedal Power
'''

class PedalPower:
	def __init__( self, locationCount, bikePaths, paths, locationList ):
		self.locationCount = locationCount
		self.bikePathAdjacencyMatrix = [ [ float( 'inf' ) for _ in range( locationCount ) ] for _ in range( locationCount ) ]
		self.pathAdjacencyMatrix = [ [ float( 'inf' ) for _ in range( locationCount ) ] for _ in range( locationCount ) ]

		for location in range( self.locationCount ):
			self.bikePathAdjacencyMatrix[ location ][ location ] = self.pathAdjacencyMatrix[ location ][ location ] = 0

		for u, v, timeTaken in bikePaths:
			self.bikePathAdjacencyMatrix[ u ][ v ] = self.bikePathAdjacencyMatrix[ v ][ u ] = timeTaken
		for u, v, timeTaken in paths:
			self.pathAdjacencyMatrix[ u ][ v ] = self.pathAdjacencyMatrix[ v ][ u ] = timeTaken

		self.locationList = locationList

	def minimumTime( self ):
		bikePathDistanceMatrix = ShortestPaths.allPathsShortestPath( self.bikePathAdjacencyMatrix )
		pathDistanceMatrix = ShortestPaths.allPathsShortestPath( self.pathAdjacencyMatrix )

		myLocation = 0
		if self.locationList[ -1 ] != myLocation:
			self.locationList.append( myLocation )

		timeTakenWithBikeLocationList = list()
		for index, arrivalLocation in enumerate( self.locationList ):
			if index == 0:
				for v in range( self.locationCount ):
					# We are at myLocation with the bike. We have to get the bike to location v, and arrive at "arrivalLocation".
					timeTaken = bikePathDistanceMatrix[ myLocation ][ v ] + pathDistanceMatrix[ v ][ arrivalLocation ]
					timeTakenWithBikeLocationList.append( timeTaken )
				myLocation = arrivalLocation

			newTimeTakenWithBikeLocationList = list()
			for v in range( self.locationCount ):
				timeTaken = float( 'inf' )
				for u in range( self.locationCount ):
					totalTimeTaken = timeTakenWithBikeLocationList[ u ]
					# We are at myLocation, and the bike is at location u. Move the bike to location v and arrive at "arrivalLocation".
					if u == v:
						totalTimeTaken += pathDistanceMatrix[ myLocation ][ arrivalLocation ]
					else:
						totalTimeTaken += pathDistanceMatrix[ myLocation ][ u ] + bikePathDistanceMatrix[ u ][ v ] + \
						                  pathDistanceMatrix[ v ][ arrivalLocation ]
					timeTaken = min( timeTaken, totalTimeTaken  )
				newTimeTakenWithBikeLocationList.append( timeTaken )
			myLocation = arrivalLocation
			timeTakenWithBikeLocationList = newTimeTakenWithBikeLocationList

		return timeTakenWithBikeLocationList[ myLocation ]

class PedalPowerTest( unittest.TestCase ):
	def test_PedalPower( self ):
		for testfile in getTestFileList( tag='pedalpower' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/pedalpower/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/pedalpower/{}.ans'.format( testfile ) ) as solutionFile:

			locationCount = readInteger( inputFile )
			bikePathCount = readInteger( inputFile )
			bikePaths = [ tuple( readIntegers( inputFile ) ) for _ in range( bikePathCount ) ]
			pathCount = readInteger( inputFile )
			paths = [ tuple( readIntegers( inputFile ) ) for _ in range( pathCount ) ]
			_ = readInteger( inputFile )
			locationList = list( readIntegers( inputFile ) )

			totalTimeTaken = readInteger( solutionFile )

			formatString = 'Testcase {} locationCount = {} [{} {}] totalTimeTaken = {}'
			print( formatString.format( testfile, locationCount, bikePathCount, pathCount, totalTimeTaken ) )
			self.assertEqual( PedalPower( locationCount, bikePaths, paths, locationList ).minimumTime(), totalTimeTaken )

	def test_PedalPower_Sample( self ):
		locationCount = 4
		bikePaths = [ (0, 1, 2), (3, 1, 10), (2, 3, 2), (2, 0, 10) ]
		paths = [ (1, 0, 11), (3, 1, 3), (2, 3, 11), (2, 0, 3) ]
		locationList = [ 1, 3, 2 ]
		self.assertEqual( PedalPower( locationCount, bikePaths, paths, locationList ).minimumTime(), 16 )

'''
Baltic Olympiad in Informatics 2010
Practice Task : grid
BOI_2010_Practice_Letter_Grid
'''

class LetterGrid:
	def __init__( self, letterGrid, word ):
		self.rows, self.cols = len( letterGrid ), len( letterGrid[ 0 ] )
		self.letterGrid = letterGrid
		self.word = word
		self.maximumWordLength = 100

	def count( self ):
		adjacentCellDelta = list()
		for u, v in itertools.product( [ -1, 0, 1 ], [ -1, 0, 1 ] ):
			if u == v == 0:
				continue
			adjacentCellDelta.append( (u, v) )

		table = [ [ [ 0 for _ in range( self.cols ) ] for _ in range( self.rows ) ] for _ in range( 2 ) ]
		
		for index in range( len( self.word ) ):
			for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
				currentIndex = index & 0x1
				previousIndex = currentIndex ^ 0x1

				if index == 0:
					table[ currentIndex ][ u ][ v ] = 1 if self.word[ index ] == self.letterGrid[ u ][ v ] else 0
					continue

				if self.word[ index ] != self.letterGrid[ u ][ v ]:
					table[ currentIndex ][ u ][ v ] = 0
					continue

				ways = 0
				for du, dv in adjacentCellDelta:
					x, y = u + du, v + dv
					if 0 <= x < self.rows and 0 <= y < self.cols:
						ways += table[ previousIndex ][ x ][ y ]
				table[ currentIndex ][ u ][ v ] = ways

		tableIndex = ( len( self.word ) - 1 ) & 0x1
		totalWays = 0
		for u, v in itertools.product( range( self.rows ), range( self.cols ) ):
			totalWays += table[ tableIndex ][ u ][ v ]
		return totalWays

class LetterGridTest( unittest.TestCase ):
	def test_LetterGrid( self ):
		for testfile in getTestFileList( tag='lettergrid' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests/lettergrid/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests/lettergrid/{}.out'.format( testfile ) ) as solutionFile:

			rows, cols, wordLength = readIntegers( inputFile )
			letterGrid = [ readString( inputFile ) for _ in range( rows ) ]
			word = readString( inputFile )
			count = readInteger( solutionFile )

			print( 'Testcase {} rows = {} cols = {} count = {}'.format( testfile, rows, cols, count ) )
			self.assertEqual( LetterGrid( letterGrid, word ).count(), count )

	def test_LetterGrid_Sample( self ):
		letterGrid = [
		'ERAT',
		'ATSR',
		'AUTU'
		]
		word = 'TARTU'
		self.assertEqual( LetterGrid( letterGrid, word ).count(), 7 )

		letterGrid = [
		'AA',
		'AA'
		]
		word = 'AAAAAAAAAA'
		self.assertEqual( LetterGrid( letterGrid, word ).count(), 78732 )

class SolutionManager:
	def __init__( self ):
		self.totalSolutions = 0
		self.contestDict = dict()
		self.testClassSet = set()

	def registerSolution( self, contestName, problemName, testClass, tagList ):
		if contestName not in self.contestDict:
			self.contestDict[ contestName ] = dict()
		assert problemName not in self.contestDict[ contestName ]
		assert testClass not in self.testClassSet
		self.contestDict[ contestName ][ problemName ] = testClass
		self.testClassSet.add( testClass )
		self.totalSolutions += 1

	def printSummary( self ):
		for contestName, problemDict in self.contestDict.items():
			print( '{} : {}'.format( contestName, len( problemDict ) ) )
		print( 'Total : {}'.format( self.totalSolutions ) )

def registerContestSolutions( solutionManager ):
	solutionManager.registerSolution( 'Nordic Collegiate Programming Contest 2015', 'Entertainment Box', EntertainmentBoxTest,
		                              [ 'Greedy' ] )
	
	solutionManager.registerSolution( 'Nordic Collegiate Programming Contest 2015', 'Disastrous Downtime', DisastrousDowntimeTest,
		                              [ 'Greedy' ] )
	
	solutionManager.registerSolution( 'Benelux Algorithm Programming Contest 2014', 'Button Bashing', ButtonBashingTest,
		                              [ 'Graph-BFS' ] )
	
	solutionManager.registerSolution( 'East Central North America Regional Contest 2019', 'Square Rooms', SquareRoomsTest,
		                              [ 'Backtracking' ] )
	
	solutionManager.registerSolution( 'German Collegiate Programming Contest 2012', 'Treasure Diving', TreasureDivingTest,
		                              [ 'Graph-ShortestPath' ] )
	
	solutionManager.registerSolution( 'Benelux Algorithm Programming Contest 2014 Preliminaries', 'Failing Components', FailingComponentsTest,
		                              [ 'Graph-Property' ] )
	
	solutionManager.registerSolution( 'Nordic Collegiate Programming Contest 2014', 'Amanda Lounges', AmandaLoungesTest,
		                              [ 'Graph-Property' ] )
	
	solutionManager.registerSolution( 'Nordic Collegiate Programming Contest 2014', 'Train Passengers', TrainPassengersTest, [ ] )

	solutionManager.registerSolution( 'North Central North America Regional Contest 2020', 'Substring Characters', SubstringCharactersTest, [ ] )

	solutionManager.registerSolution( 'East Central North America Regional Contest 2015', 'Rings', RingsTest, [ 'Graph-FloodFill' ] )

	solutionManager.registerSolution( 'East Central North America Regional Contest 2015', 'Squawk Virus', SquawkVirusTest, [ ] )

	solutionManager.registerSolution( 'East Central North America Regional Contest 2018', 'Car Vet', CarVetTest, [ 'Backtracking' ] )

	solutionManager.registerSolution( 'North Central North America Regional Contest 2020', 'LogDB', LogDBTest, [ ] )

	solutionManager.registerSolution( 'The UK & Ireland Programming Contest 2020', 'Family Fares', FamilyFaresTest, [ ] )

	solutionManager.registerSolution( 'Virginia Tech High School Programming Contest 2018', 'Pedal Power', PedalPowerTest,
	                                  [ 'Graph-ShortestPath', 'Dynamic Programming' ] )

	solutionManager.registerSolution( 'Baltic Olympiad in Informatics 2010', 'Letter Grid', LetterGridTest, [] )

def registerUVaPracticeSolutions( solutionManager ):
	solutionManager.registerSolution( 'UVa Practice', 'Stacking Boxes', StackingBoxesTest, [ 'Graph-TopologicalSort' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Project Scheduling', ProjectSchedulingTest, [ 'Graph-TopologicalSort' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Hippity Hopscotch', HippityHopscotchTest, [ 'Graph-TopologicalSort' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Longest Run on a Snowboard', LongestRunTest, [ 'Graph-TopologicalSort' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Walking Around Wisely', WalkingAroundWiselyTest, [ 'Graph-TopologicalSort' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Rare Order', RareOrderTest, [ 'Graph-TopologicalSort' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Almost Shortest Path', AlmostShortestPathTest, [ 'Graph-ShortestPath' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Route Change', RouteChangeTest, [ 'Graph-SingleSourceShortestPath' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Patrol Robot', PatrolRobotTest, [ 'Graph-BFS' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Letters', LettersTest, [ 'Graph-BFS', 'Bitmap' ] )
	
	solutionManager.registerSolution( 'UVa Practice', 'Eternal Truths', EternalTruthsTest, [ 'Graph-BFS', 'State Space Search' ] )

	solutionManager.registerSolution( 'UVa Practice', 'Degrees of Separation', DegreesofSeparationTest, [ 'Graph-ShortestPath' ] )

	solutionManager.registerSolution( 'UVa Practice', 'Asterix And Obelix', AsterixTest, [ 'Graph-ShortestPath' ] )

class TopologicalSort:
	@staticmethod
	def toposort( graph, vertexCount ):
		visited = [ False for _ in range( vertexCount ) ]
		toposortList = list()

		def _dfs( graph, vertex, visited, toposortList ):
			stack = list()
			stack.append( (vertex, 'VISIT') )
			
			while len( stack ) > 0:
				currentVertex, command = stack.pop()
				if command == 'VISIT' and not visited[ currentVertex ]:
					stack.append( (currentVertex, 'POP') )
					for adjacentVertex in graph[ currentVertex ]:
						stack.append( (adjacentVertex, 'VISIT') )
				elif command == 'POP':
					visited[ currentVertex ] = True
					toposortList.append( currentVertex )

		for vertex in range( vertexCount ):
			_dfs( graph, vertex, visited, toposortList )
		toposortList.reverse()
		return toposortList

'''
UVa_103_StackingBoxes
'''

class StackingBoxes:
	def __init__( self, dimension, boxList ):
		self.dimension = dimension
		self.boxList = boxList

	def analyze( self ):
		'''
		If the dimension is 1, we just have to sort the unique boxes.
		'''
		if self.dimension == 1:
			boxSet = set()
			largestStackBox = list()
			for index, box in enumerate( self.boxList ):
				boxId = box.pop()
				if boxId not in boxSet:
					boxSet.add( boxId )
					largestStackBox.append( (boxId, index + 1) )
			largestStackBox.sort()
			return [ position for _, position in largestStackBox ]

		boxes = len( self.boxList )
		graph = [ list() for _ in range( boxes ) ]
		for boxDimensions in self.boxList:
			boxDimensions.sort()
		for i, j in itertools.product( range( boxes ), range( boxes ) ):
			if i == j:
				continue
			addEdge = True
			boxA = self.boxList[ i ]
			boxB = self.boxList[ j ]
			for k in range( self.dimension ):
				if boxA[ k ] >= boxB[ k ]:
					addEdge = False
					break
			if addEdge:
				# Box at index i can be placed before the box at index j.
				graph[ i ].append( j )

		pathLengthList = [ 0 for _ in range( boxes ) ]
		pathList = [ None for _ in range( boxes ) ]
		
		largestStackSize = 0
		largestStackBox = 0

		for boxA in TopologicalSort.toposort( graph, boxes ):
			for boxB in graph[ boxA ]:
				currentPathLength = pathLengthList[ boxB ]
				possiblePathLength = pathLengthList[ boxA ] + 1
				if possiblePathLength > currentPathLength:
					pathLengthList[ boxB ] = possiblePathLength
					pathList[ boxB ] = boxA

					if possiblePathLength > largestStackSize:
						largestStackSize = possiblePathLength
						largestStackBox = boxB
		
		largestStack = list()
		while largestStackBox is not None:
			position = largestStackBox + 1
			largestStack.append( position )
			largestStackBox = pathList[ largestStackBox ]
		largestStack.reverse()
		return largestStack

class StackingBoxesTest( unittest.TestCase ):
	def test_StackingBoxes( self ):
		for testfile in getTestFileList( tag='stackingboxes', basepath='tests-practice' ):
			self._verify( testfile )

	def _verifyStack( self, stack, boxList ):
		for index in range( 1, len( stack ) ):
			# Verify that, for each pair of consecutive boxes i and j, box j can be stacked on top of box i. 
			i, j = stack[ index - 1 ] - 1, stack[ index ] - 1
			for length1, length2 in zip( sorted( boxList[ i ] ), sorted( boxList[ j ] ) ):
				self.assertLess( length1, length2 )

	def _verify( self, testfile ):
		with open( 'tests-practice/stackingboxes/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/stackingboxes/{}.ans'.format( testfile ) ) as solutionFile:

			while True:
				inputLine = readString( inputFile )
				if len( inputLine ) == 0:
					break
				boxes, dimension = map( int, inputLine.split() )
				boxList = [ list( readIntegers( inputFile ) ) for _ in range( boxes ) ]

				_ = readInteger( solutionFile )
				largestStack = list( readIntegers( solutionFile ) )

				print( 'Testcase {} boxes = {} dimension = {}'.format( testfile, boxes, dimension ) )
				stack = StackingBoxes( dimension, boxList ).analyze()
				self.assertEqual( len( stack ), len( largestStack ) )
				if stack != largestStack:
					self._verifyStack( stack, boxList )
					self._verifyStack( largestStack, boxList )

	def test_StackingBoxes_Sample( self ):
		dimension = 2
		boxList = [ [3, 7], [8, 10], [5, 2], [9, 11], [21, 18] ]
		self.assertEqual( StackingBoxes( dimension, boxList ).analyze(), [ 3, 1, 2, 4, 5 ] )

		dimension = 6
		boxList = [
		[  5,  2, 20,  1, 30, 10 ],
		[ 23, 15,  7,  9, 11,  3 ],
		[ 40, 50, 34, 24, 14,  4 ],
		[  9, 10, 11, 12, 13, 14 ],
		[ 31,  4, 18,  8, 27, 17 ],
		[ 44, 32, 13, 19, 41, 19 ],
		[  1,  2,  3,  4,  5,  6 ],
		[ 80, 37, 47, 18, 21,  9 ],
		]
		self.assertEqual( StackingBoxes( dimension, boxList ).analyze(), [ 7, 2, 5, 6 ] )

'''
UVa_452_ProjectScheduling
'''
class ProjectScheduling:
	def __init__( self, edgeInfoList ):
		self.graph = [ list() for _ in range( len( string.ascii_uppercase ) ) ]
		self.timeTaken = [ 0 for _ in range( len( string.ascii_uppercase ) ) ]
		
		def getIndex( character ):
			return ord( character ) - ord( 'A' )
		for taskLabel, timeTaken, dependentTaskLabels in edgeInfoList:
			for dependentTaskLabel in dependentTaskLabels:
				self.graph[ getIndex( dependentTaskLabel ) ].append( getIndex( taskLabel ) )
			self.timeTaken[ getIndex( taskLabel ) ] = timeTaken

	def completionTime( self ):
		taskList = TopologicalSort.toposort( self.graph, len( self.graph ) )
		totalTimeTaken = [ 0 for _ in range( len( string.ascii_uppercase ) ) ]
		timeForCompletion = 0

		for task in taskList:
			totalTimeTaken[ task ] += self.timeTaken[ task ]
			timeForCompletion = max( timeForCompletion, totalTimeTaken[ task ] )
			for dependentTask in self.graph[ task ]:
				totalTimeTaken[ dependentTask ] = max( totalTimeTaken[ dependentTask ], totalTimeTaken[ task ] )
		return timeForCompletion

class ProjectSchedulingTest( unittest.TestCase ):
	def test_ProjectScheduling( self ):
		for testfile in getTestFileList( tag='projectscheduling', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/projectscheduling/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/projectscheduling/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			readString( inputFile )
			for i in range( testcaseCount ):
				edgeInfoList = list()
				while True:
					edgeInfoString = readString( inputFile )
					if len( edgeInfoString ) == 0:
						break
					tokens = edgeInfoString.split()
					if len( tokens ) == 2:
						tokens.append( str() )
					task, timeTaken, dependentTasks = tokens
					edgeInfoList.append( (task, int( timeTaken ), dependentTasks) )

				timeForCompletion = readInteger( solutionFile )
				readString( solutionFile )

				print( 'Testcase {}#{} timeForCompletion = {}'.format( testfile, i + 1, timeForCompletion) )
				self.assertEqual( ProjectScheduling( edgeInfoList ).completionTime(), timeForCompletion )

	def test_ProjectScheduling_Sample( self ):
		edgeInfoList = [
		('A', 5, ''  ),
		('B', 3, 'A' ),
		('D', 2, 'A' ),
		('C', 2, 'B' ),
		('F', 2, 'CE'),
		('E', 4, 'DC')
		]
		self.assertEqual( ProjectScheduling( edgeInfoList ).completionTime(), 16 )

'''
UVa_10259_HippityHopscotch
'''
class HippityHopscotch:
	def __init__( self, size, k, areaLayout ):
		self.size, self.k = size, k
		self.areaLayout = areaLayout

		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

	def go( self ):
		table = [ [ 0 for _ in range( self.size ) ] for _ in range( self.size ) ]
		reachable = [ [ False for _ in range( self.size ) ] for _ in range( self.size ) ]

		toposortList = list()
		for i, j in itertools.product( range( self.size ), range( self.size ) ):
			toposortList.append( (self.areaLayout[ i ][ j ], i, j) )

		toposortList.sort()
		coinList = [ coins for coins, _, _ in toposortList ]

		coinsAtStartLocation = self.areaLayout[ 0 ][ 0 ]
		reachable[ 0 ][ 0 ] = True
		
		maximumCoins = 0
		for index in range( bisect.bisect_left( coinList, coinsAtStartLocation ), len( toposortList ) ):
			_, i, j = toposortList[ index ]
			if not reachable[ i ][ j ]:
				continue
			table[ i ][ j ] += self.areaLayout[ i ][ j ]
			maximumCoins = max( maximumCoins, table[ i ][ j ] )
			for du, dv in self.adjacentCellDelta:
				for k in range( 1, self.k + 1 ):
					x, y = i + du * k, j + dv * k
					if 0 <= x < self.size and 0 <= y < self.size and self.areaLayout[ x ][ y ] > self.areaLayout[ i ][ j ]:
						table[ x ][ y ] = max( table[ x ][ y ], table[ i ][ j ] )
						reachable[ x ][ y ] = True
		return maximumCoins

class HippityHopscotchTest( unittest.TestCase ):
	def test_HippityHopscotch( self ):
		for testfile in getTestFileList( tag='hippityhopscotch', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/hippityhopscotch/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/hippityhopscotch/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				readString( inputFile )
				size, k = readIntegers( inputFile )
				areaLayout = [ list( readIntegers( inputFile ) ) for _ in range( size ) ]

				maximumCoins = readInteger( solutionFile )
				readString( solutionFile )

				print( 'Testcase {}#{} size = {} k = {} maximumCoins = {}'.format( testfile, i + 1, size, k, maximumCoins ) )
				self.assertEqual( HippityHopscotch( size, k, areaLayout ).go(), maximumCoins )

	def test_HippityHopscotch_Sample( self ):
		size, k = 3, 1
		areaLayout = [
		[ 1, 2, 5 ],
		[ 10, 11, 6 ],
		[ 12, 12, 7 ]
		]
		self.assertEqual( HippityHopscotch( size, k, areaLayout ).go(), 37 )

'''
UVa_10285_LongestRunOnASnowboard
'''
class LongestRun:
	def __init__( self, rows, cols, areaLayout ):
		self.rows, self.cols = rows, cols
		self.areaLayout = areaLayout

		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

	def longestRun( self ):
		table = [ [ 0 for _ in range( self.cols ) ] for _ in range( self.rows ) ]

		toposortList = list()
		for i, j in itertools.product( range( self.rows ), range( self.cols ) ):
			toposortList.append( (self.areaLayout[ i ][ j ], i, j) )
		toposortList.sort( reverse=True )

		longestRunLength = 0
		for _, i, j in toposortList:
			for du, dv in self.adjacentCellDelta:
				x, y = i + du, j + dv
				if 0 <= x < self.rows and 0 <= y < self.cols and self.areaLayout[ x ][ y ] < self.areaLayout[ i ][ j ]:
					table[ x ][ y ] = max( table[ x ][ y ], table[ i ][ j ] + 1 )
					longestRunLength = max( longestRunLength, table[ x ][ y ] )
		return longestRunLength + 1

class LongestRunTest( unittest.TestCase ):
	def test_LongestRun( self ):
		for testfile in getTestFileList( tag='longestrun', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/longestrun/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/longestrun/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				tag, rows, cols = readString( inputFile ).split()
				rows, cols = int( rows ), int( cols )
				areaLayout = [ list( readIntegers( inputFile ) ) for _ in range( rows ) ]

				longestRunString = readString( solutionFile )
				longestRunLength = LongestRun( rows, cols, areaLayout ).longestRun()

				print( 'Testcase {}#{} rows = {} cols = {} [{}]'.format( testfile, i + 1, rows, cols, longestRunString ) )
				self.assertEqual( '{}: {}'.format( tag, longestRunLength ), longestRunString )

	def test_LongestRun_Sample( self ):
		rows, cols = 10, 5
		areaLayoutStringList = [
		'56 14 51 58 88',
		'26 94 24 39 41',
		'24 16 8 51 51',
		'76 72 77 43 10',
		'38 50 59 84 81',
		'5 23 37 71 77',
		'96 10 93 53 82',
		'94 15 96 69 9',
		'74 0 62 38 96',
		'37 54 55 82 38'
		]
		areaLayout = [ list( map( int, areaLayoutStringRow.split() ) ) for areaLayoutStringRow in areaLayoutStringList ]
		self.assertEqual( LongestRun( rows, cols, areaLayout ).longestRun(), 7 )

		rows, cols = 5, 5
		areaLayoutStringList = [
		'1 2 3 4 5',
		'16 17 18 19 6',
		'15 24 25 20 7',
		'14 23 22 21 8',
		'13 12 11 10 9'
		]
		areaLayout = [ list( map( int, areaLayoutStringRow.split() ) ) for areaLayoutStringRow in areaLayoutStringList ]
		self.assertEqual( LongestRun( rows, cols, areaLayout ).longestRun(), 25 )

'''
UVa_926_WalkingAroundWisely
'''
class WalkingAroundWisely:
	def __init__( self, size, startLocation, targetLocation, blockedLocationsInfo ):
		self.size = size
		self.startLocation, self.targetLocation = startLocation, targetLocation
		self.blockedLocations = set( blockedLocationsInfo )

	def numberOfWays( self ):
		ways = [ [ 0 for _ in range( self.size + 1 ) ] for _ in range( self.size + 1 ) ]

		x, y = self.startLocation
		u, v = self.targetLocation

		for i in range( x, u + 1 ):
			for j in range( y, v + 1 ):
				wayFromSouthBlocked = (i, j, 'S') in self.blockedLocations or (i, j - 1, 'N') in self.blockedLocations
				wayFromWestBlocked  = (i, j, 'W') in self.blockedLocations or (i - 1, j, 'E') in self.blockedLocations
				
				waysFromSouth = 0 if wayFromSouthBlocked else ways[ i ][ j - 1 ]
				waysFromWest =  0 if wayFromWestBlocked  else ways[ i - 1 ][ j ]
				if i == x and j == y:
					ways[ i ][ j ] = 1
					continue
				ways[ i ][ j ] = waysFromSouth + waysFromWest
		return ways[ u ][ v ]

class WalkingAroundWiselyTest( unittest.TestCase ):
	def test_WalkingAroundWisely( self ):
		for testfile in getTestFileList( tag='walkingaround', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/walkingaround/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/walkingaround/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				size = readInteger( inputFile )
				startLocation = tuple( readIntegers( inputFile ) )
				targetLocation = tuple( readIntegers( inputFile ) )

				blockedCount = readInteger( inputFile )
				blockedLocationsInfo = list()
				for _ in range( blockedCount ):
					x, y, direction = readString( inputFile ).split()
					blockedLocationsInfo.append( (int( x ), int( y ), direction) )

				numberOfWays = readInteger( solutionFile )

				formatString = 'Testcase {}#{} size = {} startLocation = {} targetLocation = {} numberOfWays = {}'
				print( formatString.format( testfile, i + 1, size, startLocation, targetLocation, numberOfWays ) )
				self.assertEqual( WalkingAroundWisely( size, startLocation, targetLocation, blockedLocationsInfo ).numberOfWays(), numberOfWays )

	def test_WalkingAroundWisely_Sample( self ):
		size = 3
		startLocation, targetLocation = (1, 1), (3, 3)
		blockedLocationsInfo = [ (2, 3, 'S'), (2, 2, 'W') ]
		self.assertEqual( WalkingAroundWisely( size, startLocation, targetLocation, blockedLocationsInfo ).numberOfWays(), 3 )

		size = 3
		startLocation, targetLocation = (1, 1), (3, 3)
		blockedLocationsInfo = [ ]
		self.assertEqual( WalkingAroundWisely( size, startLocation, targetLocation, blockedLocationsInfo ).numberOfWays(), 6 )

'''
UVa_200_RareOrder
'''
class RareOrder:
	def __init__( self, stringList ):
		self.stringList = stringList

	def order( self ):
		graph = list()	
		characterToVertexIdDict = dict()
		vertexIdToCharacterDict = dict()
		nextVertexId = 0

		def getVertexId( character ):
			nonlocal graph
			nonlocal characterToVertexIdDict
			nonlocal nextVertexId
			if character not in characterToVertexIdDict:
				graph.append( list() )
				characterToVertexIdDict[ character ] = nextVertexId
				vertexIdToCharacterDict[ nextVertexId ] = character
				nextVertexId += 1
			return characterToVertexIdDict[ character ]

		if len( self.stringList ) > 0:
			character, * _ = self.stringList[ 0 ]
			graph[ getVertexId( character ) ]

		for i in range( 1, len( self.stringList ) ):
			stringA, stringB = self.stringList[ i - 1 ], self.stringList[ i ]
			for character1, character2 in zip( stringA, stringB ):
				if character1 != character2:
					graph[ getVertexId( character1 ) ].append( getVertexId( character2 ) )
					break

		toposortList = TopologicalSort.toposort( graph, len( graph ) )
		return ''.join( [ vertexIdToCharacterDict[ vertexId ] for vertexId in toposortList ] )

class RareOrderTest( unittest.TestCase ):
	def test_RareOrder( self ):
		for testfile in getTestFileList( tag='rareorder', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/rareorder/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/rareorder/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = 0
			stringList = list()
			for inputLine in readAllStrings( inputFile ):
				if len( inputLine ) == 0:
					continue
				if inputLine == '#':
					testcaseCount += 1

					orderedLetters = readString( solutionFile )

					print( 'Testcase {}#{} orderedLetters = {}'.format( testfile, testcaseCount, orderedLetters ) )
					self.assertEqual( RareOrder( stringList ).order(), orderedLetters )

					stringList.clear()
				else:
					stringList.append( inputLine )

	def test_RareOrder_Sample( self ):
		stringList = [ 'XWY', 'ZX', 'ZXY', 'ZXW', 'YWWX' ]
		self.assertEqual( RareOrder( stringList ).order(), 'XZYW' )

'''
UVa_12144_AlmostShortestPath
'''
class AlmostShortestPath:
	def __init__( self, vertexCount, edgeList, sourceVertex, targetVertex ):
		self.vertexCount = vertexCount
		self.edgeList = edgeList
		self.graph = [ list() for _ in range( vertexCount ) ]
		for u, v, distance in edgeList:
			self.graph[ u ].append( (v, distance) )
		self.sourceVertex, self.targetVertex = sourceVertex, targetVertex

	def almost( self ):
		distanceList, pathList = ShortestPaths.singleSourceShortestPath( self.graph, self.sourceVertex )

		if distanceList[ self.targetVertex ] == float( 'inf' ):
			return -1

		def _dfs( u, v, disableEdges ):
			if v is not None:
				disableEdges.add( (u, v) )
			for edge in pathList[ u ]:
				_dfs( edge, u, disableEdges )

		disableEdges = set()
		_dfs( self.targetVertex, None, disableEdges )

		distanceList, _ = ShortestPaths.singleSourceShortestPath( self.graph, self.sourceVertex, disableEdges=disableEdges )
		return distanceList[ self.targetVertex ] if distanceList[ self.targetVertex ] != float( 'inf' ) else -1

class AlmostShortestPathTest( unittest.TestCase ):
	def test_AlmostShortestPath( self ):
		for testfile in getTestFileList( tag='almost', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/almost/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/almost/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = 0
			while True:
				vertexCount, edgeCount = readIntegers( inputFile )
				if vertexCount == edgeCount == 0:
					break
				testcaseCount += 1

				sourceVertex, targetVertex = readIntegers( inputFile )
				edgeList = [ tuple( readIntegers( inputFile ) ) for _ in range( edgeCount ) ]
				almostShortestPath = readInteger( solutionFile )

				formatString = 'Testcase {}#{} vertexCount = {} edgeCount = {} almostShortestPath = {}'
				print( formatString.format( testfile, testcaseCount, vertexCount, edgeCount, almostShortestPath ) )
				self.assertEqual( AlmostShortestPath( vertexCount, edgeList, sourceVertex, targetVertex ).almost(), almostShortestPath )

	def test_AlmostShortestPath_Sample( self ):
		vertexCount = 7
		edgeList = [ (0, 1, 1), (0, 2, 1), (0, 3, 2), (0, 4, 3), (1, 5, 2), (2, 6, 4), (3, 6, 2), (4, 6, 4), (5, 6, 1) ]
		sourceVertex, targetVertex = 0, 6
		self.assertEqual( AlmostShortestPath( vertexCount, edgeList, sourceVertex, targetVertex ).almost(), 5 )

		vertexCount = 4
		edgeList = [ (0, 1, 1), (1, 2, 1), (1, 3, 1), (3, 2, 1), (2, 0, 3), (3, 0, 2) ]
		sourceVertex, targetVertex = 0, 2
		self.assertEqual( AlmostShortestPath( vertexCount, edgeList, sourceVertex, targetVertex ).almost(), -1 )

		vertexCount = 6
		edgeList = [ (0, 1, 1), (0, 2, 2), (0, 3, 3), (2, 5, 3), (3, 4, 2), (4, 1, 1), (5, 1, 1), (3, 0, 1) ]
		sourceVertex, targetVertex = 0, 1
		self.assertEqual( AlmostShortestPath( vertexCount, edgeList, sourceVertex, targetVertex ).almost(), 6 )

'''
UVa_11833_RouteChange
'''
class RouteChange:
	def __init__( self, cities, citiesInServiceRoad, repairCity, roadNetwork ):
		self.cities = cities
		self.citiesInServiceRoad = citiesInServiceRoad
		self.repairCity = repairCity
		self.graph = [ list() for _ in range( cities ) ]
		for u, v, cost in roadNetwork:
			self.graph[ u ].append( (v, cost) )
			self.graph[ v ].append( (u, cost) )

	def go( self ):
		startCity, targetCity = 0, self.citiesInServiceRoad - 1
		serviceDistance = list()
		for city in range( startCity, targetCity ):
			for adjacentCity, cost in self.graph[ city ]:
				if adjacentCity == city + 1:
					break
			serviceDistance.append( cost )
		i = len( serviceDistance ) - 1
		while i > 0:
			serviceDistance[ i - 1 ] += serviceDistance[ i ]
			i = i - 1

		distanceList = [ float( 'inf' ) for _ in range( self.cities ) ]
		distanceList[ self.repairCity ] = 0

		q = list()
		q.append( (0, self.repairCity) )

		while len( q ) > 0:
			cost, currentCity = heapq.heappop( q )
			if currentCity == targetCity:
				return cost
			if cost > distanceList[ currentCity ]:
				continue

			if 0 <= currentCity < len( serviceDistance ):
				totalCost = cost + serviceDistance[ currentCity ]
				heapq.heappush( q, (totalCost, targetCity) )
				continue

			for adjacentCity, additionalCost in self.graph[ currentCity ]:
				totalCost = cost + additionalCost
				if totalCost < distanceList[ adjacentCity ]:
					distanceList[ adjacentCity ] = totalCost
					heapq.heappush( q, (totalCost, adjacentCity) ) 

class RouteChangeTest( unittest.TestCase ):
	def test_RouteChange( self ):
		for testfile in getTestFileList( tag='routechange', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/routechange/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/routechange/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = 0
			while True:
				cities, roads, citiesInServiceRoad, repairCity = readIntegers( inputFile )
				if cities == roads == 0:
					break
				testcaseCount += 1

				roadNetwork = [ tuple( readIntegers( inputFile ) ) for _ in range( roads ) ]
				cost = readInteger( solutionFile )

				print( 'Testcase {}#{} cities = {} roads = {} cost = {}'.format( testfile, testcaseCount, cities, roads, cost ) )
				self.assertEqual( RouteChange( cities, citiesInServiceRoad, repairCity, roadNetwork ).go(), cost )

	def test_RouteChange_Sample( self ):
		cities, citiesInServiceRoad, repairCity = 4, 3, 3
		roadNetwork = [ (0, 1, 10), (1, 2, 10), (0, 2, 1), (3, 0, 1), (3, 1, 10), (3, 2, 10) ]
		self.assertEqual( RouteChange( cities, citiesInServiceRoad, repairCity, roadNetwork ).go(), 10 )

		cities, citiesInServiceRoad, repairCity = 6, 2, 5
		roadNetwork = [ (5, 2, 1), (2, 1, 10), (1, 0, 1), (3, 0, 2), (3, 4, 2), (3, 5, 3), (5, 4, 2) ]
		self.assertEqual( RouteChange( cities, citiesInServiceRoad, repairCity, roadNetwork ).go(), 6 )

		cities, citiesInServiceRoad, repairCity = 5, 2, 4
		roadNetwork = [ (0, 1, 1), (1, 2, 2), (2, 3, 3), (3, 4, 4), (4, 0, 5) ]
		self.assertEqual( RouteChange( cities, citiesInServiceRoad, repairCity, roadNetwork ).go(), 6 )

'''
UVa_1600_PatrolRobot
'''
class PatrolRobot:
	def __init__( self, rows, cols, areaLayout, k ):
		self.rows, self.cols = rows, cols
		self.areaLayout = areaLayout
		self.k = k
		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]
		self.startLocation, self.targetLocation = (0, 0), (self.rows - 1, self.cols - 1)
		self.emptyCellToken = 0

	def shortestPath( self ):
		visitedState = [ [ [ False for _ in range( self.k + 1 ) ] for _ in range( self.cols ) ] for _ in range( self.rows ) ]

		q = deque()
		x, y = self.startLocation
		q.append( (x, y, 0) )
		visitedState[ x ][ y ][ 0 ] = True

		pathLength = 0
		while len( q ) > 0:
			N = len( q )
			while N > 0:
				N = N - 1

				u, v, k = q.popleft()
				if (u, v) == self.targetLocation:
					return pathLength

				for du, dv in self.adjacentCellDelta:
					x, y = u + du, v + dv
					if not 0 <= x < self.rows or not 0 <= y < self.cols:
						continue
					obstaclesTraversed = 0 if self.areaLayout[ x ][ y ] == self.emptyCellToken else k + 1
					if obstaclesTraversed <= self.k and not visitedState[ x ][ y ][ obstaclesTraversed ]:
						visitedState[ x ][ y ][ obstaclesTraversed ] = True
						q.append( (x, y, obstaclesTraversed) )
			pathLength += 1
		return -1

class PatrolRobotTest( unittest.TestCase ):
	def test_PatrolRobot( self ):
		for testfile in getTestFileList( tag='patrolrobot', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/patrolrobot/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/patrolrobot/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				rows, cols = readIntegers( inputFile )
				k = readInteger( inputFile )
				areaLayout = [ list( readIntegers( inputFile ) ) for _ in range( rows ) ]
				shortestPath = readInteger( solutionFile )

				print( 'Testcase {}#{} rows = {} cols = {} k = {} shortestPath = {}'.format( testfile, i + 1, rows, cols, k, shortestPath ) )
				self.assertEqual( PatrolRobot( rows, cols, areaLayout, k ).shortestPath(), shortestPath )

	def test_PatrolRobot_Sample( self ):
		rows, cols = 2, 5
		k = 0
		areaLayout = [
		[ 0, 1, 0, 0, 0 ],
		[ 0, 0, 0, 1, 0 ]
		]
		self.assertEqual( PatrolRobot( rows, cols, areaLayout, k ).shortestPath(), 7 )

		rows, cols = 4, 6
		k = 1
		areaLayout = [
		[ 0, 1, 1, 0, 0, 0 ],
		[ 0, 0, 1, 0, 1, 1 ],
		[ 0, 1, 1, 1, 1, 0 ],
		[ 0, 1, 1, 1, 0, 0 ]
		]
		self.assertEqual( PatrolRobot( rows, cols, areaLayout, k ).shortestPath(), 10 )

		rows, cols = 2, 2
		k = 0
		areaLayout = [
		[ 0, 1 ],
		[ 1, 0 ]
		]
		self.assertEqual( PatrolRobot( rows, cols, areaLayout, k ).shortestPath(), -1 )

'''
UVa_12797_Letters
'''
class Letters:
	def __init__( self, size, lettersLayout ):
		self.size = size
		self.lettersLayout = lettersLayout
		self.startLocation, self.targetLocation = (0, 0), (self.size - 1, self.size - 1)
		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

	def _shortestPath( self, lettersToBitNumberDict, allowedLowercaseLettersBitmap ):
		visited = [ [ False for _ in range( self.size ) ] for _ in range( self.size ) ]
		pathLength = 0
		x, y = self.startLocation

		def _isAllowed( letter ):
			isBitSet = ( allowedLowercaseLettersBitmap & ( 1 << lettersToBitNumberDict[ letter.lower() ] ) ) > 0
			return ( letter.islower() and isBitSet ) or ( letter.isupper() and not isBitSet )

		q = deque()
		if _isAllowed( self.lettersLayout[ x ][ y ] ):
			visited[ x ][ y ] = True
			q.append( (x, y) )

		while len( q ) > 0:
			N = len( q )
			while N > 0:
				N = N - 1
				
				u, v = currentLocation = q.popleft()
				if currentLocation == self.targetLocation:
					return pathLength + 1 # We need the number of letters in the path; hence adjust the pathLength.
				for du, dv in self.adjacentCellDelta:
					x, y = u + du, v + dv
					if 0 <= x < self.size and 0 <= y < self.size and not visited[ x ][ y ] and _isAllowed( self.lettersLayout[ x ][ y ] ):
						visited[ x ][ y ] = True
						q.append( (x, y) )
			pathLength += 1
		return float( 'inf' )

	def shortestPath( self ):
		uniqueLowercaseLetters = set()
		for lettersLayoutRow in self.lettersLayout:
			for letter in lettersLayoutRow:
				uniqueLowercaseLetters.add( letter.lower() )

		lettersToBitNumberDict = dict()
		nextBitNumber = 0
		for letter in uniqueLowercaseLetters:
			lettersToBitNumberDict[ letter ] = nextBitNumber
			nextBitNumber += 1

		pathLength = float( 'inf' )
		for allowedLowercaseLettersBitmap in range( 1 << len( lettersToBitNumberDict ) ):
			pathLength = min( pathLength, self._shortestPath( lettersToBitNumberDict, allowedLowercaseLettersBitmap ) )
		return -1 if pathLength == float( 'inf' ) else pathLength

class LettersTest( unittest.TestCase ):
	def test_Letters( self ):
		for testfile in getTestFileList( tag='letters', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/letters/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/letters/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = 0
			while True:
				token = readString( inputFile )
				if len( token ) == 0:
					break
				testcaseCount += 1

				size = int( token )
				lettersLayout = [ readString( inputFile ) for _ in range( size ) ]
				shortestPath = readInteger( solutionFile )

				print( 'Testcase {}#{} size = {} shortestPath = {}'.format( testfile, testcaseCount, size, shortestPath ) )
				self.assertEqual( Letters( size, lettersLayout ).shortestPath(), shortestPath )

	def test_Letters_Sample( self ):
		size = 6
		lettersLayout = [
		'DdaAaA',
		'CBAcca',
		'eEaeeE',
		'bBbabB',
		'DbDdDc',
		'fFaAaC'
		]
		self.assertEqual( Letters( size, lettersLayout ).shortestPath(), 13 )

		size = 7
		lettersLayout = [
		'aAaaaaa',
		'aAaaaAa',
		'aAaaaAA',
		'aaAaAaa',
		'AaAaaAa',
		'aaAAaAa',
		'aaaaaAa'
		]
		self.assertEqual( Letters( size, lettersLayout ).shortestPath(), -1 )

'''
UVa_928_EternalTruths
'''
class EternalTruths:
	def __init__( self, rows, cols, areaLayout ):
		self.rows, self.cols = rows, cols
		self.areaLayout = areaLayout
		self.startCellToken, self.targetCellToken = 'SE'
		self.emptyCell, self.blockedCell = '.#'

		self.adjacentCellDelta = [ (0, 1), (0, -1), (1, 0), (-1, 0) ]

	def shortestPath( self ):
		maximumSteps = 3
		visited = [ [ [ False for _ in range( maximumSteps + 1 ) ] for _ in range( self.cols ) ] for _ in range( self.rows ) ]

		startLocation = None
		for i, j in itertools.product( range( self.rows ), range( self.cols ) ):
			if self.areaLayout[ i ][ j ] == self.startCellToken:
				startLocation = i, j
				break

		q = deque()
		pathLength = 0

		x, y = startLocation
		startingStep = 1
		visited[ x ][ y ][ startingStep ] = True
		q.append( (x, y, startingStep) )

		while len( q ) > 0:
			N = len( q )
			while N > 0:
				N = N - 1

				u, v, step = q.popleft()
				if self.areaLayout[ u ][ v ] == self.targetCellToken:
					return pathLength
				nextStep = startingStep if step == maximumSteps else step + 1
				for du, dv in self.adjacentCellDelta:
					x, y = u + step * du, v + step * dv
					if not 0 <= x < self.rows or not 0 <= y < self.cols:
						continue
					blockingCellPresent = False
					for s in range( 1, step + 1 ):
						r, c = u + s * du, v + s * dv
						if self.areaLayout[ r ][ c ] == self.blockedCell:
							blockingCellPresent = True
							break
					if not blockingCellPresent and not visited[ x ][ y ][ nextStep ]:
						visited[ x ][ y ][ nextStep ] = True
						q.append( (x, y, nextStep) )
			pathLength += 1
		return 'NO'

class EternalTruthsTest( unittest.TestCase ):
	def test_EternalTruths( self ):
		for testfile in getTestFileList( tag='eternal', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/eternal/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/eternal/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = readInteger( inputFile )
			for i in range( testcaseCount ):
				rows, cols = readIntegers( inputFile )
				areaLayout = [ readString( inputFile ) for _ in range( rows ) ]
				shortestPath = readString( solutionFile )
				if shortestPath != 'NO':
					shortestPath = int( shortestPath )

				print( 'Testcase {}#{} rows = {} cols = {} shortestPath = [{}]'.format( testfile, i + 1, rows, cols, shortestPath ) )
				self.assertEqual( EternalTruths( rows, cols, areaLayout ).shortestPath(), shortestPath )

	def test_EternalTruths_Sample( self ):
		rows, cols = 5, 4
		areaLayout = [
		'S...',
		'.#.#',
		'.#..',
		'.##.',
		'...E'
		]
		self.assertEqual( EternalTruths( rows, cols, areaLayout ).shortestPath(), 'NO' )

		rows, cols = 6, 6
		areaLayout = [
		'.S...E',
		'.#.##.',
		'.#....',
		'.#.##.',
		'.####.',
		'......'
		]
		self.assertEqual( EternalTruths( rows, cols, areaLayout ).shortestPath(), 3 )

'''
UVa_1056_DegreesofSeparation
'''
class DegreesofSeparation:
	def __init__( self, vertices, edgeList ):
		self.vertices = vertices
		self.adjacencyMatrix = [ [ float( 'inf' ) for _ in range( vertices ) ] for _ in range( vertices ) ]
		
		vertexId = 0
		self.nameToVertexIdDict = dict()
		for A, B in edgeList:
			if A not in self.nameToVertexIdDict:
				self.nameToVertexIdDict[ A ] = vertexId
				vertexId += 1
			if B not in self.nameToVertexIdDict:
				self.nameToVertexIdDict[ B ] = vertexId
				vertexId += 1
			u, v = self.nameToVertexIdDict[ A ], self.nameToVertexIdDict[ B ]
			self.adjacencyMatrix[ u ][ v ] = self.adjacencyMatrix[ v ][ u ] = 1

	def analyze( self ):
		pathMatrix = ShortestPaths.allPathsShortestPath( self.adjacencyMatrix )
		maximumSeparation = 0
		for u in range( self.vertices ):
			for v in range( self.vertices ):
				if u == v:
					continue
				maximumSeparation = max( maximumSeparation, pathMatrix[ u ][ v ] )
		return maximumSeparation if maximumSeparation != float( 'inf' ) else 'DISCONNECTED'

class DegreesofSeparationTest( unittest.TestCase ):
	def test_DegreesofSeparation( self ):
		for testfile in getTestFileList( tag='separation', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/separation/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/separation/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = 0
			while True:
				vertices, edges = readIntegers( inputFile )
				if vertices == edges == 0:
					break
				testcaseCount += 1

				edgeList = [ tuple( readStrings( inputFile ) ) for _ in range( edges ) ]
				state = readString( solutionFile )
				readString( solutionFile )

				print( 'Testcase {}#{} vertices = {} edges = {} [{}]'.format( testfile, testcaseCount, vertices, edges, state ) )
				maximumSeparation = DegreesofSeparation( vertices, edgeList ).analyze()
				self.assertEqual( 'Network {}: {}'.format( testcaseCount, maximumSeparation ), state )

	def test_DegreesofSeparation_Sample( self ):
		vertices = 4
		edgeList = [ ('Ashok', 'Kiyoshi'), ('Ursala', 'Chun'), ('Ursala', 'Kiyoshi'), ('Kiyoshi', 'Chun') ]
		self.assertEqual( DegreesofSeparation( vertices, edgeList ).analyze(), 2 )

		vertices = 4
		edgeList = [ ('Ashok', 'Chun'), ('Ursala', 'Kiyoshi') ]
		self.assertEqual( DegreesofSeparation( vertices, edgeList ).analyze(), 'DISCONNECTED' )

		vertices = 6
		edgeList = [ ('Bubba', 'Cooter'), ('Ashok', 'Kiyoshi'), ('Ursala', 'Chun'), ('Ursala', 'Kiyoshi'), ('Kiyoshi', 'Chun') ]
		self.assertEqual( DegreesofSeparation( vertices, edgeList ).analyze(), 'DISCONNECTED' )

'''
UVa_10246_AsterixAndObelix
'''
class Asterix:
	def __init__( self, cities, costList, roadList ):
		vertexCount = cities + 1
		self.costMatrix = [ [ (float( 'inf' ), float( 'inf' )) for _ in range( vertexCount ) ] for _ in range( vertexCount ) ]
		self.costList = [ float( 'inf' ) ] + costList
		for u, v, cost in roadList:
			self.costMatrix[ u ][ v ] = self.costMatrix[ v ][ u ] = (cost, max( self.costList[ u ], self.costList[ v ] ))
		self._calculate()
		self._calculate()

	def _calculate( self ):
		vertexCount = len( self.costMatrix )
		for k in range( vertexCount ):
			for u in range( vertexCount ):
				for v in range( vertexCount ):
					C1, M1 = self.costMatrix[ u ][ k ]
					C2, M2 = self.costMatrix[ k ][ v ]
					C, M = C1 + C2, max( M1, M2 )
					if C + M < sum( self.costMatrix[ u ][ v ] ):
						self.costMatrix[ u ][ v ] = C, M

	def query( self, fromCity, toCity ):
		C, M = self.costMatrix[ fromCity ][ toCity ]
		cost = C + M
		return cost if cost != float( 'inf' ) else -1

	def bulkQuery( self, queryList ):
		return [ self.query( fromCity, toCity ) for (fromCity, toCity) in queryList ]

class AsterixTest( unittest.TestCase ):
	def test_Asterix( self ):
		for testfile in getTestFileList( tag='asterix', basepath='tests-practice' ):
			self._verify( testfile )

	def _verify( self, testfile ):
		with open( 'tests-practice/asterix/{}.in'.format( testfile ) ) as inputFile, \
		     open( 'tests-practice/asterix/{}.ans'.format( testfile ) ) as solutionFile:

			testcaseCount = 0
			while True:
				cities, roads, queries = readIntegers( inputFile )
				if cities == roads == queries == 0:
					break
				testcaseCount += 1

				costList = list( readIntegers( inputFile ) )
				roadList = [ tuple( readIntegers( inputFile ) ) for _ in range( roads ) ]
				queryList = [ tuple( readIntegers( inputFile ) ) for _ in range( queries ) ]

				readString( solutionFile )
				resultList = [ readInteger( solutionFile ) for _ in range( queries ) ]
				readString( solutionFile )

				print( 'Testcase {}#{} cities = {} roads = {} queries = {}'.format( testfile, testcaseCount, cities, roads, queries ) )
				asterix = Asterix( cities, costList, roadList )
				self.assertEqual( asterix.bulkQuery( queryList ), resultList )

	def test_Asterix_Sample( self ):
		cities = 7
		costList = [ 2, 3, 5, 15, 4, 4, 6 ]
		roadList = [ (1, 2, 20), (1, 4, 20), (1, 5, 50), (2, 3, 10), (3, 4, 10), (3, 5, 10), (4, 5, 15), (6, 7, 10) ]
		asterix = Asterix( cities, costList, roadList )
		queryList = [ (1, 5), (1, 6), (5, 1), (3, 1), (6, 7) ]
		resultList = [ 45, -1, 45, 35, 16 ]
		self.assertEqual( asterix.bulkQuery( queryList ), resultList )

		cities = 4
		costList = [ 2, 1, 8, 3 ]
		roadList = [ (1, 2, 7), (1, 3, 5), (2, 4, 8), (3, 4, 6) ]
		asterix = Asterix( cities, costList, roadList )
		queryList = [ (1, 4), (2, 3) ]
		resultList = [ 18, 20 ]
		self.assertEqual( asterix.bulkQuery( queryList ), resultList )

if __name__ == '__main__':
	solutionManager = SolutionManager()
	registerContestSolutions( solutionManager )
	registerUVaPracticeSolutions( solutionManager )
	solutionManager.printSummary()

	unittest.main()